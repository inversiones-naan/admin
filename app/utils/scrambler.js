export default {
  resolve: (options, callback) => {
    // The string to resolve
    let resolveString = options.resolveString || options.element.getAttribute('data-target-resolver');
    let combinedOptions = Object.assign({}, options, {resolveString: resolveString});

    function generateRandomString (length, codeletters){
      var random_text = '';
      while(random_text.length < length){
        random_text += codeletters.charAt(Math.floor(Math.random()*codeletters.length));
      } 
      
      return random_text;
    }

    function animateFadeBuffer () {
      if(combinedOptions.fadeBuffer === false){
        combinedOptions.fadeBuffer = [];
        for(var i = 0; i < combinedOptions.resolveString.length; i++){
          combinedOptions.fadeBuffer.push({c: (Math.floor(Math.random()*12))+1, l: combinedOptions.resolveString.charAt(i)});
        }
      }
      
      var do_cycles = false;
      var message = ''; 
      
      for(var i = 0; i < combinedOptions.fadeBuffer.length; i++){
        var fader = combinedOptions.fadeBuffer[i];
        if(fader.c > 0){
          do_cycles = true;
          fader.c--;
          message += combinedOptions.codeletters.charAt(Math.floor(Math.random()*combinedOptions.codeletters.length));
        } else {
          message += fader.l;
        }
      }
      
      combinedOptions.element.textContent = message
      
      if(do_cycles === true){
        setTimeout(animateFadeBuffer, 50);
      } else {
        setTimeout(callback, 50);
      }
    }

    function doResolverEffect (){
      if(combinedOptions.current_length < combinedOptions.resolveString.length){
        combinedOptions.current_length = combinedOptions.current_length + 2;
        if(combinedOptions.current_length > resolveString.length) {
          combinedOptions.current_length = combinedOptions.resolveString.length;
        }
        
        var message = generateRandomString(combinedOptions.current_length, combinedOptions.codeletters);
        combinedOptions.element.textContent = message
        
        setTimeout(doResolverEffect, 20);
      } else { 
        setTimeout(animateFadeBuffer, 20);
      }
    };
    doResolverEffect()
  }
}