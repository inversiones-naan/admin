/*
 * FrontendPerfilPage Messages
 *
 * This contains all the text for the FrontendPerfilPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.FrontendPerfilPage.header',
    defaultMessage: 'This is FrontendPerfilPage container !',
  },
});
