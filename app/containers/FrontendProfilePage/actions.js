/*
 *
 * FrontendPerfilPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PROFILE_UPDATING,
  PROFILE_UPDATED,
  PROFILE_ERROR,
  PROFILE_PROPS,
  PROFILE_REQUEST,
  PROFILE_SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function profileUpdating () {
  return {
    type: PROFILE_UPDATING
  }
}

export function profileUpdated (user) {
  return {
    type: PROFILE_UPDATED,
    user
  }
}

export function profileError (error) {
  return {
    type: PROFILE_ERROR,
    error
  }
}

export function profileProps (prop, value) {
  return {
    type: PROFILE_PROPS,
    prop,
    value
  }
}

export function profileRequest () {
  return {
    type: PROFILE_REQUEST
  }
}

export function profileSuccess (user) {
  return {
    type: PROFILE_SUCCESS,
    user
  }
}