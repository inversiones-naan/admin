/**
 *
 * FrontendPerfilPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectFrontendPerfilPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { profileUpdating, profileProps, profileRequest } from './actions';
import { makeSelectUser } from 'containers/App/selectors';

import {Container, Row, Col, CardGroup, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

import 'react-widgets/dist/css/react-widgets.css'
import { Calendar } from 'react-widgets'
import moment from 'moment'

import LoadingIndicator from 'components/LoadingIndicator';

export class FrontendPerfilPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function // onProfileRequest
  componentDidMount () {
    let { history, user } = this.props
    this.goDashboard (user, history)
    // Bloquear el acceso a no usuarios
    if (user) {
      this.props.onProfileRequest()
    }    
  }
  componentWillReceiveProps (nextProps) {
    let { history } = this.props
    this.goDashboard(nextProps.user, history)
  }
  goDashboard (user, history) {
    if (!user) {
      history.push('/')
      this.props.onClear()
    }
  }
  render() {
    const {firstName, lastName, rfc, street, ext, int, colony, city, cp, state, country, interbankClabe, birthdate, status, code, loading, success, error, modalCalendar} = this.props.frontendperfilpage
    // console.log(this.props.frontendperfilpage)
    return (
      <div className="app flex-row align-items-center animated fadeIn site-header-background">
        <Container style={{maxWidth: 1800, paddingLeft: 40, paddingRight: 40}}>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  {loading
                  ?
                    <LoadingIndicator />
                  : <CardBlock className="card-body">
                      <h1>Mi perfíl</h1>
                      <p className="text-muted">¡Completa todos tus datos e invierte con Rendex!</p>
                      {success
                      ? <Card className="text-white bg-success">
                          <CardBlock className="card-body">
                            {'Se ha actualizado correctamente'}
                          </CardBlock>
                        </Card>                  
                      : null}
                      {error
                      ? <Card className="text-white bg-danger">
                          <CardBlock className="card-body">
                            {error}
                          </CardBlock>
                        </Card>                  
                      : null}
                      <Row>
                        <Col xs="4">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                            <Input
                              ref="firstName"
                              type="text"
                              placeholder="Ingresa los nombres"
                              value={firstName}
                              onChange={(evt)=> this.props.onProfileProps('firstName', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.lastName)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="4">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                            <Input
                              ref="lastName"
                              type="text"
                              placeholder="Ingresa los apellidos"
                              value={lastName}
                              onChange={(evt)=> this.props.onProfileProps('lastName', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.rfc)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="4">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-target"></i></InputGroupAddon>
                            <Input
                              ref="rfc"
                              type="text"
                              placeholder="Ingresa el RFC"
                              value={rfc}
                              onChange={(evt)=> this.props.onProfileProps('rfc', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.street)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="6">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-direction"></i></InputGroupAddon>
                            <Input
                              ref="street"
                              type="text"
                              placeholder="Ingresa la calle"
                              value={street}
                              onChange={(evt)=> this.props.onProfileProps('street', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.ext)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="3">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-direction"></i></InputGroupAddon>
                            <Input
                              ref="ext"
                              type="text"
                              placeholder="Exterior"
                              value={ext}
                              onChange={(evt)=> this.props.onProfileProps('ext', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.int)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="3">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-direction"></i></InputGroupAddon>
                            <Input
                              ref="int"
                              type="text"
                              placeholder="Interior"
                              value={int}
                              onChange={(evt)=> this.props.onProfileProps('int', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.colony)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="4">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-directions"></i></InputGroupAddon>
                            <Input
                              ref="colony"
                              type="text"
                              placeholder="Colonia"
                              value={colony}
                              onChange={(evt)=> this.props.onProfileProps('colony', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.city)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="4">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-directions"></i></InputGroupAddon>
                            <Input
                              ref="city"
                              type="text"
                              placeholder="Ciudad"
                              value={city}
                              onChange={(evt)=> this.props.onProfileProps('city', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.cp)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="4">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-directions"></i></InputGroupAddon>
                            <Input
                              ref="cp"
                              type="text"
                              placeholder="Código Postal"
                              value={cp}
                              onChange={(evt)=> this.props.onProfileProps('cp', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.state)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="6">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-location-pin"></i></InputGroupAddon>
                            <Input
                              ref="state"
                              type="text"
                              placeholder="Estado"
                              value={state}
                              onChange={(evt)=> this.props.onProfileProps('state', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.country)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="6">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-map"></i></InputGroupAddon>
                            <Input
                              ref="country"
                              type="text"
                              placeholder="País"
                              value={country}
                              onChange={(evt)=> this.props.onProfileProps('country', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.interbankClabe)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="6">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-credit-card"></i></InputGroupAddon>
                            <Input
                              ref="interbankClabe"
                              type="text"
                              placeholder="Código Interbancario"
                              value={interbankClabe}
                              onChange={(evt)=> this.props.onProfileProps('interbankClabe', evt.target.value)}
                              onKeyDown={(evt) => {
                                let keyCode = evt.keyCode || evt.which
                                if (evt.shiftKey === false && keyCode === 13) {
                                  evt.preventDefault()
                                  let el = ReactDOM.findDOMNode(this.refs.birthdate)
                                  el.focus()
                                }
                              }}
                            />
                          </InputGroup>
                        </Col>
                        <Col xs="6">
                          <InputGroup className="mb-3">
                            <InputGroupAddon><i className="icon-calendar"></i></InputGroupAddon>
                            <Input
                              ref="birthdate"
                              type="text"
                              placeholder="Fecha de cumpleaños"
                              value={birthdate === '' || birthdate === null ?'':moment(birthdate).format('DD MMM YYYY')}
                              onFocus={()=>this.props.onProfileProps('modalCalendar', true)}
                              onChange={(evt)=> this.props.onProfileProps('birthdate', evt.target.value)}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      
                      <Row>
                        <Col xs="12">
                          <Button
                            color="success"
                            block
                            onClick={this.props.onProfileUpdating}
                            disabled={loading}
                          >
                            Actualizar
                          </Button>
                        </Col>
                      </Row>
                    </CardBlock>
                  }
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
        <Modal
          isOpen={modalCalendar}
          toggle={()=>{this.props.onProfileProps('modalCalendar', false)}}
          className={'modal-sm'}
        >
          <ModalHeader
            toggle={()=>{this.props.onProfileProps('modalCalendar', false)}}
          >
            Seleccione la fecha
          </ModalHeader>
          <ModalBody>
            <Calendar
              onChange={value => {
                this.props.onProfileProps('modalCalendar', false)
                this.props.onProfileProps('birthdate', value)
              }}
            />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

FrontendPerfilPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  frontendperfilpage: makeSelectFrontendPerfilPage(),
  user: makeSelectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onProfileUpdating: () => dispatch(profileUpdating()), 
    onProfileProps:(prop, value) => dispatch(profileProps(prop, value)),
    onProfileRequest: () => dispatch(profileRequest())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'frontendPerfilPage', reducer });
const withSaga = injectSaga({ key: 'frontendPerfilPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(FrontendPerfilPage);
