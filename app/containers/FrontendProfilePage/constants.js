/*
 *
 * FrontendPerfilPage constants
 *
 */

export const DEFAULT_ACTION = 'app/FrontendPerfilPage/DEFAULT_ACTION';

export const PROFILE_UPDATING = 'app/FrontendPerfilPage/PROFILE_UPDATING';
export const PROFILE_UPDATED = 'app/FrontendPerfilPage/PROFILE_UPDATED';
export const PROFILE_ERROR = 'app/FrontendPerfilPage/PROFILE_ERROR';

export const PROFILE_PROPS = 'app/FrontendPerfilPage/PROFILE_PROPS';

export const PROFILE_REQUEST = 'app/FrontendPerfilPage/PROFILE_REQUEST';
export const PROFILE_SUCCESS = 'app/FrontendPerfilPage/PROFILE_SUCCESS';
