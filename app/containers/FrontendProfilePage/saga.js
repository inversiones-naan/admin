import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PROFILE_REQUEST, PROFILE_UPDATING } from './constants';
import { profileSuccess, profileError, profileUpdated } from './actions';
import makeSelectFrontendPerfilPage from './selectors';
import { makeSelectUser, makeSelectLoading, makeSelectError } from '../App/selectors';
import { updateUser } from '../App/actions';
import { makeSelectToken } from '../App/selectors';
import request from 'utils/request';
import params from 'utils/params';

import { logout } from '../App/actions';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PROFILE_REQUEST, myProfile);
  yield takeLatest(PROFILE_UPDATING, updateProfile);  
}

export function * myProfile () {

  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(instrumentError('Error token'));
    return
  }

  const requestURL = `${params.apiUrl}users/me`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`,
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(profileError(payload.error.message));
      if (payload.error.message === 'Authentication: Login Failed') {
        yield put(logout());
      }
    } else {
      yield put(profileSuccess(payload.user));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(profileError('Error obteniendo perfil'));
  }
}

export function * updateProfile () {

  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(instrumentError('Error token'));
    return
  }

  const frontendperfilpage = yield select(makeSelectFrontendPerfilPage())

  const requestURL = `${params.apiUrl}users/me`;
  const options = {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${authToken}`,
    },
    body: JSON.stringify(frontendperfilpage)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(profileError(payload.error.message));
      if (payload.error.message === 'Authentication: Login Failed') {
        yield put(logout());
      }
    } else {
      yield put(profileUpdated(payload.user));

      // Actualizar usuario
      let user = yield select(makeSelectUser())      
      for (const key in payload.user) {
        if (payload.user.hasOwnProperty(key)) {
          user[key] = payload.user[key]          
        }
      }
      yield put(updateUser(user))
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(profileError('Error obteniendo perfil'));
  }
}