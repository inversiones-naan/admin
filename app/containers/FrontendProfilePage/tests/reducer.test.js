
import { fromJS } from 'immutable';
import frontendPerfilPageReducer from '../reducer';

describe('frontendPerfilPageReducer', () => {
  it('returns the initial state', () => {
    expect(frontendPerfilPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
