import { createSelector } from 'reselect';

/**
 * Direct selector to the frontendPerfilPage state domain
 */
const selectFrontendPerfilPageDomain = (state) => state.get('frontendPerfilPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by FrontendPerfilPage
 */

const makeSelectFrontendPerfilPage = () => createSelector(
  selectFrontendPerfilPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectFrontendPerfilPage;
export {
  selectFrontendPerfilPageDomain,
};
