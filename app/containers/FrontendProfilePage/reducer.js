/*
 *
 * FrontendPerfilPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  PROFILE_UPDATING,
  PROFILE_UPDATED,
  PROFILE_ERROR,
  PROFILE_PROPS,
  PROFILE_REQUEST,
  PROFILE_SUCCESS,
} from './constants';

const initialState = fromJS({
  loading: true,
  success: false,
  error: false,
  firstName: '',
  lastName: '',
  rfc: '',
  street: '',
  ext: '',
  int: '',
  colony: '',
  city: '',
  cp: '',
  state: '',
  country: '',
  interbankClabe: '',
  birthdate: '',
  modalCalendar: false,
  // status: '', // 0 pendiente, 1 activado, 2 baneado, 3 inactivo
  // code: ''
});

function frontendPerfilPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PROFILE_UPDATING:
      return state
        .set('loading', true)
        .set('success', null)
        .set('error', null)
    case PROFILE_UPDATED:
      return state
        .set('loading', false)
        .set('success', true)
        .set('error', null)
    case PROFILE_ERROR:
      return state
        .set('loading', false)
        .set('success', null)
        .set('error', action.error)
    case PROFILE_PROPS:
      return state
        .set(action.prop, action.value)
    case PROFILE_REQUEST:
      return state
        .set('loading', true)
        .set('success', null)
        .set('error', null)
    case PROFILE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('firstName', action.user.firstName || '')
        .set('lastName', action.user.lastName || '')
        .set('rfc', action.user.rfc || '')
        .set('street', action.user.street || '')
        .set('ext', action.user.ext || '')
        .set('int', action.user.int || '')
        .set('colony', action.user.colony || '')
        .set('city', action.user.city || '')
        .set('cp', action.user.cp || '')
        .set('state', action.user.state || '')
        .set('country', action.user.country || '')
        .set('interbankClabe', action.user.interbankClabe || '')
        .set('birthdate', action.user.birthdate || '')
    default:
      return state;
  }
}

export default frontendPerfilPageReducer;
