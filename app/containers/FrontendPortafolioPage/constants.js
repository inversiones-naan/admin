/*
 *
 * FrontendPortafolioPage constants
 *
 */

export const DEFAULT_ACTION = 'app/FrontendPortafolioPage/DEFAULT_ACTION';

export const PORTAFOLIO_ATTEMPT = 'app/FrontendPortafolioPage/PORTAFOLIO_ATTEMPT';
export const PORTAFOLIO_SUCCESS = 'app/FrontendPortafolioPage/PORTAFOLIO_SUCCESS';
export const PORTAFOLIO_FAILURE = 'app/FrontendPortafolioPage/PORTAFOLIO_FAILURE';
