/**
 *
 * FrontendPortafolioPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectFrontendPortafolioPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

// Actions
import { portafolioAttempt } from './actions';
import { makeSelectUser } from 'containers/App/selectors';

import {Container, Row, Col, CardGroup, Card, CardHeader, CardBlock, Button, Input, InputGroup, InputGroupAddon, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";


import Widget04 from '../../views/Widgets/Widget04';
import Widget01 from '../../views/Widgets/Widget01';

import {NavLink} from 'react-router-dom';

// Externals
import { animateScroll as scroll} from 'react-scroll'

export class FrontendPortafolioPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    let { history, user } = this.props
    this.goDashboard (user, history)
    if (user) {
      this.props.onMyPortafolio()
    }
  }
  componentWillReceiveProps (nextProps) {
    let { history } = this.props
    this.goDashboard(nextProps.user, history)
  }
  goDashboard (user, history) {
    if (!user) {
      history.push('/iniciar-sesion')
      scroll.scrollToTop();
    }
  }
  render() {
    const { payload } = this.props.frontendportafoliopage
    const numProyects = payload.projects.length
    const numMinVisible = 6
    return (
      <div className="app flex-row align-items-center animated fadeIn card-rounded">
        <Container style={{marginTop: 100, maxWidth: 1800, paddingLeft: 40, paddingRight: 40}}>
          <div className="row justify-content-md-center" style={{fontSize: '0.65rem'}}>
            <div className="col-md-3">
              <Widget04 icon="icon-graph" color="primary" header={`$ ${payload.pendingInvestment.value}`} value={payload.pendingInvestment.percent} invert>Inversión pendiente</Widget04>
              {/* <Widget01 color="success" header="89.9%"/> */}
            </div>
            <div className="col-md-3"> 
              <Widget04 icon="icon-graph" color="primary" header={`$ ${payload.investmentInConstruction.value}`} value={payload.investmentInConstruction.percent} invert>Inversión en construcción</Widget04>
            </div>
            <div className="col-md-3">
              <Widget04 icon="icon-pie-chart" color="primary" header={`$ ${payload.actualPerformance.value}`} value={payload.actualPerformance.percent} invert>Rendimiento al día</Widget04>
            </div>
            <div className="col-md-3">
              <Widget04 icon="icon-graph" color="primary" header={`$ ${payload.moneyInvestor.value}`} value={payload.moneyInvestor.percent} invert>Dinero al inversionista</Widget04>
            </div>
          </div>
          <div style={{marginLeft: 40, marginRight:40}}>
            <Row>
              {payload.projects.map((project, i) => {
                return (
                  <Col xs="12" sm="6" md="4" key={`project-${project.id}`}>
                    <Card className="text-black bg-primary">
                      <CardBlock className="card-body">
                        <div style={{height: 300, fontSize: 14, display: 'flex', justifyContent: 'center'}} className='align-items-center'>
                          <div className="features text-center">
                            <h3>{project.name}</h3>
                            <NavLink to={`/portafolio-proyecto/${project.id}`}>
                              <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper" >Ver proyecto</button>
                            </NavLink>
                          </div>
                        </div>
                      </CardBlock>
                    </Card>
                  </Col>
                )
              })}
              {Array.from({length: numMinVisible-numProyects}, (x,i) => {
                return (
                  <Col xs="12" sm="6" md="4" key={`project-empty-${i}`}>
                    <Card className="text-black bg-primary">
                      <CardBlock className="card-body">
                        <div style={{height: 300, fontSize: 14, display: 'flex', justifyContent: 'center'}} className='align-items-center'>
                          <div className="features text-center">
                            <h3>Nuevo Proyecto</h3>
                            <NavLink to="/proyectos">
                              <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper" >Comienza a invertir</button>
                            </NavLink>
                          </div>
                        </div>
                      </CardBlock>
                    </Card>
                  </Col>
                )
              })}
            </Row>
          </div>
          
        </Container>
      </div>
    );
  }
}

FrontendPortafolioPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  frontendportafoliopage: makeSelectFrontendPortafolioPage(),
  user: makeSelectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onMyPortafolio: () => dispatch(portafolioAttempt())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'frontendPortafolioPage', reducer });
const withSaga = injectSaga({ key: 'frontendPortafolioPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(FrontendPortafolioPage);
