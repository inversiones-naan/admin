/*
 * FrontendPortafolioPage Messages
 *
 * This contains all the text for the FrontendPortafolioPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.FrontendPortafolioPage.header',
    defaultMessage: 'This is FrontendPortafolioPage container !',
  },
});
