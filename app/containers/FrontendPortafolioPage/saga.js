import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PORTAFOLIO_ATTEMPT } from './constants';
import { portafolioSuccess, portafolioFailure } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectFrontendPortafolioPage from './selectors';
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PORTAFOLIO_ATTEMPT, getMyPortafolio);
}

/**
 * Obtener los proyectos de la API
 */
export function* getMyPortafolio() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(userError('Error autenticación'));
    return
  }

  const requestURL = `${params.apiUrl}projects/portafolio`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    console.log(result)
    if (result.error) {
      yield put(portafolioFailure(result.error.message));
    } else {
      yield put(portafolioSuccess(result.portafolio));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(portafolioFailure(err));
  }
}

