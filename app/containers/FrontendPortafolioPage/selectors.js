import { createSelector } from 'reselect';

/**
 * Direct selector to the frontendPortafolioPage state domain
 */
const selectFrontendPortafolioPageDomain = (state) => state.get('frontendPortafolioPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by FrontendPortafolioPage
 */

const makeSelectFrontendPortafolioPage = () => createSelector(
  selectFrontendPortafolioPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectFrontendPortafolioPage;
export {
  selectFrontendPortafolioPageDomain,
};
