
import { fromJS } from 'immutable';
import frontendPortafolioPageReducer from '../reducer';

describe('frontendPortafolioPageReducer', () => {
  it('returns the initial state', () => {
    expect(frontendPortafolioPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
