/*
 *
 * FrontendPortafolioPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  PORTAFOLIO_ATTEMPT,
  PORTAFOLIO_SUCCESS,
  PORTAFOLIO_FAILURE,
} from './constants';

const defaultPayload = {
  pendingInvestment: {
    value: '0',
    percent: '0.00'
  },
  investmentInConstruction: {
    value: '0',
    percent: '0.00'
  },
  actualPerformance: {
    value: '0',
    percent: '0.00'
  },
  moneyInvestor: {
    value: '0',
    percent: '0.00'
  },
  projects: []
}
const initialState = fromJS({
  loading: false,
  payload: defaultPayload,
  error: null
});

function frontendPortafolioPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PORTAFOLIO_ATTEMPT:
      return state
      .set('loading', true)
      .set('payload', fromJS(defaultPayload))
      .set('error', null)
    case PORTAFOLIO_SUCCESS:
      return state
      .set('loading', false)
      .set('payload', fromJS(action.payload))
      .set('loading', null)
    case PORTAFOLIO_FAILURE:
      return state
      .set('loading', false)
      .set('payload', fromJS(defaultPayload))
      .set('loading', action.error)
    default:
      return state;
  }
}

export default frontendPortafolioPageReducer;
