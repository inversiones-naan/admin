/*
 *
 * FrontendPortafolioPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PORTAFOLIO_ATTEMPT,
  PORTAFOLIO_SUCCESS,
  PORTAFOLIO_FAILURE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/** 
 * Obtener mi portafolio
*/
export function portafolioAttempt () {
  return {
    type: PORTAFOLIO_ATTEMPT
  }
}

/** 
 * Información de mi portafolio
*/
export function portafolioSuccess (payload) {
  return {
    type: PORTAFOLIO_SUCCESS,
    payload
  }
}

/** 
 * Error al obtener mi portafolio
*/
export function portafolioFailure (error) {
  return {
    type: PORTAFOLIO_FAILURE,
    error
  }
}