/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import styled from 'styled-components';
import { Switch, Route, Redirect } from 'react-router-dom';

// Containers
import ContainerApp from '../ContainerApp'
import FrontendApp from '../FrontendApp'

// Views
import Register from '../../views/Pages/Register/'
import Page404 from '../../views/Pages/Page404/'
import Page500 from '../../views/Pages/Page500/'


import HomePage from 'containers/HomePage/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Header from 'components/Header';
import Footer from 'components/Footer';

const AppWrapper = styled.div`
  max-width: calc(768px + 16px * 2);
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  padding: 0 16px;
  flex-direction: column;
`;

var element = document.getElementById("initial-loading");
element.parentNode.removeChild(element);

export default function App() {
  return (
    <Switch>
      {/* <Route exact path="/" name="Home" component={HomePage} />
      <Route path="/features" component={FeaturePage} />
      <Route path="" component={NotFoundPage} /> */}
      {/* <Route exact path="/register" name="Registro de usuario" component={Register}/> */}
      <Route exact path="/404" name="Page 404" component={Page404}/>
      <Route exact path="/500" name="Page 500" component={Page500}/>
      <Route path="/admin" name="Admin" component={ContainerApp}/>
      <Route path="/" name="Inicio" component={FrontendApp}/>
    </Switch>
  );
}
