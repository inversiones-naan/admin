/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');

const selectRoute = (state) => state.get('route');

const makeSelectCurrentUser = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('currentUser')
);

const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

const makeSelectError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('error')
);

const makeSelectRepos = () => createSelector(
  selectGlobal,
  (globalState) => {
    let repositories = globalState.getIn(['userData', 'repositories'])
    return repositories
  }
);

const makeSelectToken = () => createSelector(
  selectGlobal,
  (globalState) => {
    return globalState.getIn(['dataUser', 'token'])
  }
);

const makeSelecteExpires = () => createSelector(
  selectGlobal,
  (globalState) => {
    return globalState.getIn(['dataUser', 'expires'])
  }
);

const makeSelectUser = () => createSelector(
  selectGlobal,
  (globalState) => {
    let user = globalState.getIn(['dataUser', 'user'])
    if (!user) {
      return null
    }
    return user.toJS()
  }
);

const makeSelectUserId = () => createSelector(
  selectGlobal,
  (globalState) => {
    return globalState.getIn(['dataUser', 'user', 'id'])
  }
);

const makeSelectLocation = () => createSelector(
  selectRoute,
  (routeState) => {
    return routeState.get('location').toJS()
  }
);

export {
  selectGlobal,
  makeSelectCurrentUser,
  makeSelectLoading,
  makeSelectError,
  makeSelectRepos,
  makeSelectToken,
  makeSelecteExpires,
  makeSelectUser,
  makeSelectUserId,
  makeSelectLocation,
};
