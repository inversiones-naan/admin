/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  LOAD_REPOS_SUCCESS,
  LOAD_REPOS,
  LOAD_REPOS_ERROR,
  LOAD_USER,
  LOAD_USER_SUCCESS,
  LOAD_USER_ERROR,
  CLEAR_USER,
  UPDATE_USER
} from './constants';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  currentUser: false,
  userData: {
    repositories: false,
  },
  dataUser: {
    token: localStorage.getItem('global_token') || false,
    expires: localStorage.getItem('global_expires') || false,
    user: JSON.parse(localStorage.getItem('global_user')) || false
  }
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_REPOS:
    return state
      .set('loading', true)
      .set('error', false)
      .setIn(['userData', 'repositories'], false);
  case LOAD_REPOS_SUCCESS:
    return state
      .setIn(['userData', 'repositories'], action.repos)
      .set('loading', false)
      .set('error', false);
  case LOAD_REPOS_ERROR:
    return state
      // .set('userData', false)
      .set('error', action.error)
      .set('loading', false);
    case LOAD_USER:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['dataUser', 'token'], false)
        .setIn(['dataUser', 'expires'], false)
        .setIn(['dataUser', 'user'], false);
    case LOAD_USER_SUCCESS:      
      localStorage.setItem('global_token', action.payload.data.token)
      localStorage.setItem('global_expires', action.payload.data.expires)
      localStorage.setItem('global_user', JSON.stringify(action.payload.data.user))
      if (zE) {
        const {user} = action.payload.data
        zE(function () {
          zE.setLocale('es')  
          if (user) {
            let name = user.firstName + ' ' + user.lastName
            name = name.trim()
            if (name === '') {
              name = `usuario_${user.id}`
            }
            zE.identify({
              name: name,
              email: user.username
            })
          }
        })
      }
      
      return state
        .setIn(['dataUser', 'token'], action.payload.data.token)
        .setIn(['dataUser', 'expires'], action.payload.data.expires)
        .setIn(['dataUser', 'user'], fromJS(action.payload.data.user))
        .set('loading', false)
        .set('error', false);
    case LOAD_USER_ERROR:
      return state
        .setIn(['dataUser', 'token'], false)
        .setIn(['dataUser', 'expires'], false)
        .setIn(['dataUser', 'user'], false)
        .set('error', action.error)
        .set('loading', false);
    case CLEAR_USER:
      document.body.classList.toggle('sidebar-mobile-show');
      localStorage.removeItem('global_token')
      localStorage.removeItem('global_expires')
      localStorage.removeItem('global_user')
      if (zE) {
        zE(function () {
          zE.identify({
            name: '',
            email: ''
          })
        })
      }
      return state
      .setIn(['dataUser', 'token'], false)
      .setIn(['dataUser', 'expires'], false)
      .setIn(['dataUser', 'user'], false)
      .set('error', false)
      .set('loading', false);
    case UPDATE_USER:
    localStorage.setItem('global_user', JSON.stringify(action.user))
      return state
      .setIn(['dataUser', 'user'], fromJS(action.user))
    default:
      return state;
  }
}

export default appReducer;
