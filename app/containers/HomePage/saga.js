/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import { PROJECTS_REQUEST } from './constants';
import { projectsSuccess, projectsError } from './actions';
import { LOAD_REPOS } from 'containers/App/constants';
import { reposLoaded, repoLoadingError } from 'containers/App/actions';

import request from 'utils/request';
import params from 'utils/params';
import { makeSelectUsername } from 'containers/HomePage/selectors';
import makeSelectHomePage from './selectors';

/**
 * Github repos request/response handler
 */
export function* getRepos() {
  // Select username from store
  const username = yield select(makeSelectUsername());
  const requestURL = `${params.apiUrl}users/${username}/repos?type=all&sort=updated`;

  try {
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, requestURL);
    yield put(reposLoaded(repos, username));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_REPOS, getRepos);

  yield takeLatest(PROJECTS_REQUEST, requestProjects);
}


export function* requestProjects () {
  const homepage = yield select(makeSelectHomePage())
  const requestURL = `${params.apiUrl}projects/suggestions`;
  const options = {
    method: 'GET'
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(projectsError(payload.error.message));
    } else {
      yield put(projectsSuccess(payload.projects));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectsError('Error proyectos'));
  }
}