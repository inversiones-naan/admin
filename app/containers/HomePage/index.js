/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Parallax, Background } from 'react-parallax';
import { Link as LinkScroll, DirectLink, Element as ElementScroll , Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import Slider from 'react-slick';

import injectReducer from 'utils/injectReducer'
import makeSelectHomePage from './selectors';
import injectSaga from 'utils/injectSaga';
import { makeSelectRepos, makeSelectLoading, makeSelectError } from 'containers/App/selectors';
import H2 from 'components/H2';
import ReposList from 'components/ReposList';
import AtPrefix from './AtPrefix';
import CenteredSection from './CenteredSection';
import Form from './Form';
import Input from './Input';
import Section from './Section';
import messages from './messages';
import { loadRepos } from '../App/actions';
import { changeUsername, projectsRequest } from './actions';
import { makeSelectUsername } from './selectors';
import { makeSelectUser } from 'containers/App/selectors';
import reducer from './reducer';
import saga from './saga';

import ComparisonGraph from './ComparisonGraph'
import HowToInvest from './HowToInvest'
import HeadScroll from '../../components/HeadScroll'
import SomeProjects from './SomeProjects' 

import {NavLink} from 'react-router-dom';

export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  /**
   * when initial state username is not null, submit the form to load repos
   */
  constructor(props) {
    super(props);
    // Crear el evento para el scroll 
  }
  componentDidMount() {
    let AppHeader = document.getElementsByClassName('app-header')[0]
    if (document.body.classList.contains('sidebar-mobile-show')) {
      AppHeader.classList.add('navbar-mobile-rendex');
    } else {
      AppHeader.classList.remove('navbar-mobile-rendex');
    }
    // demo-3 sidebar-fixed aside-menu-fixed aside-menu-hidden sidebar-hidden
    // demo-3 sidebar-fixed aside-menu-fixed aside-menu-hidden sidebar-hidden sidebar-mobile-show

    // demo-3 sidebar-fixed aside-menu-fixed aside-menu-hidden sidebar-hidden

    // demo-3 sidebar-fixed aside-menu-fixed aside-menu-hidden sidebar-hidden

    // demo-3 sidebar-fixed aside-menu-fixed aside-menu-hidden sidebar-hidden
    // container intro-effect-sliced
    // app-header navbar
    this.props.onProjectsRequest()
  }
  componentWillUnmount () {
    let container = document.getElementById('container')
    container.classList.add("intro-effect-sliced")
    container.classList.add("modify")
  }
  scrollToTop() {
    scroll.scrollToTop();
  }
  render() {
    const {user, homePage} = this.props
    return (
      <div>
        <HeadScroll />
        <article className="content">
          <section className="landing-section landing-section--vertical-center landing-investment" id="investment" style={{maxHeight: 1000}}> 
            <Parallax bgImage={'./images/patterns/pattern004.jpg'} strength={300} opacity={0.25}>
              <div className="row" style={{maxWidth: 1800, margin: 'auto'}}>
                <div className="col-sm-12">
                  <h2 className="text-center">Crowdfunding inmobiliario</h2>
                  <h3 className="text-center">Con Rendex podrás convertirte en un gran inversionista, te ofrecemos altas tazas de retorno en bienes raices.</h3>
                  <div className="row justify-content-md-center">
                    <div className="col-md-8 col-xs-12">
                      <div className="investment-data">
                        <ComparisonGraph />
                        <p className="investment-data-text">Rendimientos hasta</p>
                        <p className="investment-data-number">14% - 18%</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {!user
              ? <div className="text-center">
                  <NavLink to="/registro" >
                    <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper" onClick={this.scrollToTop}>Regístrate</button>
                  </NavLink>
                </div>
              :null}              
              <br />
            </Parallax>            
          </section>
          <section className="landing-section landing-section--vertical-center landing-how-works" id="how-works">
            <Parallax bgImage={'./images/backgrounds/background06.jpg'} strength={600}>
              <ElementScroll name="howToInvest" className="row justify-content-md-center">
                <div className="col-md-12 col-sm-12">
                  <h2 className="text-center">Cómo funciona</h2>
                </div>
              </ElementScroll>
              <div style={{paddingLeft: 40, paddingRight: 40}}>
                <HowToInvest />
              </div>
              <div className="text-center" />
            </Parallax>              
          </section>

          <section className="landing-section landing-section--vertical-center landing-projects" id="projects_section" style={{textAlign: 'center'}}>
              <div className="row" style={{maxWidth: 1800, margin: 'auto'}}>
                <div className="col-md-12">
                  <br />
                  <br />
                  <h2 className="text-center">Ventajas de invertir con RENDEX</h2>
                  <br />
                  <br />
                </div>
                <div className="col-md-12">
                  <div className="row">
                    <div className="col-md-4 col-sm-6 col-xs-12 advantage-rendex">
                      <span>
                        <img src="/images/icons/accessible.png"/>
                      </span>
                      <h3>Accesible</h3>
                      <p>Invierte con nosotros desde $15,000 MXN</p>
                    </div>
                    <div className="col-md-4 col-sm-6 col-xs-12 advantage-rendex">
                      <span>
                        <img src="/images/icons/simple.png"/>
                      </span>
                      <h3>Simple</h3>
                      <p>Hazlo fácilmente desde la comodidad de tu casa</p>
                    </div>
                    <div className="col-md-4 col-sm-6 col-xs-12 advantage-rendex">
                      <span>
                        <img src="/images/icons/flexible.png"/>
                      </span>
                      <h3>Flexible</h3>
                      <p>Elige el proyecto que más te convenga.</p>
                    </div>
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="row">
                    <div className="col-md-4 col-sm-6 col-xs-12 advantage-rendex">
                      <span>
                        <img src="/images/icons/safe.png"/>
                      </span>
                      <h3>Seguro</h3>
                      <p>Filtramos los proyectos para garantizarte los mejores.</p>
                    </div>
                    <div className="col-md-4 col-sm-6 col-xs-12 advantage-rendex">
                      <span>
                        <img src="/images/icons/diversify.png"/>
                      </span>
                      <h3>Cartera diversificada</h3>
                      <p>Diversifica tú cartera y  disminuye el riesgo.</p>
                    </div>
                    <div className="col-md-4 col-sm-6 col-xs-12 advantage-rendex">
                      <span>
                        <img src="/images/icons/follow_up.png"/>
                      </span>
                      <h3>Seguimiento constante</h3>
                      <p>Desde tu portafolio verás el crecimiento de tú dinero y el avance del proyecto.</p>
                    </div>
                  </div>
                </div>
              </div>

              <div className="features text-center">
                <NavLink to="/proyectos" >
                  <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper" onClick={this.scrollToTop}>Comienza a invertir</button>
                </NavLink>
              </div>
          </section>

          <section className="landing-section landing-section--vertical-center landing-how-works" id="how-works">
            <Parallax bgImage={'./images/backgrounds/background05.jpg'} strength={1000} opacity={0.25}>
              <div className="row justify-content-md-center" style={{maxWidth: 1800, margin: 'auto'}}>
                <div className="col-md-12 col-sm-12">
                  <h2 className="text-center">¡No te quedes fuera!</h2>
                </div>
                <div className="col-md-5">
                  <div className="text-center">
                    <h4>Invierte en desarrollo inmobiliario desde $15,000 pesos. <strong>¡Fácil, rápido y seguro!</strong></h4>

                    <div className="features text-center">
                      <NavLink to="/proyectos" >
                        <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper" onClick={this.scrollToTop}>Comienza a invertir</button>
                      </NavLink>                      
                    </div>
                  </div>
                </div>
              </div>
            </Parallax>
          </section>

          <section className="landing-section landing-section--vertical-center landing-projects" id="projects_section">
            <Parallax bgImage={'./images/patterns/pattern006.jpg'} strength={450} opacity={0.25}>
              <div style={{maxWidth: 1800, margin: 'auto'}}>
                <h2 className="text-center">Se han fondeado 26 campañas</h2>
                <div className="projects" style={{paddingLeft: 40, paddingRight: 40}}>
                  <SomeProjects projects={homePage.projects} user={user} />
                </div>
                <br />
                <div className="features text-center">
                  <NavLink to="/proyectos" >
                    <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper" onClick={this.scrollToTop}>Ver mas oportunidades</button>
                  </NavLink>
                </div>
              </div>
            </Parallax>
          </section>
        </article>
      </div>
    )
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  repos: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: (evt) => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
    onProjectsRequest: () => dispatch(projectsRequest())
  };
}

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  user: makeSelectUser(),
  homePage: makeSelectHomePage()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
