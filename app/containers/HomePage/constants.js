/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CHANGE_USERNAME = 'boilerplate/Home/CHANGE_USERNAME'; // TODO eliminar esta constante

export const DEFAULT_ACTION = 'app/Home/DEFAULT_ACTION';

export const PROJECTS_REQUEST = 'app/Home/PROJECTS_REQUEST';
export const PROJECTS_SUCCESS = 'app/Home/PROJECTS_SUCCESS';
export const PROJECTS_FAILED = 'app/Home/PROJECTS_FAILED';
