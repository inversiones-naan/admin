/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectHome = (state) => state.get('home');

/**
 * Default selector used by ProjectNewPage
 */

const makeSelectHomePage = () => createSelector(
  selectHome,
  (substate) => substate.toJS()
);

const makeSelectUsername = () => createSelector(
  selectHome,
  (homeState) => homeState.get('username')
);
export default makeSelectHomePage;

export {
  selectHome,
  makeSelectUsername,
};
