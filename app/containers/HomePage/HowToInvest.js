import React, {Component} from 'react';
import Slider from 'react-slick';

export default class HowToInvest extends Component {
  render() {
    const settings = {
      dots: true,
      fade: true,
      infinite: true,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 5000
      // slidesToShow: 1,
      // slidesToScroll: 1,
      // adaptiveHeight: true
    };
    return (
      <Slider {...settings}>
        <div className="text-center">
          <div className="icon-animated piggybank"></div>
          <p>1. ¿Tienes ahorros y no sabes como invertirlos?.</p>
        </div>
        <div className="text-center">
          <div className="icon-animated lightbulb"></div>
          <p>2. ¡RENDEX! es tu mejor opción.</p>
        </div>
        <div className="text-center">
          <div className="icon-animated search"></div>
          <p>3. En Rendex podrás elegir entre multiples proyectos de inversión.</p>
        </div>
        <div className="text-center">
          <div className="icon-animated bullseye"></div>
          <p>4. Selecciona el más adecuado conforme a tus necesidades.</p>
        </div>
        <div className="text-center">
          <div className="icon-animated chat"></div>
          <p>5. Contacta con nostros que con gusto te atenderemos</p>
        </div>
        <div className="text-center">
          <div className="icon-animated cogs"></div>
          <p>6. Permitanos trabajar para ti y hacer crecer tu dinero.</p>
        </div>
        <div className="text-center">
          <div className="icon-animated map"></div>
          <p>7. Sigue los proyectos en los cuales has invertido.</p>          
        </div>
        <div className="text-center">
          <div className="icon-animated trophy"></div>
          <p>8. ¡Y obten los mejores rendimientos del mercado!</p>         
        </div>
      </Slider>
    );
  }
}