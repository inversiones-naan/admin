import React, {Component} from 'react';
import {Line, Chart} from 'react-chartjs-2';

const valIPC = 2
const valCETES = 4
const valRendex = 16

const initialState = {
  labels: ['','IPC', 'CETES', 'RENDEX',''],
  datasets: [
    {
      pointRadius: 4,
      pointHoverRadius: 5,
      label: 'IPC',
      fill: true,
      lineTension: 0.4,
      backgroundColor: 'rgba(255,99,132,0.8)',
      // borderColor: '#0b8793',
      data: [0,0,0,0,0] //[65, 59, 80, 81, 56, 55, 40]
    },
    {
      pointRadius: 4,
      pointHoverRadius: 5,
      label: 'CETES',
      fill: true,
      lineTension: 0.4,
      backgroundColor: 'rgba(75, 192, 192,0.8)',
      // borderColor: '#0b8793',
      data: [0,0,0,0,0] //[65, 59, 80, 81, 56, 55, 40]
    },
    {
      pointRadius: 4,
      pointHoverRadius: 5,
      label: 'RENDEX',
      fill: true,
      lineTension: 0.4,
      backgroundColor: 'rgba(54, 162, 235,0.8)',
      // borderColor: '#360033',
      data: [0,0,0,0,0] //[65, 59, 80, 81, 56, 55, 40]
    }
  ]
};

let options = {
  responsive: true,
  showAllTooltips: true,
  tooltips: {
    yAlign: 'bottom',
    xAlign: 'right',
    callbacks: {
      title: function(tooltipItem, data) {
        return tooltipItem[0].yLabel +' %'
      },
      label: function(tooltipItem, data) {
        return ''; // data['datasets'][0]['data'][tooltipItem['index']];
      },
      afterLabel: function(tooltipItem, data) {
        // var dataset = data['datasets'][0];
        // var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
        return '';
      }
    },
    backgroundColor: 'rgba(255,99,132,0)', //'#FFF',
    titleFontSize: 18,
    titleFontColor: '#69606b',
    bodyFontColor: '#000',
    bodyFontSize: 14,
    displayColors: false
  }, 
  legend: {
    display: false,
  },
  scales: {
    yAxes: [{
      ticks: {
        min: 0,
        max: 25,
        callback: function(value, index, values) {
          return parseFloat(value).toFixed(0)+ '%'
        },
        fontSize: 18
      },
      gridLines: {
        display: false
      }
    }],
    xAxes: [{
      gridLines: {
        display: false
      },
      ticks: {
        fontSize: 18
      }
    }]
  }
}

const steps = [
  {datas:[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]], time: 1000},
  {datas:[[0,valIPC,0,0,0],[0,0,0,0,0],[0,0,0,0,0]], time: 1500},
  {datas:[[0,valIPC,0,0,0],[0,0,valCETES,0,0],[0,0,0,0,0]], time: 1500},
  {datas:[[0,valIPC,0,0,0],[0,0,valCETES,0,0],[0,0,0,valRendex,0]], time: 3000},
  {datas:[[0,valIPC,0,0,0],[0,0,0,0,0],[0,0,0,valRendex,0]], time: 500},
  {datas:[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,valRendex,0]], time: 500}
]

const plugins = [{
  beforeRender: function (chart) {
    const w = document.body.getBoundingClientRect().width
    let fontSize = chart.scales['x-axis-0'].options.ticks.fontSize = fontSize
    if (w < 200){
      fontSize = 8
    } else if (w < 500){
      fontSize = 11     
    } else if (w < 1000){
      fontSize = 14
    } else {
      fontSize = 18
    }
    if (fontSize !== chart.scales['x-axis-0'].options.ticks.fontSize) {
      chart.scales['x-axis-0'].options.ticks.fontSize = fontSize
      chart.scales['x-axis-0'].options.ticks.minor.fontSize = fontSize
    }
    if (chart.config.options.showAllTooltips) {
      // create an array of tooltips
      // we can't use the chart tooltip because there is only one tooltip per chart
      chart.pluginTooltips = [];
      chart.config.data.datasets.forEach(function (dataset, i) {
        chart.getDatasetMeta(i).data.forEach(function (sector, j) {
          // if ([3].indexOf(sector._index) === -1) {
          //   return
          // }
          let show = false
          if (i === 0 && j === 1) {
            show = true
          } else if (i === 1 && j === 2) {
            show = true
          } else if (i === 2 && j === 3) {
            show = true
          }
          if (!show) {return}
          chart.pluginTooltips.push(new Chart.Tooltip({
            _chart: chart.chart,
            _chartInstance: chart,
            _data: chart.data,
            _options: chart.options.tooltips,
            _active: [sector]
          }, chart));
        });
      });
      // turn off normal tooltips
      chart.options.tooltips.enabled = false;
    }
  },
  afterDraw: function (chart, easing) {
    if (chart.config.options.showAllTooltips) {
        // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
        if (!chart.allTooltipsOnce) {
            if (easing !== 1)
                return;
            chart.allTooltipsOnce = true;
        }

        // turn on tooltips
        chart.options.tooltips.enabled = true;
        Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
            tooltip.initialize();
            tooltip.update();
            // we don't actually need this since we are not animating tooltips
            tooltip.pivot();
            tooltip.transition(easing).draw();
        });
        chart.options.tooltips.enabled = false;
    }
  }
}]

  
// https://github.com/TerenceGe/react-fancytext
// https://github.com/RRutsche/react-parallax
// https://github.com/bobiblazeski/react-3d-carousel
// https://codepen.io/dobladov/pen/kXAXJx
// https://github.com/akiran/react-slick

// https://tympanus.net/Development/AnimatedSVGIcons/
// https://tympanus.net/Development/IconHoverEffects/#set-2
// https://tympanus.net/Development/AnimatedCheckboxes/
// https://tympanus.net/Development/CreativeButtons/

// http://cssdeck.com/labs/css3-webkit-vertical-scrollbars

// FOndo
// https://tympanus.net/Development/AnimatedHeaderBackgrounds/index2.html

// Bar menu
// https://tympanus.net/Development/OffCanvasMenuEffects/elastic.html

// Para el carrusel de proyectos
// https://tympanus.net/Development/ArrowNavigationStyles/

// Efecto par ala primera imagen
// https://tympanus.net/Development/ArticleIntroEffects/index3.html

// Formulario de como funciona
// https://tympanus.net/Development/FullscreenForm/

// Para presentar el proyecto
// https://tympanus.net/Development/HoverEffectIdeas/index2.html

// para chronos
// https://tympanus.net/Development/MorphingSearch/#
// https://tympanus.net/Development/WobblySlideshowEffect/
class Graph extends Component {
  constructor(props) {
    super(props);
    this.steps = JSON.parse(JSON.stringify(steps))
  }
	componentWillMount(){
    
		this.setState(initialState);
  }
	componentDidMount(){
    this.animation()
  }
  componentWillUnmount () {
    clearInterval(this.timer)
  }
  animation () {
    if (typeof this.step === 'undefined') {
      this.step = 0
      this.datasets = initialState.datasets
    } else {
      if (this.step >= this.steps.length) {
        this.step = 0
      }
    }
    var newState = {
      ...initialState
    };
    for (let i = 0; i < newState.datasets.length; i++) {
      newState.datasets[i].data = JSON.parse(JSON.stringify(this.steps[this.step].datas[i]))
    }      
    this.setState(newState);
    var _this = this;
    this.timer = setTimeout(() => {
      _this.animation()
    }, _this.steps[_this.step].time);
    this.step+=1    
  }
	render() {
    let self = this
    const data = (canvas) => {
      const ctx = canvas.getContext("2d")
      let gradient1 = ctx.createLinearGradient(0, 0, 0, 600);
      gradient1.addColorStop(0, '#b0f4ea');
      gradient1.addColorStop(1, '#54d4bf');
      self.state.datasets[0].backgroundColor = gradient1
      let gradient2 = ctx.createLinearGradient(0, 0, 0, 600);
      gradient2.addColorStop(0, '#56c3cd'); 
      gradient2.addColorStop(1, '#0b8793');
      self.state.datasets[1].backgroundColor = gradient2
      let gradient3 = ctx.createLinearGradient(0, 0, 0, 300);
      gradient3.addColorStop(0, '#4AC3E9');
      gradient3.addColorStop(1, '#1B6AA9');
      self.state.datasets[2].backgroundColor = gradient3
      return self.state
    }
    const w = document.body.getBoundingClientRect().width
    if (w < 200){
      options.scales.xAxes[0].ticks.fontSize = 8
    } else if (w < 500){
      options.scales.xAxes[0].ticks.fontSize = 11
    } else if (w < 1000){
      options.scales.xAxes[0].ticks.fontSize = 14
    } else {
      options.scales.xAxes[0].ticks.fontSize = 18
    }
    // console.log(options.scales.xAxes[0].ticks.fontSize)
		return (
			<Line data={data} options={options} plugins={plugins}/>
		);
	}
};

class Comparison extends Component {
  render() {
    return (
      <Graph />
    );
  }
}

export default Comparison;