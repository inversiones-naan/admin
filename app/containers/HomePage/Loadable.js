/**
 * Asynchronously loads the component for HomePage
 */
import Loadable from 'react-loadable';

import LoadingIndicatorCenter from 'components/LoadingIndicatorCenter';

export default Loadable({
  loader: () => import('./index'),
  loading: LoadingIndicatorCenter,
});
