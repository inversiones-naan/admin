import React, {Component} from 'react';
import Slider from 'react-slick';

import {NavLink} from 'react-router-dom';

export default class SomeProjects extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 2000,
      slidesToShow: 3,
      slidesToScroll: 3,
      initialSlide: 0,
      responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
        }
      }, {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1
        }
      },{
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    };
    const {projects, user} = this.props
    return (
      <Slider {...settings}>
        {projects.map((project, i) => {
          const {id, urlImage, name, funding, fixedAnnualRate, term, status} = project
          let ribbon = 'waiting'
          if (status === '2') {
            ribbon = 'fund'
          } else if (status === '3') {
            ribbon = 'generating'
          } else if (status === '4') {
            ribbon = 'finished'
          }
          let goTo = `/proyecto/${id}`
          if (!user) {
            goTo = `iniciar-sesion`
          }
          console.log(urlImage)
          return (
            <div className="col-md-4 col-sm-4 project-item-container box-ribbon" key={`project-${id}`}>
              <NavLink to={goTo}>
                <div className={`ribbon-${ribbon}`}>
                  <div className="row project-photo" style={{backgroundImage: `url(${urlImage})`}} />
                </div>
              </NavLink>              
              <h4 className="text-center">{name}</h4>
              <table className="table">
                <tbody>
                  <tr>
                    <th className="project-data-left">Inversión</th>
                    <td className="project-data-right">{`$${funding}`}</td>
                  </tr>
                  <tr>
                    <th className="project-data-left">Tasa anual fija</th>
                    <td className="project-data-right">{`${fixedAnnualRate}%`}</td>
                  </tr>
                  <tr>
                    <th className="project-data-left">Plazo</th>
                    <td className="project-data-right">{`${term} meses`}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          )
        })}
      </Slider>
    );
  }
}