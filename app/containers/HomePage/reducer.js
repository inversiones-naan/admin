/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import {
  CHANGE_USERNAME,
  DEFAULT_ACTION,
  PROJECTS_REQUEST,
  PROJECTS_SUCCESS,
  PROJECTS_FAILED,
} from './constants';

import params from 'utils/params';

const projectDefault = {
  id: '1',
  urlImage: 'https://d359dzbkfqzc9h.cloudfront.net/uploads/project/photo/72/thumb_2017-12-04-PHOTO-00000165.jpg',
  name: 'Proyecto 1',
  // builderName: 'SBGN',
  // builderImage: 'https://d359dzbkfqzc9h.cloudfront.net/uploads/developer/logo/26/LOGO.png',
  // address: 'Benjamín Hill 245, Hipódromo',
  // progressFunding: '60.19',
  // investmentProgress: '1805771.17',
  fixedAnnualRate: '13.00',
  term: '24',
  // possibilityOfPrepaid: 'Sí, a partir del mes 6',
  // instrument: 'Deuda Senior',
  funding: '3000000.00',
  // minimumInvestment: '25000.00',
  status: '1'
}
let projects = []
for (let i = 1; i < 15; i++) {
  let p = JSON.parse(JSON.stringify(projectDefault))
  p.status = Math.floor((Math.random() * 4) + 1)+'';
  p.id = i+''
  p.fixedAnnualRate = Math.floor((Math.random() * 20) + 10).toFixed(2)+'';
  p.term = Math.floor((Math.random() * 36) + 12)+'';
  p.funding = Math.floor((Math.random() * 1000000) + 800000).toFixed(2)+'';
  p.name = `Proyecto ${(i)}`
  projects.push(p)
}

// The initial state of the App
const initialState = fromJS({
  username: '',
  projects: [],
  loadingProjects: false 
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_USERNAME:

      // Delete prefixed '@' from the github username
      return state
        .set('username', action.name.replace(/@/gi, ''));
    case DEFAULT_ACTION:
      return state;
    case PROJECTS_REQUEST:
      return state
      .set('projects', fromJS([]))
      .set('loadingProjects', true)
    case PROJECTS_SUCCESS:
      for (let i = 0; i < action.projects.length; i++) {
        action.projects[i].urlImage = params.apiUrl+action.projects[i].images[0].url
      }
      return state
      .set('projects', fromJS(action.projects))
      .set('loadingProjects', false)
    case PROJECTS_FAILED:
      return state
      .set('projects', fromJS([]))
      .set('loadingProjects', false)
    default:
      return state;
  }
}

export default homeReducer;
