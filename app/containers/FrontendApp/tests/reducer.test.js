
import { fromJS } from 'immutable';
import frontendAppReducer from '../reducer';

describe('frontendAppReducer', () => {
  it('returns the initial state', () => {
    expect(frontendAppReducer(undefined, {})).toEqual(fromJS({}));
  });
});
