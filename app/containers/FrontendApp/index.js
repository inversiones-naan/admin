/**
 *
 * FrontendApp
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { logout } from '../App/actions';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectFrontendApp from './selectors';
import { makeSelectUser } from 'containers/App/selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

// Externals
import { Helmet } from 'react-helmet';
import {Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';

// Components
import Header from '../../components/HeaderFrontent/Loadable';
import Sidebar from '../../components/SidebarFrontend/Loadable';
import Footer from '../../components/Footer/Loadable'

// Pages
import HomePage from '../HomePage/Loadable'
import FrontendProjectsPage from '../FrontendProjectsPage/Loadable'
import Sigin from '../SiginPage/Loadable'
import Activate from '../ActivatePage/Loadable'
import Login from '../LoginPage/Loadable'
import PasswordNewPage from '../PasswordNewPage/Loadable'
import ActivatePasswordPage from '../ActivatePasswordPage/Loadable'
import ActivateNewPage from '../ActivateNewPage/Loadable'
import FrontendProfilePage from '../FrontendProfilePage/Loadable'
import FrontendPortafolioPage from '../FrontendPortafolioPage/Loadable'
import FrontendProjectPage from '../FrontendProjectPage/Loadable'
import FrontendPortaforlioProjectPage from '../FrontendPortaforlioProjectPage/Loadable'

export class FrontendApp extends React.PureComponent {  
  // constructor(props) {
  //   super(props);
  // }
  componentDidMount() {
    // if (document.body.classList.contains('sidebar-mobile-show')) {
    //   let AppHeader = document.getElementsByClassName('app-header')[0]
    //   AppHeader.classList.add("navbar-mobile-rendex")
    // }
    // document.body.classList.remove('sidebar-minimized')
    // document.body.classList.remove('brand-minimized')
    // document.body.classList.add('sidebar-hidden')


    document.body.classList.add('sidebar-hidden');
    document.body.classList.remove('sidebar-minimized');
    document.body.classList.remove('brand-minimized');
    

    window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="rendex.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();

    let self = this
    zE(function () {
      
      zE.setLocale('es')

      if (self.props.user) {
        let name = self.props.user.firstName + ' ' + self.props.user.lastName
        name = name.trim()
        if (name === '') {
          name = `usuario_${self.props.user.id}`
        }
        zE.identify({
          name: name,
          email: self.props.user.username
        })
      }
      window.zESettings = {
        webWidget: {
          color: { theme: '#d6199b' }
        }
      }
    })
  }
  render() {
    // app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden
    // app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden sidebar-hidden
    const {user} = this.props
    return (
      <div id="container" className="container intro-effect-sliced modify">
        <Helmet
            titleTemplate="%s - Inversiones en bienes raices"
            defaultTitle="Rendex"
          >
          <meta name="description" content="A Administrador Inversiones application" />
        </Helmet>
        <Header onLogout={this.props.onLogout} user={user}/>
        <Sidebar {...this.props} user={user}/>
        <Switch>
          <Route path="/inicio" name="Inicio" component={HomePage}/>
          <Route path="/proyectos" name="Proyectos" component={FrontendProjectsPage}/>
          <Route path="/registro" name="Registro de usuario" component={Sigin}/>
          <Route path="/activate-account" name="Activar cuenta" component={Activate}/>
          <Route path="/iniciar-sesion" name="Inicio de sesión" component={Login}/>
          <Route path="/new-password" name="Recuperar contraseña" component={PasswordNewPage}/>
          <Route path="/activate-password" name="Activar cuenta" component={ActivatePasswordPage}/>
          <Route path="/new-activate" name="Solicitar Activar" component={ActivateNewPage}/>
          <Route path="/mi-perfil" name="Perfil de Usuario" component={FrontendProfilePage}/>
          <Route path="/portafolio" name="Portafolio" component={FrontendPortafolioPage}/>
          <Route path="/proyecto/:projectId" name="Proyecto" component={FrontendProjectPage}/>
          <Route path="/portafolio-proyecto/:projectId" name="Portafolio Proyecto" component={FrontendPortaforlioProjectPage}/>
          <Redirect from="/" to="/inicio"/>
        </Switch>
        <Footer /> 
      </div>
    );
  }
}

FrontendApp.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  // frontendapp: makeSelectFrontendApp(),
  user: makeSelectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onLogout: () => dispatch(logout()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'frontendapp', reducer });
const withSaga = injectSaga({ key: 'frontendapp', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(FrontendApp);
