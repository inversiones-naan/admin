/*
 * FrontendApp Messages
 *
 * This contains all the text for the FrontendApp component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.FrontendApp.header',
    defaultMessage: 'This is FrontendApp container !',
  },
});
