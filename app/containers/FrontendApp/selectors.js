import { createSelector } from 'reselect';

/**
 * Direct selector to the frontendApp state domain
 */
const selectFrontendAppDomain = (state) => state.get('frontendApp');

/**
 * Other specific selectors
 */


/**
 * Default selector used by FrontendApp
 */

const makeSelectFrontendApp = () => createSelector(
  selectFrontendAppDomain,
  (substate) => substate.toJS()
);

export default makeSelectFrontendApp;
export {
  selectFrontendAppDomain,
};
