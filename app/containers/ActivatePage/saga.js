import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { VALIDATE_REQUEST } from './constants';
import { validateSuccess, validateError } from './actions';
import { loginSuccess } from 'containers/App/actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectActivatePage from './selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(VALIDATE_REQUEST, activateAccount);
}

export function* activateAccount () {
  const activatepage = yield select(makeSelectActivatePage());
  const { code } = activatepage
  const payload = {
    code: code
  }
  const requestURL = `${params.apiUrl}users/activate-account`;
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    if (result.error) {
      yield put(validateError(result.error.message));
    } else {
      yield put(validateSuccess());
      yield put(loginSuccess(result));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la solicitud'
    yield put(validateError(err));
  }
}
