/*
 *
 * ActivatePage actions
 *
 */

import {
  DEFAULT_ACTION,
  VALIDATE_REQUEST,
  VALIDATE_SUCCESS,
  VALIDATE_ERROR,
  CHANGE_PROP,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}


/**
 * Validar codigo de activación
 *
 * @return {object} An action object with a type of VALIDATE_REQUEST
 */
export function validateRequest(code) {
  return {
    type: VALIDATE_REQUEST,
    code,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object} An action object with a type of VALIDATE_SUCCESS
 */
export function validateSuccess() {
  return {
    type: VALIDATE_SUCCESS,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object} An action object with a type of VALIDATE_ERROR
 */
export function validateError(error) {
  return {
    type: VALIDATE_ERROR,
    error,
  };
}