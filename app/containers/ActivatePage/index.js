/**
 *
 * ActivatePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import {NavLink} from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectActivatePage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Container,
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
} from "reactstrap";

import { validateRequest, validateError } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

export class ActivatePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    let { location } = this.props
    const params = new URLSearchParams(location.search); 
    const code = params.get('code');
    if (code === null) {
      this.props.onValidateError('No existe el codigo para activar la cuenta')
    } else {
      this.props.onValidateRequest(code)
    }
  }
  render() {
    const { activatepage } = this.props
    return (
      <div className="app flex-row site-header-background">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              {activatepage.loading
              ? <Card className="mx-4">
                <CardBlock className="card-body p-4">
                  <h1 className='text-center'>{'Validando cuenta'}</h1>
                  <LoadingIndicator />
                </CardBlock>
              </Card>
              :activatepage.error
              ? <Card className="text-white bg-danger mx-4">
                <CardBlock className="card-body p-4">
                  <h1 className='text-center'>{activatepage.error}</h1>
                </CardBlock>
              </Card>
              :activatepage.success
              ? <Card className="mx-4">
                <CardBlock className="card-body p-4">
                  <h1 className='text-center'>{activatepage.success}</h1>
                </CardBlock>
              </Card>
              :<Card className="mx-4">
                <CardBlock className="card-body p-4">
                  <h1 className='text-center'>Validar codigo de activación de cuenta</h1>
                </CardBlock>
              </Card>
              }
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

ActivatePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  activatepage: makeSelectActivatePage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onValidateRequest: (code) => dispatch(validateRequest(code)),
    onValidateError: (error) => dispatch(validateError(error)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'activatePage', reducer });
const withSaga = injectSaga({ key: 'activatePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ActivatePage);
