/*
 * ActivatePage Messages
 *
 * This contains all the text for the ActivatePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ActivatePage.header',
    defaultMessage: 'This is ActivatePage container !',
  },
});
