import { createSelector } from 'reselect';

/**
 * Direct selector to the activatePage state domain
 */
const selectActivatePageDomain = (state) => state.get('activatePage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ActivatePage
 */

const makeSelectActivatePage = () => createSelector(
  selectActivatePageDomain,
  (substate) => substate.toJS()
);

export default makeSelectActivatePage;
export {
  selectActivatePageDomain,
};
