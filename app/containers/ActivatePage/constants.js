/*
 *
 * ActivatePage constants
 *
 */

export const DEFAULT_ACTION = 'app/ActivatePage/DEFAULT_ACTION';
export const VALIDATE_REQUEST = 'app/ActivatePage/VALIDATE_REQUEST';
export const VALIDATE_SUCCESS = 'app/ActivatePage/VALIDATE_SUCCESS';
export const VALIDATE_ERROR = 'app/ActivatePage/VALIDATE_ERROR';

export const CHANGE_PROP = 'app/ActivatePage/CHANGE_PROP';
