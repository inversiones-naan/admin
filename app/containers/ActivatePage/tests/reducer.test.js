
import { fromJS } from 'immutable';
import activatePageReducer from '../reducer';

describe('activatePageReducer', () => {
  it('returns the initial state', () => {
    expect(activatePageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
