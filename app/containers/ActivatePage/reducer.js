/*
 *
 * ActivatePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  VALIDATE_REQUEST,
  VALIDATE_SUCCESS,
  VALIDATE_ERROR,
} from './constants';

const initialState = fromJS({
  loading: false,
  success: false,
  error: false,
  code: null
});

function activatePageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case VALIDATE_REQUEST:
      return state
      .set('code', action.code)
      .set('loading', true)
      .set('success', false)
      .set('error', false)
    case VALIDATE_SUCCESS:
      return state
      .set('code', null)
      .set('loading', false)
      .set('success', 'Muy bien, ¡tu cuenta esta activa!, bienvenido a Rendex')
      .set('error', false)
    case VALIDATE_ERROR:
      return state
      .set('code', null)
      .set('loading', false)
      .set('success', false)
      .set('error', action.error)
    default:
      return state;
  }
}

export default activatePageReducer;
