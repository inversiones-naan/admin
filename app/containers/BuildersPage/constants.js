/*
 *
 * BuildersPage constants
 *
 */

export const DEFAULT_ACTION = 'app/BuildersPage/DEFAULT_ACTION';
export const BUILDER_REQUEST = 'app/UsersPage/BUILDER_REQUEST';
export const BUILDER_SUCCESS = 'app/UsersPage/BUILDER_SUCCESS';
export const BUILDER_ERROR = 'app/UsersPage/BUILDER_ERROR';
