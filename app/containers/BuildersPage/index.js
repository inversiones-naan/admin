/**
 *
 * BuildersPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Link } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectBuildersPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
} from "reactstrap";

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";


import { builderRequest } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

import Paginator from 'components/Paginator'


export class BuildersPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    this.props.onBuildersRequest()
  }
  render() {
    const { builderspage } = this.props
    const { builders, loading } = builderspage

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            {builderspage.error
            ? <Card className="text-white bg-danger">
                <CardBlock className="card-body">
                  {builderspage.error}
                </CardBlock>
              </Card>                  
            : null}
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Constructoras
              </CardHeader>
              <CardBlock className="card-body">
                {loading
                ? <LoadingIndicator />
                : <ReactTable
                  data={builders}
                  defaultPageSize={5}
                  columns={[{
                    Header: 'Nombre',
                    accessor: 'name',
                    style: { textAlign: 'center' },
                    Cell: row => (
                      <div>
                        <Link to={`/admin/builders/edit/${row.original.id}`}>
                          {row.value}
                        </Link>
                      </div>
                    )
                  }]}
                  className="-striped -highlight"
                  previousText='Anterior'
                  nextText='Siguiente'
                  loadingText='Cargando...'
                  noDataText='No hay datos'
                  pageText='Página'
                  ofText='de'
                  rowsText='filas'
                />
                }
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

BuildersPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  builderspage: makeSelectBuildersPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onBuildersRequest: () => dispatch(builderRequest()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'buildersPage', reducer });
const withSaga = injectSaga({ key: 'buildersPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(BuildersPage);
