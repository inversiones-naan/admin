/*
 * BuildersPage Messages
 *
 * This contains all the text for the BuildersPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.BuildersPage.header',
    defaultMessage: 'This is BuildersPage container !',
  },
});
