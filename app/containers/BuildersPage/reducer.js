/*
 *
 * BuildersPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  BUILDER_SUCCESS,
  BUILDER_REQUEST,
  BUILDER_ERROR,
} from './constants';

const payloadBuilder = {
  items: [],
  before: 1,
  next: 1,
  last: 1,
  current: 1,
  total_pages: 1
}
const initialState = fromJS({
  payload: payloadBuilder,
  builders: [],
  columns: [{
    dataField: 'id',
    text: 'Constructora ID',
    align: 'center'
  }, {
    dataField: 'name',
    text: 'Nombre contructora',
    align: 'center'
  }],
  loading: false,
  error: false
});

function buildersPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case BUILDER_REQUEST:
      return state
      .set('loading', true)
      .set('error', false)
    case BUILDER_SUCCESS:
      return state
      .set('builders', fromJS(action.payload))
      .set('loading', false)
      .set('error', false)
    case BUILDER_ERROR:
    return state
      .set('builders', fromJS([]))
      .set('loading', false)
      .set('error', action.error)
    default:
      return state;
  }
}

export default buildersPageReducer;
