import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { BUILDER_REQUEST } from './constants';
import { builderSuccess, builderError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectBuildersPage from './selectors';
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(BUILDER_REQUEST, getBuilders);
}

export function* getBuilders () {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(builderError('Error token'));
    return
  }
  const builderspage = yield select(makeSelectBuildersPage());
  const { payload } = builderspage
  // console.log(payload)
  const requestURL = `${params.apiUrl}builders?fields=id,name`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(builderError(result.error.message));
    } else {
      yield put(builderSuccess(result.builders));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(builderError(err));
  }
}