
import { fromJS } from 'immutable';
import buildersPageReducer from '../reducer';

describe('buildersPageReducer', () => {
  it('returns the initial state', () => {
    expect(buildersPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
