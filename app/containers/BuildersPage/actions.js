/*
 *
 * BuildersPage actions
 *
 */

import {
  DEFAULT_ACTION,
  BUILDER_REQUEST,
  BUILDER_SUCCESS,
  BUILDER_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function builderRequest () {
  return {
    type: BUILDER_REQUEST,
  };
}

export function builderSuccess (payload) {
  return {
    type: BUILDER_SUCCESS,
    payload,
  };
}
export function builderError (error) {
  return {
    type: BUILDER_ERROR,
    error
  };
}