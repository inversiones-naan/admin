import { createSelector } from 'reselect';

/**
 * Direct selector to the instrumentsPage state domain
 */
const selectInstrumentsPageDomain = (state) => state.get('instrumentsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by InstrumentsPage
 */

const makeSelectInstrumentsPage = () => createSelector(
  selectInstrumentsPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectInstrumentsPage;
export {
  selectInstrumentsPageDomain,
};
