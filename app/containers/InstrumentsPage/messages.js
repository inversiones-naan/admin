/*
 * InstrumentsPage Messages
 *
 * This contains all the text for the InstrumentsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.InstrumentsPage.header',
    defaultMessage: 'This is InstrumentsPage container !',
  },
});
