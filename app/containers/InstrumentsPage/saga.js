import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { INSTRUMENT_REQUEST } from './constants';
import { instrumentSuccess, instrumentError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectInstrumentsPage from './selectors';
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(INSTRUMENT_REQUEST, getInstruments);
}

export function* getInstruments () {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(instrumentError('Error token'));
    return
  }
  const instrumentspage = yield select(makeSelectInstrumentsPage());
  const { payload } = instrumentspage
  const requestURL = `${params.apiUrl}instruments`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(instrumentError(result.error.message));
    } else {
      yield put(instrumentSuccess(result.instruments));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(instrumentError(err));
  }
}