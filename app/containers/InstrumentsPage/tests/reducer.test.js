
import { fromJS } from 'immutable';
import instrumentsPageReducer from '../reducer';

describe('instrumentsPageReducer', () => {
  it('returns the initial state', () => {
    expect(instrumentsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
