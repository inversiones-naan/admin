/*
 *
 * InstrumentsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/InstrumentsPage/DEFAULT_ACTION';
export const INSTRUMENT_REQUEST = 'app/UsersPage/INSTRUMENT_REQUEST';
export const INSTRUMENT_SUCCESS = 'app/UsersPage/INSTRUMENT_SUCCESS';
export const INSTRUMENT_ERROR = 'app/UsersPage/INSTRUMENT_ERROR';
