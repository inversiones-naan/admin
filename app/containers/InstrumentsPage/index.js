/**
 *
 * InstrumentsPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Link } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectInstrumentsPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
} from "reactstrap";

import { instrumentRequest } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

export class InstrumentsPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    this.props.onInstrumentRequest(1)
  }
  render() {
    const { instrumentspage } = this.props
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            {instrumentspage.error
            ? <Card className="text-white bg-danger">
                <CardBlock className="card-body">
                  {instrumentspage.error}
                </CardBlock>
              </Card>                  
            : null}
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Instrumentos
              </CardHeader>
              <CardBlock className="card-body">
                {instrumentspage.loading
                ? <LoadingIndicator />
                : <ReactTable
                  data={instrumentspage.instruments}
                  defaultPageSize={5}
                  columns={[{
                    Header: 'Nombre',
                    accessor: 'name',
                    style: { textAlign: 'center' },
                    Cell: row => (
                      <div>
                        <Link to={`/admin/instruments/edit/${row.original.id}`}>
                          {row.value}
                        </Link>
                      </div>
                    )
                  }]}
                  className="-striped -highlight"
                  previousText='Anterior'
                  nextText='Siguiente'
                  loadingText='Cargando...'
                  noDataText='No hay datos'
                  pageText='Página'
                  ofText='de'
                  rowsText='filas'
                />
                }
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

InstrumentsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  instrumentspage: makeSelectInstrumentsPage()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onInstrumentRequest: () => dispatch(instrumentRequest()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'instrumentsPage', reducer });
const withSaga = injectSaga({ key: 'instrumentsPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(InstrumentsPage);
