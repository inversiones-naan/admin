/*
 *
 * InstrumentsPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  INSTRUMENT_SUCCESS,
  INSTRUMENT_REQUEST,
  INSTRUMENT_ERROR,
} from './constants';

const payloadInstrument = {
  items: [],
  before: 1,
  next: 1,
  last: 1,
  current: 1,
  total_pages: 1
}
const initialState = fromJS({
  payload: payloadInstrument,
  instruments: [],
  loading: false,
  error: false
});

function instrumentsPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case INSTRUMENT_REQUEST:
      return state
      .set('instruments', fromJS([]))
      .set('loading', true)
      .set('error', false)
    case INSTRUMENT_SUCCESS:
      return state
      .set('instruments', fromJS(action.payload))
      .set('loading', false)
      .set('error', false)
    case INSTRUMENT_ERROR:
    return state
      .set('payload', fromJS([]))
      .set('loading', false)
      .set('error', action.error)
    default:
      return state;
  }
}

export default instrumentsPageReducer;
