/*
 *
 * InstrumentsPage actions
 *
 */

import {
  DEFAULT_ACTION,
  INSTRUMENT_REQUEST,
  INSTRUMENT_SUCCESS,
  INSTRUMENT_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function instrumentRequest () {
  return {
    type: INSTRUMENT_REQUEST
  };
}

export function instrumentSuccess (payload) {
  return {
    type: INSTRUMENT_SUCCESS,
    payload,
  };
}
export function instrumentError (error) {
  return {
    type: INSTRUMENT_ERROR,
    error
  };
}