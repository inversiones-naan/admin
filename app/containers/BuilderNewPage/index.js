/**
 *
 * BuilderNewPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectBuilderNewPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

import { builderChangeProp, builderCreating, builderRequest, builderUpdating, builderClear } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

import params from 'utils/params';

export class BuilderNewPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    const { builderId } = this.props.match.params
    if (builderId) {
      this.props.onBuilderRequest(builderId)
    }
  }
  componentWillReceiveProps (newProps) {
    const builderdNew = newProps.match.params.builderId
    const builderdOld = this.props.match.params.builderId
    if (builderdNew !== builderdOld) {
      this.props.onBuilderClear()
    } 
  }
  componentWillUnmount () {
    this.props.onBuilderClear()
  }
  _handleImageChange (e) {
    e.preventDefault();
    let reader = new FileReader();
    let files = e.target.files
    for (let i = 0; i < files.length; i++) {
      let reader = new FileReader();
      reader.onloadend = this.handleUploaded.bind(this,files[i], reader, files.length)
      reader.readAsDataURL(files[i])
    }
  }
  handleUploaded (file, reader, total) {
    this.props.onBuilderChangeProp('urlTemp', reader.result)
    this.props.onBuilderChangeProp('imageFile', file)
    this.forceUpdate() 
  }
  render() {
    let { buildernewpage } = this.props
    console.log()
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <strong>{buildernewpage.id ?'Editar Constructora':'Registrar Constructora'}</strong>
              </CardHeader>
              <CardBlock className="card-body">
                {buildernewpage.loading
                ?<LoadingIndicator />
                :<Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  {buildernewpage.success
                  ? <Card className="text-white bg-success">
                      <CardBlock className="card-body">
                        {buildernewpage.success}
                      </CardBlock>
                    </Card>                  
                  : null}
                  {buildernewpage.error
                  ? <Card className="text-white bg-danger">
                      <CardBlock className="card-body">
                        {buildernewpage.error}
                      </CardBlock>
                    </Card>                  
                  : null}
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Nombre (requerido)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='name'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Escriba el nombre de la Constructora"
                        min={6}
                        value={buildernewpage.name}
                        onChange={(evt) => {
                          this.props.onBuilderChangeProp('name', evt.target.value)
                        }}
                      />
                      <FormText color="help-block">Mínimo 6 caracteres</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Descripción</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='name'
                        type="textarea"
                        id="text-input"
                        name="text-input"
                        placeholder="Escriba una descripción de la Constructora"
                        min={6}
                        value={buildernewpage.description}
                        onChange={(evt) => {
                          this.props.onBuilderChangeProp('description', evt.target.value)
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Url</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='url'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Enlace a la pagina de la constructora"
                        min={6}
                        value={buildernewpage.url}
                        onChange={(evt) => {
                          this.props.onBuilderChangeProp('url', evt.target.value)
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="file-input">Imagen (requerido)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        type="file"
                        id="image_upload"
                        name="image_upload"
                        onChange={this._handleImageChange.bind(this)}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3" />
                    <Col xs="12" md="9">
                      {buildernewpage.urlTemp
                      ? <img className="centerImg" src={buildernewpage.urlTemp} />
                      :buildernewpage.urlImage
                      ? <img className="centerImg" src={`${params.apiUrl}${buildernewpage.urlImage}`} />
                      :null}
                    </Col>
                  </FormGroup>
                </Form>
                }
              </CardBlock>
              <CardFooter>
                {buildernewpage.id
                ? <Button
                  type="submit"
                  size="sm"
                  color="primary"
                  onClick={this.props.onBuilderUpdating}
                  disabled={buildernewpage.loading}
                >Editar Constructora</Button>              
                : <Button
                  type="submit"
                  size="sm"
                  color="primary"
                  onClick={this.props.onBuilderCreate}
                  disabled={buildernewpage.loading}
                >Crear Constructora</Button>
                }                
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

BuilderNewPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  buildernewpage: makeSelectBuilderNewPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onBuilderChangeProp: (prop, value) => dispatch(builderChangeProp(prop, value)),
    onBuilderCreate: () => dispatch(builderCreating()),
    onBuilderRequest: (id) => dispatch(builderRequest(id)),
    onBuilderUpdating: () => dispatch(builderUpdating()),
    onBuilderClear: () => dispatch(builderClear()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'builderNewPage', reducer });
const withSaga = injectSaga({ key: 'builderNewPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(BuilderNewPage);
