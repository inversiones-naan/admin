/*
 *
 * BuilderNewPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  BUILDER_CHANGE_PROP,
  BUILDER_CREATING,
  BUILDER_CREATED,
  BUILDER_ERROR,
  BUILDER_REQUEST,
  BUILDER_SUCCESS,
  BUILDER_UPDATING,
  BUILDER_UPDATED,
  BUILDER_CLEAR,
} from './constants';

const payloadBuilder = {
  loading: false,
  error: false,
  success: false,
  id: false,
  name: '',
  description: '',
  url: '',
  urlTemp: null,
  urlImage: null,
  imageFile: null,
}
const initialState = fromJS(payloadBuilder);

function builderNewPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case BUILDER_CHANGE_PROP:
      return state
        .set(action.prop, action.value)
    case BUILDER_CREATING:
      return state
        .set('loading', true)
        .set('error', false)
        .set('success', false)
    case BUILDER_CREATED:
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', 'Se ha registrado la constructora')
    case BUILDER_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('success', false)
    case BUILDER_REQUEST:
      return state
        .set('loading', true)
        .set('error', false)
        .set('success', false)
        .set('id', action.id)
    case BUILDER_SUCCESS:
      // console.log(action)
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', false)
        .set('name', action.payload.name)
        .set('description', action.payload.description)
        .set('urlImage', action.payload.urlImage)
        .set('url', action.payload.url)
    case BUILDER_UPDATING:
      return state
        .set('loading', true)
        .set('error', false)
        .set('success', false)
    case BUILDER_UPDATED:
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', 'Se ha actualizado')
    case BUILDER_CLEAR:
      return state
      .set('loading', false)
      .set('error', false)
      .set('success', false)
      .set('name', '')
      .set('url', '')
      .set('urlTemp', null)      
      .set('urlImage', null)
      .set('imageFile', null)
      .set('id', false)
    default:
      return state;
  }
}

export default builderNewPageReducer;
