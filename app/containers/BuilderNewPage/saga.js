import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { BUILDER_CREATING, BUILDER_REQUEST, BUILDER_UPDATING } from './constants';
import { builderCreated, builderError, builderSuccess, builderUpdated } from './actions';
import makeSelectBuilderNewPage from './selectors';
import { makeSelectToken } from '../App/selectors';
import request from 'utils/request';
import params from 'utils/params';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(BUILDER_CREATING, createBuilder);
  yield takeLatest(BUILDER_REQUEST, getBuilder);
  yield takeLatest(BUILDER_UPDATING, updateBuilder);
}

export function* createBuilder () {  
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(builderError('Error token'));
    return
  }
  const buildernewpage = yield select(makeSelectBuilderNewPage())
  const {name, description, imageFile, url} = buildernewpage
  let formData = new FormData();
  formData.append('name', name);
  formData.append('description', description);
  formData.append('url', url);
  if (imageFile) {
    formData.append('file', imageFile);
  }
  const requestURL = `${params.apiUrl}builders/`;
  const options = {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${authToken}`,
    },
    body: formData
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(builderError(payload.error.message));
    } else {
      yield put(builderCreated());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(builderError('Error al crear'));
  }
}
  
export function* getBuilder () {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(builderError('Error token'));
    return
  }
  const buildernewpage = yield select(makeSelectBuilderNewPage())
  const requestURL = `${params.apiUrl}builders/${buildernewpage.id}`;
  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    console.log(payload)
    if (payload.error) {
      yield put(builderError(payload.error.message));
    } else {
      yield put(builderSuccess(payload.builder));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(builderError('Error al obtener la constructora'));
  }
}
  
export function* updateBuilder () {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(builderError('Error token'));
    return
  }
  const buildernewpage = yield select(makeSelectBuilderNewPage())
  const {name, description, imageFile, urlImage, url} = buildernewpage
  let formData = new FormData();
  formData.append('name', name);
  formData.append('description', description);
  formData.append('urlImage', urlImage);
  formData.append('url', url);
  if (imageFile) {
    formData.append('file', imageFile);
  }

  const requestURL = `${params.apiUrl}builders/${buildernewpage.id}`;
  const options = {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${authToken}`
    },
    body: formData
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    console.log(payload)
    if (payload.error) {
      yield put(builderError(payload.error.message));
    } else {
      yield put(builderUpdated());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(builderError('Error al actualizar'));
  }
}