/*
 * BuilderNewPage Messages
 *
 * This contains all the text for the BuilderNewPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.BuilderNewPage.header',
    defaultMessage: 'This is BuilderNewPage container !',
  },
});
