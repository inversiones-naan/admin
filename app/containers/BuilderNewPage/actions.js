/*
 *
 * BuilderNewPage actions
 *
 */

import {
  DEFAULT_ACTION,
  BUILDER_CHANGE_PROP,
  BUILDER_CREATING,
  BUILDER_CREATED,
  BUILDER_ERROR,
  BUILDER_REQUEST,
  BUILDER_SUCCESS,
  BUILDER_UPDATING,
  BUILDER_UPDATED,
  BUILDER_CLEAR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function builderChangeProp(prop, value) {
  return {
    type: BUILDER_CHANGE_PROP,
    prop,
    value,
  };
}

export function builderCreating() {
  return {
    type: BUILDER_CREATING,
  };
}

export function builderCreated() {
  return {
    type: BUILDER_CREATED,
  };
}

export function builderError(error) {
  return {
    type: BUILDER_ERROR,
    error,
  };
}

export function builderRequest(id) {
  return {
    type: BUILDER_REQUEST,
    id,
  };
}

export function builderSuccess(payload) {
  return {
    type: BUILDER_SUCCESS,
    payload,
  };
}

export function builderUpdating() {
  return {
    type: BUILDER_UPDATING,
  };
}

export function builderUpdated() {
  return {
    type: BUILDER_UPDATED,
  };
}

export function builderClear() {
  return {
    type: BUILDER_CLEAR,
  };
}
