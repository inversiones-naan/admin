/*
 *
 * BuilderNewPage constants
 *
 */

export const DEFAULT_ACTION = 'app/BuilderNewPage/DEFAULT_ACTION';
export const BUILDER_CREATING = 'app/InstrumentNewPage/BUILDER_CREATING';
export const BUILDER_CREATED = 'app/InstrumentNewPage/BUILDER_CREATED';
export const BUILDER_ERROR = 'app/InstrumentNewPage/BUILDER_ERROR';
export const BUILDER_CHANGE_PROP = 'app/InstrumentNewPage/BUILDER_CHANGE_PROP';
export const BUILDER_REQUEST = 'app/InstrumentNewPage/BUILDER_REQUEST';
export const BUILDER_SUCCESS = 'app/InstrumentNewPage/BUILDER_SUCCESS';
export const BUILDER_UPDATING = 'app/InstrumentNewPage/BUILDER_UPDATING';
export const BUILDER_UPDATED = 'app/InstrumentNewPage/BUILDER_UPDATED';
export const BUILDER_CLEAR = 'app/InstrumentNewPage/BUILDER_CLEAR';
