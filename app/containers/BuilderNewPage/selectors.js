import { createSelector } from 'reselect';

/**
 * Direct selector to the builderNewPage state domain
 */
const selectBuilderNewPageDomain = (state) => state.get('builderNewPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by BuilderNewPage
 */

const makeSelectBuilderNewPage = () => createSelector(
  selectBuilderNewPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectBuilderNewPage;
export {
  selectBuilderNewPageDomain,
};
