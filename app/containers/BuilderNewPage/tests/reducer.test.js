
import { fromJS } from 'immutable';
import builderNewPageReducer from '../reducer';

describe('builderNewPageReducer', () => {
  it('returns the initial state', () => {
    expect(builderNewPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
