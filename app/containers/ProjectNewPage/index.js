/**
 *
 * ProjectNewPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectProjectNewPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

import { projectChangeProp, projectCreating, projectRequest, projectUpdating, projectClear, projectInstrumentsRequest, projectBuilersRequest } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

export class ProjectNewPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    const { projectId } = this.props.match.params
    if (projectId) {
      this.props.onProjectRequest(projectId)
    }
    this.props.onProjectInstrumentsRequest()
    this.props.onProjectBuilersRequest()
  }
  componentWillReceiveProps (newProps) {
    const projectdNew = newProps.match.params.projectId
    const projectdOld = this.props.match.params.projectId
    if (projectdNew !== projectdOld) {
      this.props.onProjectClear()
    } 
  }
  componentWillUnmount () {
    this.props.onProjectClear()
  }
  render() {
    let { projectnewpage } = this.props
    if (projectnewpage.builders.length === 0 || projectnewpage.instruments.length === 0) {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" md="12">
              {projectnewpage.builders.length === 0
              ?<Card className="text-white bg-danger">
                <CardBlock className="card-body">
                  Aún no existe contructoras
                </CardBlock>
              </Card>            
              :null}
              {projectnewpage.instruments.length === 0
              ?<Card className="text-white bg-danger">
                <CardBlock className="card-body">
                  Aun no existen isntrumentos
                </CardBlock>
              </Card>          
              :null}              
            </Col>
          </Row>
        </div>
      )
    }
    // console.log(projectnewpage)
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <strong>{projectnewpage.id ?'Editar Projecto':'Crear Proyecto'}</strong>
              </CardHeader>
              <CardBlock className="card-body">
                {projectnewpage.loading
                ?<LoadingIndicator />
                :<Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  {projectnewpage.success
                  ? <Card className="text-white bg-success">
                      <CardBlock className="card-body">
                        {projectnewpage.success}
                      </CardBlock>
                    </Card>
                  : null}
                  {projectnewpage.error
                  ? <Card className="text-white bg-danger">
                      <CardBlock className="card-body">
                        {projectnewpage.error}
                      </CardBlock>
                    </Card>
                  : null}
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Nombre (required)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='name'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Escriba el nombre"
                        min={6}
                        value={projectnewpage.name}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('name', evt.target.value)
                        }}
                      />
                      <FormText color="help-block">Mínimo 6 caracteres</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Fondo a recaudar</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='fixedAnnualRate'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Valor del proyecto"
                        min={6}
                        value={projectnewpage.funding}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('funding', evt.target.value)
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Inversión mímima (requerida)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='minimumInvestment'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Escriba la inversion mínima"
                        min={6}
                        value={projectnewpage.minimumInvestment}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('minimumInvestment', evt.target.value)
                        }}
                      />
                      <FormText color="help-block">Mínimo 6 caracteres</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="select">Intrumento</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="instrument"
                        type="select"
                        name="select"
                        id="selectInstrument"
                        value={this.props.instrumentId}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('instrumentId', evt.target.value)
                        }}
                      >
                      {projectnewpage.instruments.map( (instrument, index)=>{ 
                        return (
                          <option key={`select-instrument-${instrument.id}`} value={instrument.id}>{instrument.name}</option>
                        )
                      })}
                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Tasa anual fija (requerida)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='fixedAnnualRate'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Escriba la Tasa anual fija"
                        min={6}
                        value={projectnewpage.fixedAnnualRate}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('fixedAnnualRate', evt.target.value)
                        }}
                      />
                      <FormText color="help-block">Mínimo 6 caracteres</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Plazo (requerido)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='term'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Escriba el Plazo"
                        min={6}
                        value={projectnewpage.term}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('term', evt.target.value)
                        }}
                      />
                      <FormText color="help-block">Mínimo 2 caracteres</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Posibilidad de Prepago (requerido)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='possibilityOfPrepaid'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Escriba la Posibilidad de Prepago"
                        min={6}
                        value={projectnewpage.possibilityOfPrepaid}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('possibilityOfPrepaid', evt.target.value)
                        }}
                      />
                      <FormText color="help-block">Mínimo 2 caracteres</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Descripción</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='description'
                        type="textarea"
                        name="description-input"
                        placeholder="Escriba la descripción del proyecto"
                        min={6}
                        value={projectnewpage.description}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('description', evt.target.value)
                        }}
                      />
                      {/* <FormText color="help-block">Min 6 characters</FormText> */}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="select">Constructora</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="builder"
                        type="select"
                        name="select"
                        id="selectBuilder"
                        value={projectnewpage.builderId}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('builderId', evt.target.value)
                        }}
                      >
                      {projectnewpage.builders.map( (builder, index)=>{ 
                        return (
                          <option key={`select-builder-${builder.id}`} value={builder.id}>{builder.name}</option>
                        )
                      })}
                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Inversión</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='address'
                        type="textarea"
                        id="text-input"
                        name="text-input"
                        placeholder="Ingrese la descripcion de la inversión"
                        min={6}
                        value={projectnewpage.investment}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('investment', evt.target.value)
                        }}
                      />
                      {/* <FormText color="help-block">Min 6 characters</FormText> */}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Dirección</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='address'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Escriba la dirección"
                        min={6}
                        value={projectnewpage.address}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('address', evt.target.value)
                        }}
                      />
                      {/* <FormText color="help-block">Min 6 characters</FormText> */}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Latitud</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='address'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Ingrese la latitud"
                        min={6}
                        value={projectnewpage.googleLat}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('googleLat', evt.target.value)
                        }}
                      />
                      {/* <FormText color="help-block">Min 6 characters</FormText> */}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Longitud</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='address'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Ingrese la longitud"
                        min={6}
                        value={projectnewpage.googleLng}
                        onChange={(evt) => {
                          this.props.onProjectChangeProp('googleLng', evt.target.value)
                        }}
                      />
                      {/* <FormText color="help-block">Min 6 characters</FormText> */}
                    </Col>
                  </FormGroup>
                </Form>
                }
              </CardBlock>
              <CardFooter>
                {projectnewpage.id
                ? <Button
                  type="submit"
                  size="sm"
                  color="primary"
                  onClick={this.props.onProjectUpdating}
                  disabled={projectnewpage.loading}
                >Editar Proyecto</Button>              
                : <Button
                  type="submit"
                  size="sm"
                  color="primary"
                  onClick={this.props.onProjectCreate}
                  disabled={projectnewpage.loading}
                >Crear Proyecto</Button>
                }
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

ProjectNewPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  projectnewpage: makeSelectProjectNewPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onProjectChangeProp: (prop, value) => dispatch(projectChangeProp(prop, value)),
    onProjectCreate: () => dispatch(projectCreating()),
    onProjectRequest: (id) => dispatch(projectRequest(id)),
    onProjectUpdating: () => dispatch(projectUpdating()),
    onProjectClear: () => dispatch(projectClear()),
    onProjectInstrumentsRequest: () => dispatch(projectInstrumentsRequest()),
    onProjectBuilersRequest: () => dispatch(projectBuilersRequest()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'projectNewPage', reducer });
const withSaga = injectSaga({ key: 'projectNewPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ProjectNewPage);
