import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PROJECT_CREATING, PROJECT_REQUEST, PROJECT_UPDATING, PROJECT_INSTRUMENTS_REQUEST, PROJECT_BUILDERS_REQUEST } from './constants';
import { projectCreated, projectError, projectSuccess, projectUpdated, projectInstrumentsSuccess, projectInstrumentsError, projectBuilersSuccess, projectBuilersError } from './actions';
import makeSelectProjectNewPage from './selectors';
import { makeSelectToken } from '../App/selectors';
import request from 'utils/request';
import params from 'utils/params';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PROJECT_CREATING, createProject);
  yield takeLatest(PROJECT_REQUEST, getProject);
  yield takeLatest(PROJECT_UPDATING, updateProject);
  yield takeLatest(PROJECT_INSTRUMENTS_REQUEST, requestInstruments);
  yield takeLatest(PROJECT_BUILDERS_REQUEST, requestBuilders);  
}

export function* createProject () {  
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(projectError('Error token'));
    return
  }
  const projectnewpage = yield select(makeSelectProjectNewPage())
  const payload = {
    name: projectnewpage.name,
    address: projectnewpage.address,
    funding: projectnewpage.funding,
    builderId: projectnewpage.builderId,
    fixedAnnualRate: projectnewpage.fixedAnnualRate,
    instrumentId: projectnewpage.instrumentId,
    minimumInvestment: projectnewpage.minimumInvestment,
    possibilityOfPrepaid: projectnewpage.possibilityOfPrepaid,
    term: projectnewpage.term,
    description: projectnewpage.description,
    investment: projectnewpage.investment,
    googleLat: projectnewpage.googleLat,
    googleLng: projectnewpage.googleLng
  }
  const requestURL = `${params.apiUrl}projects/`;
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`,
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(projectError(payload.error.message));
    } else {
      yield put(projectCreated());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectError('Error al crear el proyecto'));
  }
}
  
export function* getProject () {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(projectError('Error token'));
    return
  }
  const projectnewpage = yield select(makeSelectProjectNewPage())
  const requestURL = `${params.apiUrl}projects/${projectnewpage.id}`;
  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(projectError(payload.error.message));
    } else {
      yield put(projectSuccess(payload.project));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectError('Error updating'));
  }
}
  
export function* updateProject () {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(projectError('Error token'));
    return
  }
  const projectnewpage = yield select(makeSelectProjectNewPage())
  const payload = {
    name: projectnewpage.name,
    address: projectnewpage.address,
    funding: projectnewpage.funding,
    builderId: projectnewpage.builderId,
    fixedAnnualRate: projectnewpage.fixedAnnualRate,
    instrumentId: projectnewpage.instrumentId,
    minimumInvestment: projectnewpage.minimumInvestment,
    possibilityOfPrepaid: projectnewpage.possibilityOfPrepaid,
    term: projectnewpage.term,
    description: projectnewpage.description,
    investment: projectnewpage.investment,
    googleLat: projectnewpage.googleLat,
    googleLng: projectnewpage.googleLng
  }
  const requestURL = `${params.apiUrl}projects/${projectnewpage.id}`;
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload) 
    if (payload.error) {
      yield put(projectError(payload.error.message));
    } else {
      yield put(projectUpdated());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectError('Error al actualizar el proyecto'));
  }
}

export function* requestInstruments () {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(projectError('Error token'));
    return
  }
  const projectnewpage = yield select(makeSelectProjectNewPage())
  const requestURL = `${params.apiUrl}instruments/`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(projectInstrumentsError(payload.error.message));
    } else {
      yield put(projectInstrumentsSuccess(payload));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectInstrumentsError('Error al ontener los instrumentos'));
  }
}

export function* requestBuilders () {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(projectError('Error token'));
    return
  }
  const projectnewpage = yield select(makeSelectProjectNewPage())
  const requestURL = `${params.apiUrl}builders/`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(projectBuilersError(payload.error.message));
    } else {
      yield put(projectBuilersSuccess(payload));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectBuilersError('Error al obtener las constructoras'));
  }
}