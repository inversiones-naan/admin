import { createSelector } from 'reselect';

/**
 * Direct selector to the projectNewPage state domain
 */
const selectProjectNewPageDomain = (state) => state.get('projectNewPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ProjectNewPage
 */

const makeSelectProjectNewPage = () => createSelector(
  selectProjectNewPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectProjectNewPage;
export {
  selectProjectNewPageDomain,
};
