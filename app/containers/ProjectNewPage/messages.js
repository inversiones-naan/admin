/*
 * ProjectNewPage Messages
 *
 * This contains all the text for the ProjectNewPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ProjectNewPage.header',
    defaultMessage: 'This is ProjectNewPage container !',
  },
});
