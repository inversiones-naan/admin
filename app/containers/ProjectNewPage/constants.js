/*
 *
 * ProjectNewPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ProjectNewPage/DEFAULT_ACTION';
export const PROJECT_CREATING = 'app/InstrumentNewPage/PROJECT_CREATING';
export const PROJECT_CREATED = 'app/InstrumentNewPage/PROJECT_CREATED';
export const PROJECT_ERROR = 'app/InstrumentNewPage/PROJECT_ERROR';
export const PROJECT_CHANGE_PROP = 'app/InstrumentNewPage/PROJECT_CHANGE_PROP';
export const PROJECT_REQUEST = 'app/InstrumentNewPage/PROJECT_REQUEST';
export const PROJECT_SUCCESS = 'app/InstrumentNewPage/PROJECT_SUCCESS';
export const PROJECT_UPDATING = 'app/InstrumentNewPage/PROJECT_UPDATING';
export const PROJECT_UPDATED = 'app/InstrumentNewPage/PROJECT_UPDATED';
export const PROJECT_CLEAR = 'app/InstrumentNewPage/PROJECT_CLEAR';

export const PROJECT_INSTRUMENTS_REQUEST = 'app/InstrumentNewPage/PROJECT_INSTRUMENTS_REQUEST';
export const PROJECT_INSTRUMENTS_SUCCESS = 'app/InstrumentNewPage/PROJECT_INSTRUMENTS_SUCCESS';
export const PROJECT_INSTRUMENTS_ERROR = 'app/InstrumentNewPage/PROJECT_INSTRUMENTS_ERROR';

export const PROJECT_BUILDERS_REQUEST = 'app/InstrumentNewPage/PROJECT_BUILDERS_REQUEST';
export const PROJECT_BUILDERS_SUCCESS = 'app/InstrumentNewPage/PROJECT_BUILDERS_SUCCESS';
export const PROJECT_BUILDERS_ERROR = 'app/InstrumentNewPage/PROJECT_BUILDERS_ERROR';

