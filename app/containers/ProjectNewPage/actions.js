/*
 *
 * ProjectNewPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PROJECT_CHANGE_PROP,
  PROJECT_CREATING,
  PROJECT_CREATED,
  PROJECT_ERROR,
  PROJECT_REQUEST,
  PROJECT_SUCCESS,
  PROJECT_UPDATING,
  PROJECT_UPDATED,
  PROJECT_CLEAR,
  PROJECT_INSTRUMENTS_REQUEST,
  PROJECT_INSTRUMENTS_SUCCESS,
  PROJECT_INSTRUMENTS_ERROR,
  PROJECT_BUILDERS_REQUEST,
  PROJECT_BUILDERS_SUCCESS,
  PROJECT_BUILDERS_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function projectChangeProp(prop, value) {
  return {
    type: PROJECT_CHANGE_PROP,
    prop,
    value,
  };
}

export function projectCreating() {
  return {
    type: PROJECT_CREATING,
  };
}

export function projectCreated() {
  return {
    type: PROJECT_CREATED,
  };
}

export function projectError(error) {
  return {
    type: PROJECT_ERROR,
    error,
  };
}

export function projectRequest(id) {
  return {
    type: PROJECT_REQUEST,
    id,
  };
}

export function projectSuccess(payload) {
  return {
    type: PROJECT_SUCCESS,
    payload,
  };
}

export function projectUpdating() {
  return {
    type: PROJECT_UPDATING,
  };
}

export function projectUpdated() {
  return {
    type: PROJECT_UPDATED,
  };
}

export function projectClear() {
  return {
    type: PROJECT_CLEAR,
  };
}

export function projectInstrumentsRequest() {
  return {
    type: PROJECT_INSTRUMENTS_REQUEST,
  };
}

export function projectInstrumentsSuccess(payload) {
  return {
    type: PROJECT_INSTRUMENTS_SUCCESS,
    payload,
  };
}
export function projectInstrumentsError(error) {
  return {
    type: PROJECT_INSTRUMENTS_ERROR,
    error,
  };
}

export function projectBuilersRequest() {
  return {
    type: PROJECT_BUILDERS_REQUEST,
  };
}

export function projectBuilersSuccess(payload) {
  return {
    type: PROJECT_BUILDERS_SUCCESS,
    payload,
  };
}
export function projectBuilersError(error) {
  return {
    type: PROJECT_BUILDERS_ERROR,
    error,
  };
}