/*
 *
 * ProjectNewPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  PROJECT_CHANGE_PROP,
  PROJECT_CREATING,
  PROJECT_CREATED,
  PROJECT_ERROR,
  PROJECT_REQUEST,
  PROJECT_SUCCESS,
  PROJECT_UPDATING,
  PROJECT_UPDATED,
  PROJECT_CLEAR,
  PROJECT_INSTRUMENTS_REQUEST,
  PROJECT_INSTRUMENTS_SUCCESS,
  PROJECT_INSTRUMENTS_ERROR,
  PROJECT_BUILDERS_REQUEST,
  PROJECT_BUILDERS_SUCCESS,
  PROJECT_BUILDERS_ERROR,
} from './constants';

const payloadProject = {
  loading: false,
  error: false,
  success: false,
  id: false,
  name: '',
  description: '',
  builderId: null,
  builders: [],
  address: '',
  funding: 0,
  fixedAnnualRate: '',
  instrumentId: null,
  instruments: [],
  term: '',
  possibilityOfPrepaid: '',
  minimumInvestment: '',
  investment:'',
  googleLat:'',
  googleLng:''
}
const initialState = fromJS(payloadProject);

function projectNewPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PROJECT_CHANGE_PROP:
      return state
        .set(action.prop, action.value)
    case PROJECT_CREATING:
      return state
        .set('loading', true)
        .set('error', false)
        .set('success', false)
    case PROJECT_CREATED:
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', 'Projecto creado')
    case PROJECT_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('success', false)
    case PROJECT_REQUEST:
      return state
        .set('loading', true)
        .set('error', false)
        .set('success', false)
        .set('id', action.id)
    case PROJECT_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', false)
        .set('name', action.payload.name)
        .set('funding', action.payload.funding)
        .set('builderId', action.payload.builderId)
        .set('address', action.payload.address)
        .set('fixedAnnualRate', action.payload.fixedAnnualRate)
        .set('instrumentId', action.payload.instrumentId)
        .set('term', action.payload.term)
        .set('possibilityOfPrepaid', action.payload.possibilityOfPrepaid)
        .set('minimumInvestment', action.payload.minimumInvestment)
        .set('description', action.payload.description)
        .set('investment', action.payload.investment)
        .set('googleLat', action.payload.googleLat)
        .set('googleLng', action.payload.googleLng)
    case PROJECT_UPDATING:
      return state
        .set('loading', true)
        .set('error', false)
        .set('success', false)
    case PROJECT_UPDATED:
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', 'Projecto actualizado')
    case PROJECT_CLEAR:
      return fromJS(payloadProject)
    case PROJECT_INSTRUMENTS_REQUEST:
      return state
    case PROJECT_INSTRUMENTS_SUCCESS:
      let instrumentId = null
      if (action.payload.instruments.length > 0) {
        instrumentId = action.payload.instruments[0].id
      }
      return state
        .set('instruments', fromJS(action.payload.instruments))
        .set('instrumentId', instrumentId)
    case PROJECT_INSTRUMENTS_ERROR:
      return state
    case PROJECT_BUILDERS_REQUEST:
      return state
    case PROJECT_BUILDERS_SUCCESS:
      let builderId = null
      if (action.payload.builders.length > 0) {
        builderId = action.payload.builders[0].id
      }
      return state
        .set('builders', fromJS(action.payload.builders))
        .set('builderId', builderId)
    case PROJECT_BUILDERS_ERROR:
      return state
    default:
      return state;
  }
}

export default projectNewPageReducer;
