
import { fromJS } from 'immutable';
import projectNewPageReducer from '../reducer';

describe('projectNewPageReducer', () => {
  it('returns the initial state', () => {
    expect(projectNewPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
