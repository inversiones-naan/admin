/*
 *
 * FrontendPortaforlioProjectPage constants
 *
 */

export const DEFAULT_ACTION = 'app/FrontendPortaforlioProjectPage/DEFAULT_ACTION';

export const FPPPI_REQUEST = 'app/FrontendPortaforlioProjectPage/FPPPI_REQUEST';
export const FPPPI_SUCCESS = 'app/FrontendPortaforlioProjectPage/FPPPI_SUCCESS';
export const FPPPI_ERROR = 'app/FrontendPortaforlioProjectPage/FPPPI_ERROR';

export const FPPPP_REQUEST = 'app/FrontendPortaforlioProjectPage/FPPPP_REQUEST';
export const FPPPP_SUCCESS = 'app/FrontendPortaforlioProjectPage/FPPPP_SUCCESS';
export const FPPPP_ERROR = 'app/FrontendPortaforlioProjectPage/FPPPP_ERROR';

export const FPPPM_REQUEST = 'app/FrontendPortaforlioProjectPage/FPPPM_REQUEST';
export const FPPPM_SUCCESS = 'app/FrontendPortaforlioProjectPage/FPPPM_SUCCESS';
export const FPPPM_ERROR = 'app/FrontendPortaforlioProjectPage/FPPPM_ERROR';

export const FPPP_CHANGE_PROPS = 'app/FrontendPortaforlioProjectPage/FPPP_CHANGE_PROPS';
