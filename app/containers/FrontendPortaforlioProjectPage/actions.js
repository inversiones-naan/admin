/*
 *
 * FrontendPortaforlioProjectPage actions
 *
 */

import {
  DEFAULT_ACTION,
  FPPPI_REQUEST,
  FPPPI_SUCCESS,
  FPPPI_ERROR,
  FPPPP_REQUEST,
  FPPPP_SUCCESS,
  FPPPP_ERROR,
  FPPP_CHANGE_PROPS,
  FPPPM_REQUEST,
  FPPPM_SUCCESS,
  FPPPM_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function inversionRequest () {
  return {
    type: FPPPI_REQUEST
  }
}

export function inversionSuccess (payload) {
  return {
    type: FPPPI_SUCCESS,
    payload,
  }
}

export function inversionError (error) {
  return {
    type: FPPPI_ERROR,
    error
  }
}

export function payRequest () {
  return {
    type: FPPPP_REQUEST
  }
}

export function paySuccess (payload) {
  return {
    type: FPPPP_SUCCESS,
    payload,
  }
}

export function payError (error) {
  return {
    type: FPPPP_ERROR,
    error
  }
}

export function changeProps (object) {
  return {
    type: FPPP_CHANGE_PROPS,
    object
  }
}


export function newsRequest () {
  return {
    type: FPPPM_REQUEST
  }
}

export function newsSuccess (payload) {
  return {
    type: FPPPM_SUCCESS,
    payload,
  }
}

export function newsError (error) {
  return {
    type: FPPPM_ERROR,
    error
  }
}