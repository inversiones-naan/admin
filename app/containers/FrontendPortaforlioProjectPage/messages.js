/*
 * FrontendPortaforlioProjectPage Messages
 *
 * This contains all the text for the FrontendPortaforlioProjectPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.FrontendPortaforlioProjectPage.header',
    defaultMessage: 'This is FrontendPortaforlioProjectPage container !',
  },
});
