import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { FPPPI_REQUEST, FPPPP_REQUEST, FPPPM_REQUEST } from './constants';
import { inversionSuccess, inversionError, paySuccess, payError, newsSuccess, newsError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectFrontendPortaforlioProjectPage from './selectors';
import { makeSelectToken, makeSelectUser } from 'containers/App/selectors';
import { logout } from 'containers/App/actions';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(FPPPI_REQUEST, getInversion);
  yield takeLatest(FPPPP_REQUEST, getPay);
  yield takeLatest(FPPPM_REQUEST, getNews);
  
}

/**
 * Obtener los proyectos de la API
 */
export function* getInversion() {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(inversionError('Error token'));
    return
  }
  // Select authToken from store
  const fppp = yield select(makeSelectFrontendPortaforlioProjectPage());
  const {projectId} = fppp

  const user = yield select(makeSelectUser())

  const requestURL = `${params.apiUrl}funding/?where={"projectId":{"e":"${projectId}"}, "userId":{"e":"${user.id}"}, "type": {"e":"0"}}&sort={"id":-1}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      if (result.error.message === 'Authentication: Login Failed') {
        yield put(logout());
      } else {
        yield put(inversionError(result.error.message));        
      }
    } else {
      yield put(inversionSuccess(result.funding));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(inversionError(err));
  }
}


/**
 * Obtener los proyectos de la API
 */
export function* getPay() {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(payError('Error token'));
    return
  }
  // Select authToken from store
  const fppp = yield select(makeSelectFrontendPortaforlioProjectPage());
  const {projectId} = fppp

  const user = yield select(makeSelectUser())

  const requestURL = `${params.apiUrl}funding/?where={"projectId":{"e":"${projectId}"}, "userId":{"e":"${user.id}"}, "type": {"e":"1"}}&sort={"id":-1}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      if (result.error.message === 'Authentication: Login Failed') {
        yield put(logout());
      } else {
        yield put(payError(result.error.message));        
      }
    } else {
      yield put(paySuccess(result.funding));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(payError(err));
  }
}

/**
 * Obtener los proyectos de la API
 */
export function* getNews() {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(newsError('Error token'));
    return
  }
  // Select authToken from store
  const fppp = yield select(makeSelectFrontendPortaforlioProjectPage());
  const {projectId} = fppp

  const requestURL = `${params.apiUrl}project-news/?having={"projectId":"${projectId}"}&sort={"id":-1}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    console.log(result)
    if (result.error) {
      if (result.error.message === 'Authentication: Login Failed') {
        yield put(logout());
      } else {
        yield put(newsError(result.error.message));        
      }
    } else {
      yield put(newsSuccess(result.projectsNews));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(newsError(err));
  }
}