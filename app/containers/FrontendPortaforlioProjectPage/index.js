/**
 *
 * FrontendPortaforlioProjectPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectFrontendPortaforlioProjectPage from './selectors';
import { makeSelectUser } from 'containers/App/selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { inversionRequest, payRequest, changeProps, newsRequest } from './actions';

import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  FormGroup,
  Label,
  Input,
  Button,
  ModalHeader,
  Modal,
  ModalBody,
  ModalFooter,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

import { animateScroll as scroll} from 'react-scroll'

export class FrontendPortaforlioProjectPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    let { history, user } = this.props
    this.goDashboard (user, history)
    const {projectId} = this.props.match.params
    if (projectId) {
      this.props.onChangeProps({projectId})
      this.props.onInversionRequest()
      this.props.onPayRequest()
      this.props.onNewsRequest()
    }
  }
  componentWillReceiveProps (nextProps) {
    let { history } = this.props
    this.goDashboard(nextProps.user, history)
  }
  goDashboard (user, history) {
    if (!user) {
      history.push('/iniciar-sesion')
      scroll.scrollToTop();
    }
  }
  render() {
    const { incersionLoading, inversion, inversionError, payLoading, pay, payError, news, newsLoading, newsError} = this.props.frontendportaforlioprojectpage
    return (
      <div className="app animated fadeIn" style={{marginLeft: 40, marginRight: 40}}>
        <Row>
          <Col xs="12">
            <div style={{height: 50}} />
          </Col>
          <Col xs="12 text-center">
            <h1>Dinero Invertido</h1>
          </Col>
          <Col xs="12 text-center">
            <ReactTable
              data={inversion}
              defaultPageSize={5}
              filterable
              defaultFilterMethod={(filter, row) =>
                String(row[filter.id]) === filter.value}
              columns={[{
                Header: 'Monto',
                accessor: 'value',
                style: { textAlign: 'center' },
                filterMethod: (filter, row) => {
                  return row[filter.id].name.indexOf(filter.value) > -1
                },
                Cell: row => (
                  <div>
                    {`$ ${row.value}`}
                  </div>
                )
              },{
                Header: 'Estado',
                accessor: 'status',
                filterMethod: (filter, row) => {
                  if (filter.value === "all") {
                    return true;
                  }
                  return row[filter.id] === filter.value
                },
                Filter: ({ filter, onChange }) =>
                  <select
                    onChange={event => onChange(event.target.value)}
                    style={{ width: "100%" }}
                    value={filter ? filter.value : "all"}
                  >
                    <option value="all">Mostrar Todo</option>
                    <option value="0">No activo</option>
                    <option value="1">Activo</option>
                  </select>
                ,
                Cell: row => (
                  <span >
                    <span style={{
                      color: row.value === '0' ? '#ff0000'
                      : row.value === '1' ? '#00ff00'
                      : '#000000',
                      transition: 'all .3s ease'
                    }}>
                      &#x25cf;
                    </span> {
                      row.value === '0' ? 'No activo'
                      : row.value === '1' ? `Activo`
                      : 'Indefinido'
                    }
                  </span>
                )
              }]}
              className="-striped -highlight"
              previousText='Anterior'
              nextText='Siguiente'
              loadingText='Cargando...'
              noDataText='No hay datos'
              pageText='Página'
              ofText='de'
              rowsText='filas'
            />
          </Col>
          <Col xs="12 text-center">
            <h1>Dinero retornado</h1>
          </Col>
          <Col xs="12 text-center">
          <ReactTable
              data={pay}
              defaultPageSize={5}
              filterable
              defaultFilterMethod={(filter, row) =>
                String(row[filter.id]) === filter.value}
              columns={[{
                Header: 'Monto',
                accessor: 'value',
                style: { textAlign: 'center' },
                filterMethod: (filter, row) => {
                  return row[filter.id].name.indexOf(filter.value) > -1
                },
                Cell: row => (
                  <div>
                    {`$ ${row.value}`}
                  </div>
                )
              },{
                Header: 'Estado',
                accessor: 'status',
                filterMethod: (filter, row) => {
                  if (filter.value === "all") {
                    return true;
                  }
                  return row[filter.id] === filter.value
                },
                Filter: ({ filter, onChange }) =>
                  <select
                    onChange={event => onChange(event.target.value)}
                    style={{ width: "100%" }}
                    value={filter ? filter.value : "all"}
                  >
                    <option value="all">Mostrar Todo</option>
                    <option value="0">No activo</option>
                    <option value="1">Activo</option>
                  </select>
                ,
                Cell: row => (
                  <span >
                    <span style={{
                      color: row.value === '0' ? '#ff0000'
                      : row.value === '1' ? '#00ff00'
                      : '#000000',
                      transition: 'all .3s ease'
                    }}>
                      &#x25cf;
                    </span> {
                      row.value === '0' ? 'No activo'
                      : row.value === '1' ? `Activo`
                      : 'Indefinido'
                    }
                  </span>
                )
              }]}
              className="-striped -highlight"
              previousText='Anterior'
              nextText='Siguiente'
              loadingText='Cargando...'
              noDataText='No hay datos'
              pageText='Página'
              ofText='de'
              rowsText='filas'
            />
          </Col>
          <Col xs="12 text-center">
            <h1>Mensajes</h1>
          </Col>
          <Col xs="12 text-center">
            <ReactTable
              data={news}
              defaultPageSize={5}
              columns={[{
                accessor: 'text',
                style: { textAlign: 'center' }
              }]}
              className="-striped -highlight"
              previousText='Anterior'
              nextText='Siguiente'
              loadingText='Cargando...'
              noDataText='No hay datos'
              pageText='Página'
              ofText='de'
              rowsText='filas'
            />
          </Col>
        </Row>
      </div>
    );
  }
}

FrontendPortaforlioProjectPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  frontendportaforlioprojectpage: makeSelectFrontendPortaforlioProjectPage(),
  user: makeSelectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onInversionRequest: () => dispatch(inversionRequest()),
    onPayRequest: () => dispatch(payRequest()),
    onChangeProps: (object) => dispatch(changeProps(object)),
    onNewsRequest: () => dispatch(newsRequest())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'frontendPortaforlioProjectPage', reducer });
const withSaga = injectSaga({ key: 'frontendPortaforlioProjectPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(FrontendPortaforlioProjectPage);
