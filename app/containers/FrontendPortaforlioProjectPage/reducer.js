/*
 *
 * FrontendPortaforlioProjectPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  FPPPI_REQUEST,
  FPPPI_SUCCESS,
  FPPPI_ERROR,
  FPPPP_REQUEST,
  FPPPP_SUCCESS,
  FPPPP_ERROR,
  FPPP_CHANGE_PROPS,
  FPPPM_REQUEST,
  FPPPM_SUCCESS,
  FPPPM_ERROR,
} from './constants';

const initialState = fromJS({
  projectId: null,
  incersionLoading: false,
  inversion: [],
  inversionError: null,
  payLoading: false,
  pay: [],
  payError: null,
  newsLoading: false,
  news: [],
  newsError: null
});

function frontendPortaforlioProjectPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case FPPP_CHANGE_PROPS:
      let copyState = state.toJS()
      let { object } = action
      for (const key in object) {
        if (object.hasOwnProperty(key)) {
          copyState[key] = object[key];
        }
      }
      return fromJS(copyState) 
    case FPPPI_REQUEST:
      return state
      .set('incersionLoading', true)
      .set('inversion', [])
      .set('inversionError', null)
    case FPPPI_SUCCESS:
      return state
      .set('incersionLoading', false)
      .set('inversion', fromJS(action.payload))
      .set('inversionError', null)
    case FPPPI_ERROR:
      return state
      .set('incersionLoading', false)
      .set('inversion', [])
      .set('inversionError', action.error)
    case FPPPP_REQUEST:
      return state
      .set('payLoading', true)
      .set('pay', [])
      .set('payError', null)
    case FPPPP_SUCCESS:
      return state
      .set('payLoading', false)
      .set('pay', fromJS(action.payload))
      .set('payError', null)
    case FPPPP_ERROR:
      return state
      .set('payLoading', false)
      .set('pay', [])
      .set('payError', action.error)
    case FPPPM_REQUEST:
      return state
      .set('newsLoading', true)
      .set('news', [])
      .set('newsError', null)
    case FPPPM_SUCCESS:
      return state
      .set('newsLoading', false)
      .set('news', fromJS(action.payload))
      .set('newsError', null)
    case FPPPM_ERROR:
      return state
      .set('newsLoading', false)
      .set('news', [])
      .set('newsError', action.error)
    default:
      return state;
  }
}

export default frontendPortaforlioProjectPageReducer;
