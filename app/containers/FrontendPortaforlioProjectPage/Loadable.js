/**
 *
 * Asynchronously loads the component for FrontendPortaforlioProjectPage
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
