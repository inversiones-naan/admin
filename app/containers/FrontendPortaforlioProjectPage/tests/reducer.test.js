
import { fromJS } from 'immutable';
import frontendPortaforlioProjectPageReducer from '../reducer';

describe('frontendPortaforlioProjectPageReducer', () => {
  it('returns the initial state', () => {
    expect(frontendPortaforlioProjectPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
