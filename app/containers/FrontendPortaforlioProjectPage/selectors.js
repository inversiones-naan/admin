import { createSelector } from 'reselect';

/**
 * Direct selector to the frontendPortaforlioProjectPage state domain
 */
const selectFrontendPortaforlioProjectPageDomain = (state) => state.get('frontendPortaforlioProjectPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by FrontendPortaforlioProjectPage
 */

const makeSelectFrontendPortaforlioProjectPage = () => createSelector(
  selectFrontendPortaforlioProjectPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectFrontendPortaforlioProjectPage;
export {
  selectFrontendPortaforlioProjectPageDomain,
};
