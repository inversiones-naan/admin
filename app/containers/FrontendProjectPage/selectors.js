import { createSelector } from 'reselect';

/**
 * Direct selector to the frontendProjectPage state domain
 */
const selectFrontendProjectPageDomain = (state) => state.get('frontendProjectPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by FrontendProjectPage
 */

const makeSelectFrontendProjectPage = () => createSelector(
  selectFrontendProjectPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectFrontendProjectPage;
export {
  selectFrontendProjectPageDomain,
};
