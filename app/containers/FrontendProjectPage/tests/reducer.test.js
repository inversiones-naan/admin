
import { fromJS } from 'immutable';
import frontendProjectPageReducer from '../reducer';

describe('frontendProjectPageReducer', () => {
  it('returns the initial state', () => {
    expect(frontendProjectPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
