/*
 *
 * FrontendProjectPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PROJECT_REQUEST,
  PROJECT_SUCCESS,
  PROJECT_ERROR,
  PROJECT_INVESTMENT_SENDING,
  PROJECT_INVESTMENT_SUCCESS,
  PROJECT_INVESTMENT_ERROR,
  PROJECT_INVESTMENT_RESET,
  PROJECT_CHANGE_PROP
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}


/**
 * Solicita los proyectos al API
 *
 * @return {object}    An action object with a type of PROJECT_REQUEST
 */
export function projectRequest(projectId) {
  return {
    type: PROJECT_REQUEST,
    projectId
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of PROJECT_SUCCESS
 */
export function projectSuccess(payload) {
  return {
    type: PROJECT_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of PROJECT_ERROR
 */
export function projectError(error) {
  return {
    type: PROJECT_ERROR,
    error,
  };
}

/**
 * Enviar la cantidad a invertir
 *
 * @return {object} An action object with a type of PROJECT_ERROR
 */
export function sentInvestment(amount) {
  return {
    type: PROJECT_INVESTMENT_SENDING,
    amount,
  };
}

/**
 * Cantidad enviada correctamente
 *
 * @return {object} An action object with a type of PROJECT_INVESTMENT_SUCCESS
 */
export function sentInvestmentSuccess() {
  return {
    type: PROJECT_INVESTMENT_SUCCESS,
  };
}

/**
 * No se pudo enviar la cantidad
 *
 * @return {object} An action object with a type of PROJECT_INVESTMENT_ERROR
 */
export function sentInvestmentError(error) {
  return {
    type: PROJECT_INVESTMENT_ERROR,
    error
  };
}

/**
 * Reiniciar el modal
 *
 * @return {object} An action object with a type of PROJECT_INVESTMENT_RESET
 */
export function sentInvestmentReset() {
  return {
    type: PROJECT_INVESTMENT_RESET
  };
}

/**
 * Cambiar el valor de alguna propiedad del reducer
 *
 * @return {object} An action object with a type of PROJECT_CHANGE_PROP
 */
export function changeProp(prop, value) {
  return {
    type: PROJECT_CHANGE_PROP,
    prop,
    value
  };
}
