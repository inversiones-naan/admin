/*
 *
 * FrontendProjectPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  PROJECT_REQUEST,
  PROJECT_SUCCESS,
  PROJECT_ERROR,
  PROJECT_INVESTMENT_SENDING,
  PROJECT_INVESTMENT_SUCCESS,
  PROJECT_INVESTMENT_ERROR,
  PROJECT_INVESTMENT_RESET,
  PROJECT_CHANGE_PROP
} from './constants';
import { PROFILE_SUCCESS, PROFILE_ERROR } from '../FrontendProfilePage/constants';

const initialState = fromJS({
  projectId: '',
  project: {
    id: '',
    name: '',
    images:[],
    progressFunding: '',
    funding: '',
    minimumInvestment: '',
    instrument: {
      name: '',
    },
    investmentProgress: '',
    fixedAnnualRate: '',
    term: '',
    possibilityOfPrepaid: '',
    description: '',
    builder: {
      name: '',
      urlImage: '',
      description: '',
      url: ''
    },
    investment: '',
    address: '',
    googleLat: 21.1635004,
    googleLng: -86.8208006
  },
  amount: '',
  amountLoading: false,
  amountSuccess: null,
  amountError: null
});

function frontendProjectPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PROJECT_REQUEST:
      return state
      .set('projectId', action.projectId)
    case PROJECT_SUCCESS:
      action.payload.googleLat = parseFloat(action.payload.googleLat)
      action.payload.googleLng = parseFloat(action.payload.googleLng)
      return state
      .set('project', fromJS(action.payload))
    case PROJECT_ERROR:
      return state
    case PROJECT_INVESTMENT_SENDING:
      return state
      .set('amountLoading', true)
      .set('amountSuccess', null)
      .set('amountError', null)
    case PROJECT_INVESTMENT_SUCCESS:
      return state
      .set('amountLoading', false)
      .set('amountSuccess', true)
      .set('amountError', null)
    case PROJECT_INVESTMENT_ERROR:
      return state
      .set('amountLoading', false)
      .set('amountSuccess', null)
      .set('amountError', action.error)
    case PROJECT_INVESTMENT_RESET:
      return state
      .set('amount', '')
      .set('amountLoading', false)
      .set('amountSuccess', null)
      .set('amountError', null)
    case PROJECT_CHANGE_PROP:
      return state
      .set(action.prop, action.value)
    default:
      return state;
  }
}

export default frontendProjectPageReducer;
