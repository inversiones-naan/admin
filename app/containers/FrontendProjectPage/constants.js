/*
 *
 * FrontendProjectPage constants
 *
 */

export const DEFAULT_ACTION = 'app/FrontendProjectPage/DEFAULT_ACTION';

export const PROJECT_REQUEST = 'app/FrontendProjectPage/PROJECTS_REQUEST';
export const PROJECT_SUCCESS = 'app/FrontendProjectPage/PROJECTS_SUCCESS';
export const PROJECT_ERROR = 'app/FrontendProjectPage/PROJECTS_ERROR';

export const PROJECT_INVESTMENT_SENDING = 'app/FrontendProjectPage/PROJECT_INVESTMENT_SENDING';
export const PROJECT_INVESTMENT_SUCCESS = 'app/FrontendProjectPage/PROJECT_INVESTMENT_SUCCESS';
export const PROJECT_INVESTMENT_ERROR = 'app/FrontendProjectPage/PROJECT_INVESTMENT_ERROR';

export const PROJECT_INVESTMENT_RESET = 'app/FrontendProjectPage/PROJECT_INVESTMENT_RESET';

export const PROJECT_CHANGE_PROP = 'app/FrontendProjectPage/PROJECT_CHANGE_PROP';
