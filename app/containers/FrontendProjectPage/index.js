/**
 *
 * FrontendProjectPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectFrontendProjectPage from './selectors';
import { makeSelectUser } from 'containers/App/selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { animateScroll as scroll} from 'react-scroll'

import { projectRequest, sentInvestmentReset, changeProp, sentInvestment } from './actions';

import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  FormGroup,
  Label,
  Input,
  Button,
  ModalHeader,
  Modal,
  ModalBody,
  ModalFooter,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";

import Slider from 'react-slick';

import ProgressBar from 'react-progressbar.js'
import LoadingIndicator from 'components/LoadingIndicator';

import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";

import params from 'utils/params';

const MapWithAMarker = withScriptjs(withGoogleMap(props =>
  <GoogleMap
    defaultZoom={15}
    defaultCenter={{ lat: props.googleLat, lng: props.googleLng }}
  >
    <Marker
      position={{ lat: props.googleLat, lng: props.googleLng }}
    />
  </GoogleMap>
));

export class FrontendProjectPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
  }
  componentDidMount () {
    let { history, user } = this.props
    this.goDashboard (user, history)
    const {projectId} = this.props.match.params
    if (projectId) {
      this.props.onProjectRequest(projectId)
    }
    this.props.onSentInvestmentReset()
  }
  componentWillReceiveProps (nextProps) {
    let { history } = this.props
    this.goDashboard(nextProps.user, history)
  }
  goDashboard (user, history) {
    if (!user) {
      history.push('/iniciar-sesion')
      scroll.scrollToTop();
    }
  }
  toggleModal () {
    this.setState({modal: !this.state.modal})
  }
  render() {
    const {project, amountLoading, amountSuccess, amountError, amount} = this.props.frontendprojectpage
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 2000,
      slidesToShow: 3,
      slidesToScroll: 3,
      initialSlide: 0,
      responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
        }
      }, {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1
        }
      },{
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    };
    const {images, id, name, builder, address, progressFunding, investmentProgress, fixedAnnualRate, term, possibilityOfPrepaid, instrument, funding, minimumInvestment, status, description, builderDescription, investment, builderUrl, googleLat, googleLng} = project
    
    return (
      <div className="animated fadeIn project-index">
        <div className="container" style={{maxWidth: 1800, paddingLeft: 40, paddingRight: 40}}>
          <Row>
            <Col xs="12">
              <div className="main-title">
                <h2 style={{marginTop: '6rem'}}>{name}</h2>
              </div>
            </Col>
            <Col xs="12">              
              <Slider {...settings}>
                {images.map((image, i) => {
                  return (
                    <img src={`${params.apiUrl}${image.url}`} key={`image-${i}`} style={{height: 300}}/>
                  )
                })}
              </Slider>
            </Col>
            <Col xs="12">
              <div className="main-title">
                <h2></h2>
              </div>
            </Col>
            <Col xs="12" style={{marginTop: '3rem'}}>
              <ProgressBar.Line
                progress={progressFunding/100}
                text={'test'}
                options={{
                  strokeWidth: 2,
                  easing: 'easeInOut',
                  duration: 3000,
                  color: '#f63a0f',
                  trailColor: '#eee',
                  trailWidth: 1,
                  svgStyle: {width: '100%', height: '100%'},
                  text: {
                    style: {
                      // Text color.
                      // Default: same as stroke color (options.color)
                      color: '#999',
                      position: 'absolute',
                      right: '0',
                      // top: '30px',
                      padding: 0,
                      margin: 0,
                      transform: null
                    },
                    autoStyleContainer: false
                  },
                  from: {color: '#1B6AA9'},
                  to: {color: '#4AC3E9'},
                  step: (state, bar) => {
                    bar.setText(Math.round(bar.value() * 100) + ' %');
                    bar.path.setAttribute('stroke', state.color);
                  }
                }}
                initialAnimate={true}
                containerClassName={'.progressbar'}
              />
            </Col>
            <Col xs="12" style={{marginTop: '5rem'}}>
              <div className="main-title">
                <h2 >Ubicación</h2>
                <h3>{address}</h3>
              </div>
            </Col>
            <Col xs="12" style={{height: 350}}>
            {/* AIzaSyBr7BDLXe0-FZLbPNORhhjmj7H-3uL8pm8 */}
              <MapWithAMarker
                googleMapURL={'https://maps.googleapis.com/maps/api/js?key=AIzaSyBr7BDLXe0-FZLbPNORhhjmj7H-3uL8pm8&v=3.exp&libraries=geometry,drawing,places'}
                googleLat={googleLat}
                googleLng={googleLng}
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `400px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
              />
            </Col>
            <Col xs="12" style={{marginTop: '5rem'}}>
              <div className="main-title">
                <h2>Especificaciones</h2>
              </div>
            </Col>
            <Col xs="12">
              <Table responsive>
                <tbody>
                  <tr>
                    <td style={{paddingLeft: 40}}>Monto Requerido</td>
                    <td style={{textAlign: 'right', paddingRight: 40}}>{funding}</td>
                  </tr>
                  <tr>
                    <td style={{paddingLeft: 40}}>Inversión mínima</td>
                    <td style={{textAlign: 'right', paddingRight: 40}}>{minimumInvestment}</td>
                  </tr>
                  <tr>
                    <td style={{paddingLeft: 40}}>Insturmento</td>
                    <td style={{textAlign: 'right', paddingRight: 40}}>{instrument.name}</td>
                  </tr>
                  <tr>
                    <td style={{paddingLeft: 40}}>Rendimiento</td>
                    <td style={{textAlign: 'right', paddingRight: 40}}>{fixedAnnualRate}</td>
                  </tr>
                  <tr>
                    <td style={{paddingLeft: 40}}>Plazo</td>
                    <td style={{textAlign: 'right', paddingRight: 40}}>{term}</td>
                  </tr>
                  <tr>
                    <td style={{paddingLeft: 40}}>Posibilidad de Prepago</td>
                    <td style={{textAlign: 'right', paddingRight: 40}}>{possibilityOfPrepaid}</td>
                  </tr>                  
                </tbody>
              </Table>
            </Col>
            <Col xs="12">
              <div className="main-title">
                <h2 style={{marginTop: '3rem'}}>Descripción</h2>
              </div>
            </Col>
            <Col xs="12">
              <p >
                {description}
              </p>
            </Col>
            <Col xs="12">
              <div className="main-title">
                <h2 style={{marginTop: '3rem'}}>Constructor</h2>
              </div>
            </Col>
            <Col md="12">
              <Row>
                <Col xs="12" md="4" lg="3" style={{height: 200, width: 200, textAlign: 'center'}}>
                  <img src={`${params.apiUrl}${builder.urlImage}`} style={{height: 200, width: 200}}/>
                </Col>
                <Col xs="12" md="8" lg="9">
                  <p >
                    {description}
                  </p>
                </Col>
                {builder.url
                ? <Col xs="12" style={{textAlign: 'center', fontSize: '1rem'}}>
                  <a href={builder.url} target="_blank">
                    <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper" onClick={this.scrollToTop}>Saber más</button>
                  </a>
                </Col>
                :null}                
              </Row>
            </Col>
            <Col xs="12" style={{marginTop: '1rem'}}>
              <div className="main-title">
                <h2 >Inversión</h2>
              </div>
            </Col>
            <Col xs="12">
              <p >
                {investment}
              </p>
            </Col>
            <Col xs="12" style={{position: 'fixed', bottom: 60, textAlign: 'center', left: 0, zIndex: 200, fontSize: '1rem'}}>
              <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper" onClick={this.toggleModal.bind(this)}>Invierte aqui</button>
            </Col>
          </Row>
        </div>
        <Modal isOpen={this.state.modal} toggle={this.toggleModal.bind(this)} className={'modal-md'}>
          {amountLoading
          ? <ModalBody>
            <div style={{height: 130}}>
              <LoadingIndicator />
            </div>
          </ModalBody>
          :amountSuccess
          ? <ModalBody>
            <CardBlock className="card-body p-4">
              <div className={'text-center'}>
                <h1 className="titulo">Monto Registrado</h1>
                <i className="fa fa-money icons font-5xl d-block mt-4"></i>
                <br/>
                <p className="texto1">Se ha registrado el monto de inversión para este proyecto.</p>
                <br/>
                <p className="texto1">
                  Uno de nuestros ajentes se contactará contigo para continuar con el proceso.
                </p>
                <Button outline color="secondary" size="lg" block onClick={this.props.onSentInvestmentReset}>Volver</Button>
              </div>
            </CardBlock>
          </ModalBody>
          :amountError
          ? <ModalBody>
            <Card className="border-danger">
              <CardBlock className="card-body p-4">
                <div className={'text-center'}>
                  <h1 className="titulo">Error al registrar el monto</h1>
                  <i className="fa fa-ban icons font-5xl d-block mt-4"></i>
                  <br/>
                  <p className="texto1">Ha ocurrido un problema al intentar registrar la inversión en este proyecto.</p>
                  <br/>
                  <Card className="text-white bg-danger">
                    <CardBlock className="card-body">
                      <p className="texto1">
                        {amountError}
                      </p>
                    </CardBlock>
                  </Card>                  
                  <Button outline color="secondary" size="lg" block onClick={this.props.onSentInvestmentReset}>Volver</Button>
                </div>
              </CardBlock>
            </Card>            
          </ModalBody>
          : <ModalBody>
            <h4 style={{textAlign: 'center', marginTop: '3rem'}} >¿Cuanto desea invertir?</h4>
            <br/>
            <InputGroup className="mb-3">
              <InputGroupAddon><i className="fa fa-dollar"></i></InputGroupAddon>
              <Input
                ref="investment"
                type="text"
                value={amount}
                placeholder="Ingresa el monto"
                onChange={(evt)=> this.props.onChangeProp('amount', evt.target.value)}
                onKeyDown={(evt) => {
                  let keyCode = evt.keyCode || evt.which
                  if (evt.shiftKey === false && keyCode === 13) {
                    evt.preventDefault()
                    // let el = ReactDOM.findDOMNode(this.refs.password)
                    // el.focus()
                  }
                }}
              />
            </InputGroup>
            <Col xs="12" style={{textAlign: 'center', fontSize: '1rem'}}>
              <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper" onClick={this.props.onSentInvestment}>Confirmar</button>
            </Col>
          </ModalBody>
          }
        </Modal>
      </div>
    );
  }
}

FrontendProjectPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  frontendprojectpage: makeSelectFrontendProjectPage(),
  user: makeSelectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onProjectRequest: (projectId) => dispatch(projectRequest(projectId)),
    onSentInvestment: () => dispatch(sentInvestment()),
    onSentInvestmentReset: () => dispatch(sentInvestmentReset()),
    onChangeProp: (prop, value) => dispatch(changeProp(prop, value))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'frontendProjectPage', reducer });
const withSaga = injectSaga({ key: 'frontendProjectPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(FrontendProjectPage);
