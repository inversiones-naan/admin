import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PROJECT_REQUEST, PROJECT_INVESTMENT_SENDING } from './constants';
import { projectSuccess, projectError, sentInvestmentSuccess, sentInvestmentError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectFrontendProjectPage from './selectors';
import { makeSelectToken, makeSelectUser } from 'containers/App/selectors';
import { logout } from 'containers/App/actions';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PROJECT_REQUEST, getProject);
  yield takeLatest(PROJECT_INVESTMENT_SENDING, sendAmount);
}

/**
 * Obtener los proyectos de la API
 */
export function* getProject() {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(fundingError('Error token'));
    return
  }
  // Select authToken from store
  const frontendprojectpage = yield select(makeSelectFrontendProjectPage());
  const {projectId} = frontendprojectpage

  const requestURL = `${params.apiUrl}projects/${projectId}?include=images,builder,instrument&where={"status":{"gt":0}}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      if (result.error.message === 'Authentication: Login Failed') {
        yield put(logout());
      } else {
        yield put(projectError(result.error.message));        
      }
    } else {
      yield put(projectSuccess(result.project));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectError(err));
  }
}


/**
 * Obtener los proyectos de la API
 */
export function* sendAmount() {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(fundingError('Error token'));
    return
  }
  const user = yield select(makeSelectUser())
  // Select authToken from store
  const frontendprojectpage = yield select(makeSelectFrontendProjectPage());
  const {projectId, amount} = frontendprojectpage

  const payload = {
    projectId: projectId,
    userId: user.id,
    creatorId: user.id,
    type: 0,
    value: amount,
    emailing: true // Esta variable indica que se informará que el proceso se realizo
    // status: 0// Al crear por defecto el monto no esta activado
  }
  const requestURL = `${params.apiUrl}funding`;
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`,
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    console.log(result)
    if (result.error) {
      if (result.error.message === 'Authentication: Login Failed') {
        yield put(logout());
      } else {
        yield put(sentInvestmentError(result.error.message));
      }
    } else {
      yield put(sentInvestmentSuccess(result.project));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(sentInvestmentError(err));
  }
}