/*
 * FrontendProjectPage Messages
 *
 * This contains all the text for the FrontendProjectPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.FrontendProjectPage.header',
    defaultMessage: 'This is FrontendProjectPage container !',
  },
});
