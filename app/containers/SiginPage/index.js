/**
 *
 * SiginPage
 *
 */

import React from 'react';
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectSiginPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Container,
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";

import { changeProp, siginRequest, loginReset } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

import 'react-widgets/dist/css/react-widgets.css'
import { Calendar } from 'react-widgets'

import moment from 'moment'

import { Link } from 'react-router-dom';

export class SiginPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.props.onReset()
  }
  submit(evt) {
    let keyCode = evt.keyCode || evt.which
    if (evt.shiftKey === false && keyCode === 13) {
      this.props.onSigin()
    }
  }
  render() {
    const {siginpage} = this.props
    return (
      <div className="app flex-row align-items-center site-header-background">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                {siginpage.loading
                ?<LoadingIndicator />
                :null}
                {siginpage.error
                ? <Card className="text-white bg-danger">
                    <CardBlock className="card-body">
                      {siginpage.error}
                    </CardBlock>
                  </Card>                  
                : null}
                { siginpage.success
                ? <CardBlock className="card-body p-4">
                    <div className={'text-center'}>
                      <h1 className="titulo">Confirma tu correo</h1>
                      <i className="icon-envelope-letter icons font-5xl d-block mt-4"></i>
                      <br/>
                      <p className="texto1">Te hemos enviado un correo a <b>{`${siginpage.email}`}</b></p>
                      <p className="texto1">
                        Por favor sigue las instrucciones para activar tu cuenta.
                      </p>
                      <p className="texto2">
                        <i>*Si no llega tu correo en la siguiente hora, revisa la carpeta de correo no deseado (SPAM)</i>
                      </p>
                      <Button outline color="secondary" size="lg" block onClick={this.props.onReset}>Reiniciar</Button>
                    </div>
                  </CardBlock>         
                : <CardBlock className="card-body p-4">
                  <h1 className='text-center'>Registrate</h1>
                  <p className="text-muted">Inicia a invertir con Rendex</p>
                  <InputGroup className="mb-3">
                    <InputGroupAddon><i className="icon-envelope"></i></InputGroupAddon>
                    <Input
                      ref="email"
                      type="text"
                      placeholder="Correo"
                      value={siginpage.email}
                      onChange={(evt) => {
                        this.props.onChangeProp('email', evt.target.value)
                      }}
                      onKeyDown={(evt) => {
                        let keyCode = evt.keyCode || evt.which
                        if (evt.shiftKey === false && keyCode === 13) {
                          evt.preventDefault()
                          let el = ReactDOM.findDOMNode(this.refs.password)
                          el.focus()
                        }
                      }}
                    />
                  </InputGroup>
                  {/* <InputGroup className="mb-3">
                    <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                    <Input
                      ref="lastName"
                      type="text"
                      placeholder="Apellidos"
                      value={siginpage.lastName}
                      onChange={(evt) => {
                        this.props.onChangeProp('lastName', evt.target.value)
                      }}
                      onKeyDown={(evt) => {
                        let keyCode = evt.keyCode || evt.which
                        if (evt.shiftKey === false && keyCode === 13) {
                          evt.preventDefault()
                          let el = ReactDOM.findDOMNode(this.refs.birthdate)
                          el.focus()
                        }
                      }}
                    />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon><i className="icon-calendar"></i></InputGroupAddon>
                    <Input
                      ref="birthdate"
                      type="text"
                      placeholder="Fecha de nacimiento"
                      onFocus={()=>this.props.onChangeProp('modalCalendar', true)}
                      value={moment(siginpage.birthdate).format('DD MMM YYYY')}
                      onKeyDown={(evt) => {
                        let keyCode = evt.keyCode || evt.which
                        if (evt.shiftKey === false && keyCode === 13) {
                          evt.preventDefault()
                          let el = ReactDOM.findDOMNode(this.refs.nationality)
                          el.focus()
                        }
                      }}
                    />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon><i className="icon-flag"></i></InputGroupAddon>
                    <Input
                      ref="nationality"
                      type="select"
                      name="nationality"
                      id="nationality"
                      value={siginpage.nationality}
                      onChange={(evt) => {
                        this.props.onChangeProp('nationality', evt.target.value)
                      }}
                      onKeyDown={(evt) => {
                        let keyCode = evt.keyCode || evt.which
                        if (evt.shiftKey === false && keyCode === 13) {
                          evt.preventDefault()
                          let el = ReactDOM.findDOMNode(this.refs.qualifiedInvestor)
                          el.focus()
                        }
                      }}
                    >
                      <option value="0">Mexicano</option>
                      <option value="1">Extranjero</option>
                    </Input>
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon>Inversionista calificado</InputGroupAddon>
                    <Input
                      ref="qualifiedInvestor"
                      type="select"
                      name="qualifiedInvestor"
                      id="qualifiedInvestor"
                      value={siginpage.qualifiedInvestor}
                      onChange={(evt) => {
                        this.props.onChangeProp('qualifiedInvestor', evt.target.value)
                      }}
                      onKeyDown={(evt) => {
                        let keyCode = evt.keyCode || evt.which
                        if (evt.shiftKey === false && keyCode === 13) {
                          evt.preventDefault()
                          let el = ReactDOM.findDOMNode(this.refs.rfc)
                          el.focus()
                        }
                      }}
                    >
                      <option value="0">No calificado</option>
                      <option value="1">Calificado</option>
                    </Input>
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon>RFC</InputGroupAddon>
                    <Input
                      ref="rfc"
                      type="text"
                      placeholder="RFC"
                      value={siginpage.rfc}
                      onChange={(evt) => {
                        this.props.onChangeProp('rfc', evt.target.value)
                      }}
                      onKeyDown={(evt) => {
                        let keyCode = evt.keyCode || evt.which
                        if (evt.shiftKey === false && keyCode === 13) {
                          evt.preventDefault()
                          let el = ReactDOM.findDOMNode(this.refs.email)
                          el.focus()
                        }
                      }}
                    />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon>@</InputGroupAddon>
                    <Input
                      ref="email"
                      type="text"
                      placeholder="Correo electronico"
                      value={siginpage.email}
                      onChange={(evt) => {
                        this.props.onChangeProp('email', evt.target.value)
                      }}
                      onKeyDown={(evt) => {
                        let keyCode = evt.keyCode || evt.which
                        if (evt.shiftKey === false && keyCode === 13) {
                          evt.preventDefault()
                          let el = ReactDOM.findDOMNode(this.refs.password)
                          el.focus()
                        }
                      }}
                    />
                  </InputGroup> */}
                  <InputGroup className="mb-3">
                    <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                    <Input
                      ref="password"
                      type="password"
                      placeholder="Contraseña"
                      value={siginpage.password}
                      onChange={(evt) => {
                        this.props.onChangeProp('password', evt.target.value)
                      }}
                      onKeyDown={(evt) => {
                        let keyCode = evt.keyCode || evt.which
                        if (evt.shiftKey === false && keyCode === 13) {
                          evt.preventDefault()
                          let el = ReactDOM.findDOMNode(this.refs.rePassword)
                          el.focus()
                        }
                      }}
                    />
                  </InputGroup>
                  <InputGroup className="mb-4">
                    <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                    <Input
                      ref="rePassword"
                      type="password"
                      placeholder="Repetir contraseña"
                      value={siginpage.rePassword}
                      onChange={(evt) => {
                        this.props.onChangeProp('rePassword', evt.target.value)
                      }}
                      onKeyDown={this.submit.bind(this)}
                    />
                  </InputGroup>
                  <Button
                    color="success"
                    block
                    onClick={this.props.onSigin}
                  >
                    Crear cuenta
                  </Button>
                  <div className="col-sm-12 form-group">
                    <br/>
                    <Link to={`/iniciar-sesion`}>Iniciar sesión</Link>
                    <br/>
                    <Link to={`/new-password`}>¿Olvidaste tu contraseña?</Link>
                    <br/>
                    <Link to={`/new-activate`}>¿No recibiste instrucciones para confirmar tu cuenta?</Link>
                    <br/>
                  </div>
                </CardBlock>
                }
              </Card>
              }
            </Col>
          </Row>
        </Container>
        <Modal
          isOpen={siginpage.modalCalendar}
          toggle={()=>{this.props.onChangeProp('modalCalendar', false)}}
          className={'modal-sm'}
        >
          <ModalHeader
            toggle={()=>{this.props.onChangeProp('modalCalendar', false)}}
          >
            Seleccione la fecha
          </ModalHeader>
          <ModalBody>
            <Calendar
              onChange={value => {
                this.props.onChangeProp('modalCalendar', false)
                this.props.onChangeProp('birthdate', value)
              }}
            />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

SiginPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  siginpage: makeSelectSiginPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onChangeProp: (prop, value) => dispatch(changeProp(prop, value)),
    onSigin: () => dispatch(siginRequest()),
    onReset: () => dispatch(loginReset())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'siginPage', reducer });
const withSaga = injectSaga({ key: 'siginPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(SiginPage);
