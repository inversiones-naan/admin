/*
 *
 * SiginPage constants
 *
 */

export const DEFAULT_ACTION = 'app/SiginPage/DEFAULT_ACTION';
export const SIGIN_REQUEST = 'app/SiginPage/SIGIN_REQUEST';
export const SIGIN_SUCCESS = 'app/SiginPage/SIGIN_SUCCESS';
export const SIGIN_ERROR = 'app/SiginPage/SIGIN_ERROR';

export const CHANGE_PROP = 'app/SiginPage/CHANGE_PROP';

export const RESET = 'app/SiginPage/RESET';

