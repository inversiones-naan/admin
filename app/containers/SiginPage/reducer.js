/*
 *
 * SiginPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SIGIN_REQUEST,
  SIGIN_SUCCESS,
  SIGIN_ERROR,
  CHANGE_PROP,
  PASSWORD_CLEAR,
  RESET,
} from './constants';

const initialState = fromJS({
  loading: false,
  error: false,
  success: false,
  // name: '',
  // lastName: '',
  // birthdate: new Date(),
  // nationality: 0,
  // qualifiedInvestor: 0,
  // rfc: '',
  email: '',
  password: '',
  rePassword: '',
  // modalCalendar: false
});

function siginPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case CHANGE_PROP:
      return state
      .set(action.prop, action.value)
    case SIGIN_REQUEST:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false)
    case SIGIN_SUCCESS:
      return state
      .set('loading', false)
      .set('success', true)
      .set('error', false)
    case SIGIN_ERROR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', action.error)
    case RESET:
      return initialState
    default:
      return state;
  }
}

export default siginPageReducer;
