
import { fromJS } from 'immutable';
import siginPageReducer from '../reducer';

describe('siginPageReducer', () => {
  it('returns the initial state', () => {
    expect(siginPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
