import { createSelector } from 'reselect';

/**
 * Direct selector to the siginPage state domain
 */
const selectSiginPageDomain = (state) => state.get('siginPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by SiginPage
 */

const makeSelectSiginPage = () => createSelector(
  selectSiginPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectSiginPage;
export {
  selectSiginPageDomain,
};
