/*
 * SiginPage Messages
 *
 * This contains all the text for the SiginPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.SiginPage.header',
    defaultMessage: 'This is SiginPage container !',
  },
});
