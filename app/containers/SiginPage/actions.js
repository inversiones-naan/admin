/*
 *
 * SiginPage actions
 *
 */

import {
  DEFAULT_ACTION,
  SIGIN_REQUEST,
  SIGIN_SUCCESS,
  SIGIN_ERROR,
  CHANGE_PROP,
  RESET,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/**
 * Crear cuenta del usuario
 *
 * @return {object}    An action object with a type of USERS_REQUEST
 */
export function siginRequest() {
  return {
    type: SIGIN_REQUEST
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of SIGIN_SUCCESS
 */
export function siginSuccess() {
  return {
    type: SIGIN_SUCCESS
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of SIGIN_ERROR
 */
export function siginError(error) {
  return {
    type: SIGIN_ERROR,
    error,
  };
}

/**
 * Cambia el valor de una propiedad
 *
 * @return {object} An action object with a type of CHANGE_PROP
 */
export function changeProp(prop, value) {
  return {
    type: CHANGE_PROP,
    prop,
    value
  };
}

/**
 * Reinica los valores
 *
 * @return {object} An action object with a type of RESET
 */
export function loginReset() {
  return {
    type: RESET,
  };
}
