import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { SIGIN_REQUEST } from './constants';
import { siginSuccess, siginError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import { makeSelectToken } from '../App/selectors';
import makeSelectSiginPage from './selectors';

import moment from 'moment'

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(SIGIN_REQUEST, createUser);
}

/**
 * Registrarse usando el API
 */
export function* createUser() {
  // Select authToken from store
  const siginpage = yield select(makeSelectSiginPage())
  if (siginpage.password.length < 8) {
    yield put(siginError('La contraseña al menos debe tener 8 caracteres'));
    return
  }
  if (siginpage.password !== siginpage.rePassword) {
    yield put(siginError('Las contraseñas no coinciden'));
    return
  }
  const payload = {
    // firstName: siginpage.name,
    // lastName: siginpage.lastName,
    // birthdate: moment(siginpage.birthdate).format('YYYY-MM-DD'),
    // nationality: siginpage.nationality,
    // qualifiedInvestor: siginpage.qualifiedInvestor,
    // rfc: siginpage.rfc,
    email: siginpage.email,
    password: siginpage.password
  }

  const requestURL = `${params.apiUrl}users/sigin`;
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    console.log(payload)
    if (payload.error) {
      yield put(siginError(payload.error.message));
    } else {
      yield put(siginSuccess());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(siginError('Ha ocurrido un error al momento de crear la cuenta'));
  }
}
