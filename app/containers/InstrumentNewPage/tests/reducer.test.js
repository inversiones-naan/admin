
import { fromJS } from 'immutable';
import instrumentNewPageReducer from '../reducer';

describe('instrumentNewPageReducer', () => {
  it('returns the initial state', () => {
    expect(instrumentNewPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
