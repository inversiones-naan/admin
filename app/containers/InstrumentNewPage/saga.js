import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { INSTRUMENT_CREATING, INSTRUMENT_REQUEST, INSTRUMENT_UPDATING } from './constants';
import { instrumentCreated, instrumentError, instrumentSuccess, instrumentUpdated } from './actions';
import makeSelectInstrumentNewPage from './selectors';
import { makeSelectToken } from '../App/selectors';
import request from 'utils/request';
import params from 'utils/params';
// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(INSTRUMENT_CREATING, createInstrument);
  yield takeLatest(INSTRUMENT_REQUEST, getInstrument);
  yield takeLatest(INSTRUMENT_UPDATING, updateInstrument);
}

export function* createInstrument () {

  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(instrumentError('Error token'));
    return
  }
  
  const instrumentnewpage = yield select(makeSelectInstrumentNewPage())
  const payload = {
    name: instrumentnewpage.name
  }
  const requestURL = `${params.apiUrl}instruments/`;
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`,
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(instrumentError(payload.error.message));
    } else {
      yield put(instrumentCreated());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(instrumentError('Error al guardar los datos'));
  }
}

export function* getInstrument () {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(instrumentError('Error token'));
    return
  }
  const instrumentnewpage = yield select(makeSelectInstrumentNewPage())
  const requestURL = `${params.apiUrl}instruments/${instrumentnewpage.id}`;
  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(instrumentError(payload.error.message));
    } else {
      yield put(instrumentSuccess(payload.instrument));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(instrumentError('Error al actualizar'));
  }
}

export function* updateInstrument () {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(instrumentError('Error token'));
    return
  }
  const instrumentnewpage = yield select(makeSelectInstrumentNewPage())
  const payload = {
    name: instrumentnewpage.name
  }
  const requestURL = `${params.apiUrl}instruments/${instrumentnewpage.id}`;
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(instrumentError(payload.error.message));
    } else {
      yield put(instrumentUpdated());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(instrumentError('Error al actualizar'));
  }
}