/*
 * InstrumentNewPage Messages
 *
 * This contains all the text for the InstrumentNewPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.InstrumentNewPage.header',
    defaultMessage: 'This is InstrumentNewPage container !',
  },
});
