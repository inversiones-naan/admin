/**
 *
 * InstrumentNewPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectInstrumentNewPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

import { instrumentChangeProp, instrumentCreating, instrumentRequest, instrumentUpdating, instrumentClear } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

export class InstrumentNewPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    const { instrumentId } = this.props.match.params
    if (instrumentId) {
      this.props.onInstrumentRequest(instrumentId)
    }
  }
  componentWillReceiveProps (newProps) {
    const instrumentIdNew = newProps.match.params.instrumentId
    const instrumentIdOld = this.props.match.params.instrumentId
    if (instrumentIdOld !== instrumentIdNew) {
      this.props.onInstrumentClear()
    }
  }
  componentWillUnmount () {
    this.props.onInstrumentClear()
  }
  render() {
    let { instrumentnewpage } = this.props
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <strong>{instrumentnewpage.id ?'Editar Instrumento':'Crear Instrumento'}</strong>
              </CardHeader>
              <CardBlock className="card-body">
                {instrumentnewpage.loading
                ?<LoadingIndicator />
                :<Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  {instrumentnewpage.success
                  ? <Card className="text-white bg-success">
                      <CardBlock className="card-body">
                        {instrumentnewpage.success}
                      </CardBlock>
                    </Card>                  
                  : null}
                  {instrumentnewpage.error
                  ? <Card className="text-white bg-danger">
                      <CardBlock className="card-body">
                        {instrumentnewpage.error}
                      </CardBlock>
                    </Card>                  
                  : null}
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Nombre (requerido)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref='name'
                        type="text"
                        id="text-input"
                        name="text-input"
                        placeholder="Nombre del instrumento"
                        min={6}
                        value={instrumentnewpage.name}
                        onChange={(evt) => {
                          this.props.onInstrumentChangeProp('name', evt.target.value)
                        }}
                      />
                      <FormText color="help-block">Mínimo 6 caracteres</FormText>
                    </Col>
                  </FormGroup>
                </Form>
                }
              </CardBlock>
              <CardFooter>
                {instrumentnewpage.id
                ? <Button
                  type="submit"
                  size="sm"
                  color="primary"
                  onClick={this.props.onInstrumentUpdating}
                  disabled={instrumentnewpage.loading}
                >Editar Instrumento</Button>              
                : <Button
                  type="submit"
                  size="sm"
                  color="primary"
                  onClick={this.props.onInstrumentCreate}
                  disabled={instrumentnewpage.loading}
                >Crear Instrumento</Button>
                }                
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

InstrumentNewPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  instrumentnewpage: makeSelectInstrumentNewPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onInstrumentChangeProp: (prop, value) => dispatch(instrumentChangeProp(prop, value)),
    onInstrumentCreate: () => dispatch(instrumentCreating()),
    onInstrumentRequest: (id) => dispatch(instrumentRequest(id)),
    onInstrumentUpdating: () => dispatch(instrumentUpdating()),
    onInstrumentClear: () => dispatch(instrumentClear()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'instrumentNewPage', reducer });
const withSaga = injectSaga({ key: 'instrumentNewPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(InstrumentNewPage);
