/*
 *
 * InstrumentNewPage actions
 *
 */

import {
  DEFAULT_ACTION,
  INSTRUMENT_CHANGE_PROP,
  INSTRUMENT_CREATING,
  INSTRUMENT_CREATED,
  INSTRUMENT_ERROR,
  INSTRUMENT_REQUEST,
  INSTRUMENT_SUCCESS,
  INSTRUMENT_UPDATING,
  INSTRUMENT_UPDATED,
  INSTRUMENT_CLEAR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function instrumentChangeProp(prop, value) {
  return {
    type: INSTRUMENT_CHANGE_PROP,
    prop,
    value
  };
}

export function instrumentCreating() {
  return {
    type: INSTRUMENT_CREATING,
  };
}

export function instrumentCreated() {
  return {
    type: INSTRUMENT_CREATED,
  };
}

export function instrumentError(error) {
  return {
    type: INSTRUMENT_ERROR,
    error,
  };
}

export function instrumentRequest(id) {
  return {
    type: INSTRUMENT_REQUEST,
    id,
  };
}

export function instrumentSuccess(payload) {
  return {
    type: INSTRUMENT_SUCCESS,
    payload,
  };
}

export function instrumentUpdating() {
  return {
    type: INSTRUMENT_UPDATING,
  };
}

export function instrumentUpdated() {
  return {
    type: INSTRUMENT_UPDATED,
  };
}

export function instrumentClear() {
  return {
    type: INSTRUMENT_CLEAR,
  };
}