/*
 *
 * InstrumentNewPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  INSTRUMENT_CHANGE_PROP,
  INSTRUMENT_CREATING,
  INSTRUMENT_CREATED,
  INSTRUMENT_ERROR,
  INSTRUMENT_REQUEST,
  INSTRUMENT_SUCCESS,
  INSTRUMENT_UPDATING,
  INSTRUMENT_UPDATED,
  INSTRUMENT_CLEAR,
} from './constants';

const initialState = fromJS({
  loading: false,
  error: false,
  success: false,
  id: false,
  name: '',
});

function instrumentNewPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case INSTRUMENT_CHANGE_PROP:
      return state
        .set(action.prop, action.value)
    case INSTRUMENT_CREATING:
      return state
        .set('loading', true)
        .set('error', false)
        .set('success', false)
    case INSTRUMENT_CREATED:
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', 'Instrument created')
    case INSTRUMENT_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('success', false)
    case INSTRUMENT_REQUEST:
      return state
        .set('loading', true)
        .set('error', false)
        .set('success', false)
        .set('id', action.id)
    case INSTRUMENT_SUCCESS:
      // console.log(action)
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', false)
        .set('name', action.payload.name)
    case INSTRUMENT_UPDATING:
      return state
        .set('loading', true)
        .set('error', false)
        .set('success', false)
    case INSTRUMENT_UPDATED:
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', 'Instrument updated')
    case INSTRUMENT_CLEAR:
      return state
      .set('loading', false)
      .set('error', false)
      .set('success', false)
      .set('name', '')
      .set('id', false)
    default:
      return state;
  }
}

export default instrumentNewPageReducer;
