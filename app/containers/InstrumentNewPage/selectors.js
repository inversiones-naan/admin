import { createSelector } from 'reselect';

/**
 * Direct selector to the instrumentNewPage state domain
 */
const selectInstrumentNewPageDomain = (state) => state.get('instrumentNewPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by InstrumentNewPage
 */

const makeSelectInstrumentNewPage = () => createSelector(
  selectInstrumentNewPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectInstrumentNewPage;
export {
  selectInstrumentNewPageDomain,
};
