/*
 *
 * InstrumentNewPage constants
 *
 */

export const DEFAULT_ACTION = 'app/InstrumentNewPage/DEFAULT_ACTION';
export const INSTRUMENT_CREATING = 'app/InstrumentNewPage/INSTRUMENT_CREATING';
export const INSTRUMENT_CREATED = 'app/InstrumentNewPage/INSTRUMENT_CREATED';
export const INSTRUMENT_ERROR = 'app/InstrumentNewPage/INSTRUMENT_ERROR';
export const INSTRUMENT_CHANGE_PROP = 'app/InstrumentNewPage/INSTRUMENT_CHANGE_PROP';
export const INSTRUMENT_REQUEST = 'app/InstrumentNewPage/INSTRUMENT_REQUEST';
export const INSTRUMENT_SUCCESS = 'app/InstrumentNewPage/INSTRUMENT_SUCCESS';
export const INSTRUMENT_UPDATING = 'app/InstrumentNewPage/INSTRUMENT_UPDATING';
export const INSTRUMENT_UPDATED = 'app/InstrumentNewPage/INSTRUMENT_UPDATED';
export const INSTRUMENT_CLEAR = 'app/InstrumentNewPage/INSTRUMENT_CLEAR';
