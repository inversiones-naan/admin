import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PASSWORD_REQUEST } from './constants';
import { passwordSuccess, passwordError } from './actions';
import { loginSuccess } from 'containers/App/actions';

import request from 'utils/request';
import params from 'utils/params';

import makeSelectPasswordNewPage from './selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PASSWORD_REQUEST, requestPassword);
}

export function* requestPassword() {
  // Select authToken from store
  const profilepage = yield select(makeSelectPasswordNewPage());
  const payload = {
    email: profilepage.email
  }
  const requestURL = `${params.apiUrl}users/new-password`;
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  }
  // console.log(options)
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    console.log(result)
    if (result.error) {
      yield put(passwordError(result.error.message));
    } else {
      yield put(passwordSuccess());
    }    
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error solicitud nueva contraseña'
    yield put(passwordError(err));
  }
}