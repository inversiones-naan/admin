/*
 *
 * PasswordNewPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  PASSWORD_CHANGE,
  PASSWORD_REQUEST,
  PASSWORD_SUCCESS,
  PASSWORD_ERROR,
  PASSWORD_CLEAR,
} from './constants';

const initialState = fromJS({
  loading: false,
  success: false,
  error: false,
  email: ''
});

function passwordNewPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PASSWORD_CHANGE:
      return state
      .set(action.prop, action.value)
    case PASSWORD_REQUEST:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false)
    case PASSWORD_SUCCESS:
      return state
      .set('loading', false)
      .set('success', true)
      .set('error', false)
    case PASSWORD_ERROR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', action.error)
    case PASSWORD_CLEAR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', false)
      .set('email', '')
    default:
      return state;
  }
}

export default passwordNewPageReducer;
