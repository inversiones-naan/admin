/**
 *
 * PasswordNewPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectPasswordNewPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {Container, Row, Col, CardGroup, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon} from "reactstrap";

import { passwordRequest, changeProp, clear } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

import { Link } from 'react-router-dom';

export class PasswordNewPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    // this.props.onClear()
  }
  submit(evt) {
    let keyCode = evt.keyCode || evt.which
    if (evt.shiftKey === false && keyCode === 13) {
      this.props.onPasswordRequest()
    }
  }
  render() {
    const {passwordnewpage} = this.props
    return (
      <div className="app flex-row align-items-center site-header-background">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  {passwordnewpage.loading
                  ? <h1 className='text-center'>Enviado correo ...</h1>
                  : passwordnewpage.success
                  ? <CardBlock className="card-body">
                      <div className={'text-center'}>
                        <h1 className="titulo">Revisa tu correo</h1>
                        <i className="icon-envelope-letter icons font-5xl d-block mt-4"></i>
                        <br/>
                        <p className="texto1">Te hemos enviado un correo a <b>{`${passwordnewpage.email}`}</b></p>
                        <p className="texto1">
                          Por favor sigue las instrucciones para reestablecer tu cuenta.
                        </p>
                        <p className="texto2">
                          <i>*Si no llega tu correo en la siguiente hora, revisa la carpeta de correo no deseado (SPAM)</i>
                        </p>
                        <Button outline color="secondary" size="lg" block onClick={this.props.onClear}>Reiniciar</Button>
                      </div>
                    </CardBlock>
                  : <CardBlock className="card-body">
                      <h1>¿Olvidaste tu contraseña?</h1>
                      <p className="text-muted">Puedes solicitar una nueva contraseña</p>
                      {passwordnewpage.error
                      ? <Card className="text-white bg-danger">
                          <CardBlock className="card-body">
                            {passwordnewpage.error}
                          </CardBlock>
                        </Card>                  
                      : null}
                      <InputGroup className="mb-3">
                        <InputGroupAddon><i className="icon-envelope"></i></InputGroupAddon>
                        <Input
                          ref="email"
                          type="text"
                          placeholder="Ingresa tu correo"
                          value={passwordnewpage.email}
                          onChange={(evt) => {
                            this.props.onChangeProp('email', evt.target.value)
                          }}
                          onKeyDown={this.submit.bind(this)}
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="12">
                          <Button
                            color="success"
                            block
                            onClick={this.props.onPasswordRequest}
                            disabled={passwordnewpage.loading}
                          >
                            Envíame instrucciones para restablecer mi contraseña
                          </Button>
                        </Col>
                        <div className="col-sm-12 form-group">
                          <br/>
                          <Link to={`/login`}>Iniciar sesión</Link>
                          <br/>
                          <Link to={`/sign_up`}>Registrarse</Link>
                          <br/>
                          <Link to={`/new-activate`}>¿No recibiste instrucciones para confirmar tu cuenta?</Link>
                          <br/>
                        </div>
                        {/* <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col> */}
                      </Row>
                    </CardBlock>
                  }
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

PasswordNewPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  passwordnewpage: makeSelectPasswordNewPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onChangeProp: (prop, value) => dispatch(changeProp(prop, value)),
    onPasswordRequest: () => dispatch(passwordRequest()),
    onClear: () => dispatch(clear())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'passwordNewPage', reducer });
const withSaga = injectSaga({ key: 'passwordNewPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PasswordNewPage);
