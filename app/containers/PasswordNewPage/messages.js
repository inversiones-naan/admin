/*
 * PasswordNewPage Messages
 *
 * This contains all the text for the PasswordNewPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.PasswordNewPage.header',
    defaultMessage: 'This is PasswordNewPage container !',
  },
});
