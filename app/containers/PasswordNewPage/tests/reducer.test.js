
import { fromJS } from 'immutable';
import passwordNewPageReducer from '../reducer';

describe('passwordNewPageReducer', () => {
  it('returns the initial state', () => {
    expect(passwordNewPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
