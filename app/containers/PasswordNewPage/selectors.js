import { createSelector } from 'reselect';

/**
 * Direct selector to the passwordNewPage state domain
 */
const selectPasswordNewPageDomain = (state) => state.get('passwordNewPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by PasswordNewPage
 */

const makeSelectPasswordNewPage = () => createSelector(
  selectPasswordNewPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectPasswordNewPage;
export {
  selectPasswordNewPageDomain,
};
