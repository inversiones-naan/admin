/*
 *
 * PasswordNewPage constants
 *
 */

export const DEFAULT_ACTION = 'app/PasswordNewPage/DEFAULT_ACTION';

export const PASSWORD_REQUEST = 'app/PasswordNewPage/PASSWORD_REQUEST';
export const PASSWORD_SUCCESS = 'app/PasswordNewPage/PASSWORD_SUCCESS';
export const PASSWORD_ERROR = 'app/PasswordNewPage/PASSWORD_ERROR';

export const PASSWORD_CHANGE = 'app/PasswordNewPage/PASSWORD_CHANGE';

export const PASSWORD_CLEAR = 'app/PasswordNewPage/PASSWORD_CLEAR';