/*
 *
 * PasswordNewPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PASSWORD_REQUEST,
  PASSWORD_SUCCESS,
  PASSWORD_ERROR,
  PASSWORD_CHANGE,
  PASSWORD_CLEAR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/**
 * Solicitar nueva contraseña
 *
 * @return {object} An action object with a type of PASSWORD_REQUEST
 */
export function passwordRequest() {
  return {
    type: PASSWORD_REQUEST,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of password_SUCCESS
 */
export function passwordSuccess() {
  return {
    type: PASSWORD_SUCCESS,
  };
}

/**
 * Respuesta con error del API
 *
 * @return {object}    An action object with a type of password_ERROR
 */
export function passwordError(error) {
  return {
    type: PASSWORD_ERROR,
    error
  };
}

/**
 * Actualizar propiedad
 *
 * @return {prop}
 * @return {value}
 */
export function changeProp(prop, value) {
  return {
    type: PASSWORD_CHANGE,
    prop,
    value,
  };
}

/**
 * Clear
 *
 */
export function clear() {
  return {
    type: PASSWORD_CLEAR,
  };
}
