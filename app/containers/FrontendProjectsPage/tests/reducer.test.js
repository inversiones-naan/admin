
import { fromJS } from 'immutable';
import frontendProjectsPageReducer from '../reducer';

describe('frontendProjectsPageReducer', () => {
  it('returns the initial state', () => {
    expect(frontendProjectsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
