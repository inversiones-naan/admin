import { createSelector } from 'reselect';

/**
 * Direct selector to the frontendProjectsPage state domain
 */
const selectFrontendProjectsPageDomain = (state) => state.get('frontendProjectsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by FrontendProjectsPage
 */

const makeSelectFrontendProjectsPage = () => createSelector(
  selectFrontendProjectsPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectFrontendProjectsPage;
export {
  selectFrontendProjectsPageDomain,
};
