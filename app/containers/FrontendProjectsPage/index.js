/**
 *
 * FrontendProjectsPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectFrontendProjectsPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { projectsRequest } from './actions';

import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  FormGroup,
  Label,
  Input,
  Button,
  ModalHeader,
  Modal,
  ModalBody,
  ModalFooter,
} from "reactstrap";

import Paginator from 'components/Paginator'

import LoadingIndicator from 'components/LoadingIndicator';

import {NavLink} from 'react-router-dom';

import Project from './Project'

export class FrontendProjectsPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    this.props.onProjectsRequest()
  }
  htmlStatus (project) {
    let className = ''
    if (project.status === '0') {
      return (<Badge color="secondary" className="float-right">Inabilitado</Badge>)
    } else if (project.status === '1') {
      return (<Badge color="warning" className="float-right">End fondeo</Badge>)      
    } else if (project.status === '2') {
      return (<Badge color="warning" className="float-right">Fondeo completo</Badge>)
    }
    return null
  }
  render() {
    let {projects, loading} = this.props.frontendprojectspage
    return (
      <div className="animated fadeIn project-index">
        <div className="container" style={{maxWidth: 1800, paddingLeft: 40, paddingRight: 40}}>
          <Row>
            {loading
            ? <Col xs="12">
              <div style={{height: screen.height/4}}/>
              <LoadingIndicator />
              <div style={{height: screen.height/4}}/>
            </Col>
            :null}
            {projects[1].length > 0
            ? <Col xs="12">
                <div className="main-title">
                  <h2 style={{marginTop: '6rem'}}>Campañas activas</h2>
                  <h3>Estas oportunidades están todavía aceptando inversiones</h3>
                </div>
              </Col>
            :null}
            {projects[1].length > 0
            ? projects[1].map((project, i) => {
                const {id, status} = project
                return (
                  <Project project={project} key={`project-${status}-${id}`} />
                )
              })
            :null}

            {projects[2].length > 0
            ? <Col xs="12">
                <div className="main-title">
                  <h2>Campañas pasadas</h2>
                  <h3>Estas oportunidades han concluido su periodo de financiamiento</h3>
                </div>
              </Col>
            :null}
            {projects[2].length > 0
            ? projects[2].map((project, i) => {
                const {id, status} = project
                return (
                  <Project project={project} key={`project-${status}-${id}`} />
                )
              })
            :null}

            {projects[3].length > 0
            ? <Col xs="12">
                <div className="main-title">
                  <h2>Campañas generando retornos</h2>
                  <h3>Estas oportunidades han concluido su periodo de financiamiento y estan generando retorno.</h3>
                </div>
              </Col>
            :null}
            {projects[3].length > 0
            ? projects[3].map((project, i) => {
                const {id, status} = project
                return (
                  <Project project={project} key={`project-${status}-${id}`} />
                )
              })
            :null}

            {projects[4].length > 0
            ? <Col xs="12">
                <div className="main-title">
                  <h2>Campañas totalmente finalizadas</h2>
                  <h3>Estas oportunidades han concluido exitosamente.</h3>
                </div>
              </Col>
            :null}
            {projects[4].length > 0
            ? projects[4].map((project, i) => {
                const {id, status} = project
                return (
                  <Project project={project} key={`project-${status}-${id}`} />
                )
              })
            :null}
          </Row>
        </div>
      </div>
    );
  }
}

FrontendProjectsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  frontendprojectspage: makeSelectFrontendProjectsPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onProjectsRequest: () => dispatch(projectsRequest())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'frontendProjectsPage', reducer });
const withSaga = injectSaga({ key: 'frontendProjectsPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(FrontendProjectsPage);
