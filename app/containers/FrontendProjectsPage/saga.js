import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PROJECTS_REQUEST } from './constants';
import { projectsSuccess, projectsError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectFrontendProjectsPage from './selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PROJECTS_REQUEST, getProjects);
}

/**
 * Obtener los proyectos de la API
 */
export function* getProjects() {
  // Select authToken from store
  
  const requestURL = `${params.apiUrl}projects?include=images,builder,instrument&where={"status":{"gt":0}}`;
  const options = {
    method: 'GET'
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(projectsError(result.error.message));
    } else {
      yield put(projectsSuccess(result.projects));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectsError(err));
  }
}
