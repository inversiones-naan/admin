/*
 *
 * FrontendProjectsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/FrontendProjectsPage/DEFAULT_ACTION';
export const PROJECTS_REQUEST = 'app/FrontendProjectsPage/PROJECTS_REQUEST';
export const PROJECTS_SUCCESS = 'app/FrontendProjectsPage/PROJECTS_SUCCESS';
export const PROJECTS_ERROR = 'app/FrontendProjectsPage/PROJECTS_ERROR';
