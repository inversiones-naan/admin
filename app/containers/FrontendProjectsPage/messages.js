/*
 * FrontendProjectsPage Messages
 *
 * This contains all the text for the FrontendProjectsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.FrontendProjectsPage.header',
    defaultMessage: 'This is FrontendProjectsPage container !',
  },
});
