/*
 *
 * FrontendProjectsPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PROJECTS_REQUEST,
  PROJECTS_SUCCESS,
  PROJECTS_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}


/**
 * Solicita los proyectos al API
 *
 * @return {object}    An action object with a type of PROJECTS_REQUEST
 */
export function projectsRequest() {
  return {
    type: PROJECTS_REQUEST
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of PROJECTS_SUCCESS
 */
export function projectsSuccess(payload) {
  return {
    type: PROJECTS_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of PROJECTS_ERROR
 */
export function projectsError(error) {
  return {
    type: PROJECTS_ERROR,
    error,
  };
}