import React, {Component} from 'react';
import Slider from 'react-slick';

import {
  Col
} from "reactstrap";

import {NavLink} from 'react-router-dom';

import params from 'utils/params';

export default class Project extends Component {
  render() {
    const {id, name, images, builder, address, progressFunding, investmentProgress, fixedAnnualRate, term, possibilityOfPrepaid, instrument, funding, minimumInvestment, status} = this.props.project
    let ribbon = 'waiting'
    if (status === '2') {
      ribbon = 'fund'
    } else if (status === '3') {
      ribbon = 'generating'
    } else if (status === '4') {
      ribbon = 'finished'
    }
    let urlImageProyect = ''
    if (images) {
      if (images[0]) {
        urlImageProyect = images[0].url
      }
    }
    
    return (
      <Col xs="12">
        <div className="project" style={{fontSize: 20}}>
          <div className="row">
            <div className="col-md-4">
              <NavLink to={`/proyecto/${id}`}>
                <div className={`project-photo ribbon-${ribbon}`} style={{display: 'block', backgroundImage: `url(${params.apiUrl}${urlImageProyect})`, minHeight: 280}}/>
              </NavLink>
            </div>
            <div className="col-md-8">
              <NavLink className="pull-right more hidden-xs" to={`/proyecto/${id}`}>
                Conocer más
              </NavLink>
              <div className="media logo-and-address">
                <div className="media-left media-middle">
                  <img src={`${params.apiUrl}${builder.urlImage}`} alt="Logo" />
                </div>
                <div className="media-body text-left">
                  <h2 className="media-heading">
                    <NavLink to={`/proyecto/${id}`}>
                      {name}
                    </NavLink>
                  </h2>
                  <p>
                    {`por ${builder.name}`}
                  </p>
                  <small>
                    <span aria-hidden="true" className="glyphicon glyphicon-map-marker" data-placement="bottom" data-toggle="tooltip" title="" data-original-title="Ubicación del proyecto">
                    </span>
                    {address}
                  </small>
                </div>
              </div>
              <div className="row progress-and-amount">
                <div className="col-md-6">
                  <div className="progress" style={{position: 'relative'}}>
                    <div aria-valuemax="100" className="progress-bar progress-bar-success" role="progressbar" style={{width: `${progressFunding}%`, minWidth: '2em'}}>
                      {`${progressFunding}%`}
                    </div>
                    {/* <span style={{position: 'absolute', right: 0, fontSize: 'small', padding: '2px 5px', color: '#bbb'}}>
                      Min: 70%
                    </span> */}
                  </div>
                </div>
                <div className="col-md-6">
                  <p>
                    Cantidad conseguida:
                    <strong>{` $${investmentProgress}`}</strong>
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="table-responsive">
                    <table className="table table-condensed">
                      <tbody>
                        <tr>
                          <th className="first">Tasa anual fija</th>
                          <td className="first">{`${fixedAnnualRate}%`}</td>
                        </tr>
                        <tr>
                          <th>Plazo</th>
                          <td>{`${term} meses`}</td>
                        </tr>
                        <tr>
                          <th>Posibilidad de Prepago</th>
                          <td>{`${possibilityOfPrepaid}`}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="table-responsive">
                    <table className="table table-condensed">
                      <tbody>
                        <tr>
                          <th className="first">Instrumento</th>
                          <td className="first">{instrument.name}</td>
                        </tr>
                        <tr>
                          <th>Financiamiento</th>
                          <td>{`${funding}`}</td>
                        </tr>
                        <tr>
                          <th>Inversion mín. por persona</th>
                          <td>{`${minimumInvestment}`}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Col>
    );
  }
}