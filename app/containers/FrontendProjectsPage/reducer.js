/*
 *
 * FrontendProjectsPage reducer
 *
 */

import { fromJS } from 'immutable';

import {
  DEFAULT_ACTION,
  PROJECTS_REQUEST,
  PROJECTS_SUCCESS,
  PROJECTS_ERROR,
} from './constants';

const projectDefault = {
  id: '1',
  images:[
    {
      url:'https://d359dzbkfqzc9h.cloudfront.net/uploads/project/photo/72/thumb_2017-12-04-PHOTO-00000165.jpg'
    }
  ],
  name: 'Proyecto 1',
  builder:{
    name: 'SBGN',
    urlImage: 'https://d359dzbkfqzc9h.cloudfront.net/uploads/developer/logo/26/LOGO.png',
  },
  address: 'Benjamín Hill 245, Hipódromo',
  progressFunding: '60.19',
  investmentProgress: '1805771.17',
  fixedAnnualRate: '13.00',
  term: '24',
  possibilityOfPrepaid: 'Sí, a partir del mes 6',
  instrument: {
    name: 'Deuda Senior'
  },
  funding: '3000000.00',
  minimumInvestment: '25000.00'
}

const projects = {}

// for (let i = 1; i < 5; i++) {
//   projects[i] = []
//   for (let j = 1; j < 5; j++) {
//     let p = JSON.parse(JSON.stringify(projectDefault))
//     p.id = i*j+''
//     p.status = i+''
//     p.name = `Proyecto ${(i*j)}`
//     p.builderName = `Contructora ${(j)}`
//     p.progressFunding = (50+7*j).toFixed(2)
//     p.funding = Math.floor((Math.random() * 1000000) + 800000).toFixed(2)+'';
//     p.fixedAnnualRate = Math.floor((Math.random() * 10) + 10).toFixed(2)+'';
//     p.term = Math.floor((Math.random() * 36) + 12)+'';
//     if (i > 1) {
//       p.progressFunding = '100.00'
//     }
//     p.investmentProgress = (p.progressFunding*p.funding/100).toFixed(2)
//     projects[i].push(p)
//   }
// }
const initialState = fromJS({
  projects: {
    '1': [],
    '2': [],
    '3': [],
    '4': []
  },
  // projects: projects,
  loading: false,
  error:false
});

function frontendProjectsPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PROJECTS_REQUEST:
      return state
      .set('projects', fromJS({
        1:[],
        2:[],
        3:[],
        4:[],
      }))
      .set('loading', true)
      .set('error', false)
    case PROJECTS_SUCCESS:
      let ps = {
        1:[],
        2:[],
        3:[],
        4:[],
      }
      for (let i = 0; i < action.payload.length; i++) {
        let p = action.payload[i];
        p.investmentProgress = (p.progressFunding*p.funding/100).toFixed(2)
        ps[p.status].push(p)
      }
      // console.log(ps)
      return state
      .set('projects', fromJS(ps))
      .set('loading', false)
      .set('error', false)
    case PROJECTS_ERROR:
      return state
      .set('projects', fromJS({
        1:[],
        2:[],
        3:[],
        4:[],
      }))
      .set('loading', false)
      .set('error', false)
    default:
      return state;
  }
}

export default frontendProjectsPageReducer;
