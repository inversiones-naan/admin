/*
 * ContainerApp Messages
 *
 * This contains all the text for the ContainerApp component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ContainerApp.header',
    defaultMessage: 'This is ContainerApp container !',
  },
});
