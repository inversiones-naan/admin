import { createSelector } from 'reselect';

/**
 * Direct selector to the containerApp state domain
 */
const selectContainerAppDomain = (state) => state.get('containerApp');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ContainerApp
 */

const makeSelectContainerApp = () => createSelector(
  selectContainerAppDomain,
  (substate) => substate.toJS()
);

export default makeSelectContainerApp;
export {
  selectContainerAppDomain,
};
