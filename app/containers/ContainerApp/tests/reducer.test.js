
import { fromJS } from 'immutable';
import containerAppReducer from '../reducer';

describe('containerAppReducer', () => {
  it('returns the initial state', () => {
    expect(containerAppReducer(undefined, {})).toEqual(fromJS({}));
  });
});
