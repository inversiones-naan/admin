/**
 *
 * ContainerApp
 *
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { logout } from '../App/actions';

import injectSaga from 'utils/injectSaga';
import { makeSelectUser, makeSelectUserId } from 'containers/App/selectors';
import injectReducer from 'utils/injectReducer';
import makeSelectContainerApp from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { Helmet } from 'react-helmet';
import {Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';

// Components
import Header from '../../components/Header/Loadable';
import Sidebar from '../../components/Sidebar/Loadable';
import Breadcrumb from '../../components/Breadcrumb/Loadable';
import Aside from '../../components/Aside/Loadable';
import Footer from '../../components/Footer2/Loadable';
import Dashboard from '../DashboardPage/Loadable';
// import Buttons from '../../views/Components/Buttons';
// import Cards from '../../views/Components/Cards';
// import Forms from '../../views/Components/Forms';
// import Modals from '../../views/Components/Modals';
// import SocialButtons from '../../views/Components/SocialButtons';
// import Switches from '../../views/Components/Switches';
// import Tables from '../../views/Components/Tables';
// import Tabs from '../../views/Components/Tabs';

// Containers
// import Charts from '../../views/Charts/';
// import Widgets from '../../views/Widgets/';
import UsersPage from '../UsersPage/Loadable';
import UserNewPage from '../UserNewPage/Loadable';
import ProfilePage from '../ProfilePage/Loadable';
import InstrumentNewPage from '../InstrumentNewPage/Loadable';
import InstrumentsPage from '../InstrumentsPage/Loadable';
import BuilderNewPage from '../BuilderNewPage/Loadable';
import BuildersPage from '../BuildersPage/Loadable';
import ProjectNewPage from '../ProjectNewPage/Loadable';
import ProjectsPage from '../ProjectsPage/Loadable';
import ProjectsusersPage from '../ProjectsusersPage/Loadable';
import ProjectsfundingPage from '../ProjectsfundingPage/Loadable';
import ProjectsImagesPage from '../ProjectsImagesPage/Loadable'
import AdminNewsPage from '../AdminNewsPage/Loadable'

// Icons
import FontAwesome from '../../views/Icons/FontAwesome/';
import SimpleLineIcons from '../../views/Icons/SimpleLineIcons/';

// <FormattedMessage {...messages.header} />

export class ContainerApp extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }
  componentWillReceiveProps (nextProps) {
    let { history } = this.props
    this.goLogin (nextProps.user, history)
  }
  componentDidMount () {
    document.body.classList.remove('sidebar-hidden');
    // let container = document.getElementById('container')
    // container.classList.add("intro-effect-sliced")
    // container.classList.add("modify")

    let { history, user } = this.props
    if (user.role !== 'Administrator') {
      history.push('/')
      return
    }
    this.goLogin (user, history)
  }
  goLogin (user, history) {
    if (!user) {
      history.push('/login')
    }
  }
  render() {
    return (
      <div className="app" id="container">
        <Helmet
          titleTemplate="%s - Administrador Inversiones"
          defaultTitle="Administrador Inversiones"
        >
          <meta name="description" content="A Administrador Inversiones application" />
        </Helmet>
        <Header onLogout={this.props.onLogout} user={this.props.user}/>
        <div className="app-body"> 
          <Sidebar {...this.props}/>
          <main className="main"> 
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <Route path="/admin/dashboard" name="Dashboard" component={Dashboard}/>
                <Route path="/admin/users/all-users" name="Users" component={UsersPage}/>
                <Route path="/admin/users/user-new" name="UserNewPage" component={UserNewPage}/>
                <Route path="/admin/users/profile/:userId" name="Profile" component={ProfilePage}/>
                <Route path="/admin/users/your-profile" name="YourProfile" component={ProfilePage}/>

                <Route path="/admin/instruments/new" name="InstrumentNew" component={InstrumentNewPage}/>
                <Route path="/admin/instruments/all" name="Instruments" component={InstrumentsPage}/>
                <Route path="/admin/instruments/edit/:instrumentId" name="InstrumentEdit" component={InstrumentNewPage}/>

                <Route path="/admin/builders/new" name="BuilderNew" component={BuilderNewPage}/>
                <Route path="/admin/builders/edit/:builderId" name="BuilderEdit" component={BuilderNewPage}/>
                <Route path="/admin/builders/all" name="Builder" component={BuildersPage}/> 
                
                <Route path="/admin/projects/all" name="Projecs" component={ProjectsPage}/>
                <Route path="/admin/projects/new" name="ProjectNew" component={ProjectNewPage}/>
                <Route path="/admin/projects/edit/:projectId" name="ProjectEdit" component={ProjectNewPage}/>
                <Route path="/admin/projects/users/:projectId" name="ProjectUsers" component={ProjectsusersPage}/>
                <Route path="/admin/projects/funding/:projectId" name="ProjectFunding" component={ProjectsfundingPage}/>
                <Route path="/admin/projects/images/:projectId" name="ProjectImages" component={ProjectsImagesPage}/>
                <Route path="/admin/projects/news/:projectId" name="ProjectNews" component={AdminNewsPage}/>
                
                {/* <Route path="/admin/components/buttons" name="Buttons" component={Buttons}/>
                <Route path="/admin/components/cards" name="Cards" component={Cards}/>
                <Route path="/admin/components/forms" name="Forms" component={Forms}/>
                <Route path="/admin/components/modals" name="Modals" component={Modals}/>
                <Route path="/admin/components/social-buttons" name="Social Buttons" component={SocialButtons}/>
                <Route path="/admin/components/switches" name="Swithces" component={Switches}/>
                <Route path="/admin/components/tables" name="Tables" component={Tables}/>
                <Route path="/admin/components/tabs" name="Tabs" component={Tabs}/>
                <Route path="/admin/icons/font-awesome" name="Font Awesome" component={FontAwesome}/>
                <Route path="/admin/icons/simple-line-icons" name="Simple Line Icons" component={SimpleLineIcons}/>
                <Route path="/admin/widgets" name="Widgets" component={Widgets}/>
                <Route path="/admin/charts" name="Charts" component={Charts}/> */}
                <Redirect from="/admin" to="/admin/dashboard"/>
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

ContainerApp.propTypes = {
  dispatch: PropTypes.func.isRequired,
  // user: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  containerapp: makeSelectContainerApp(),
  user: makeSelectUser(),
  userId: makeSelectUserId()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onLogout: () => dispatch(logout()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'containerApp', reducer });
const withSaga = injectSaga({ key: 'containerApp', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ContainerApp);
