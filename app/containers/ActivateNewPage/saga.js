import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { ACTIVATE_REQUEST } from './constants';
import { activateSuccess, activateError } from './actions';
import { loginSuccess } from 'containers/App/actions';

import request from 'utils/request';
import params from 'utils/params';

import makeSelectActivateNewPage from './selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(ACTIVATE_REQUEST, requestPActivate);
}

export function* requestPActivate() {
  // Select authToken from store
  const activatenewpage = yield select(makeSelectActivateNewPage());
  const payload = {
    email: activatenewpage.email
  }
  const requestURL = `${params.apiUrl}users/new-activate`;
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  }
  // console.log(options)
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    console.log(result)
    if (result.error) {
      yield put(activateError(result.error.message));
    } else {
      yield put(activateSuccess());
    }    
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error solicitud correo de activación'
    yield put(activateError(err));
  }
}
