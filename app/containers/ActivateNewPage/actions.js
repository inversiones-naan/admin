/*
 *
 * ActivateNewPage actions
 *
 */

import {
  DEFAULT_ACTION,
  ACTIVATE_REQUEST,
  ACTIVATE_SUCCESS,
  ACTIVATE_ERROR,
  ACTIVATE_CHANGE,
  ACTIVATE_CLEAR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/**
 * Solicitar nueva contraseña
 *
 * @return {object} An action object with a type of ACTIVATE_REQUEST
 */
export function activateRequest() {
  return {
    type: ACTIVATE_REQUEST,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of ACTIVATE_SUCCESS
 */
export function activateSuccess() {
  return {
    type: ACTIVATE_SUCCESS,
  };
}

/**
 * Respuesta con error del API
 *
 * @return {object}    An action object with a type of ACTIVATE_ERROR
 */
export function activateError(error) {
  return {
    type: ACTIVATE_ERROR,
    error
  };
}

/**
 * Actualizar propiedad
 *
 * @return {prop}
 * @return {value}
 */
export function changeProp(prop, value) {
  return {
    type: ACTIVATE_CHANGE,
    prop,
    value,
  };
}

/**
 * Clear
 *
 */
export function clear() {
  return {
    type: ACTIVATE_CLEAR,
  };
}
