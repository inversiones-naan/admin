/*
 *
 * ActivateNewPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ActivateNewPage/DEFAULT_ACTION';

export const ACTIVATE_REQUEST = 'app/ActivateNewPage/ACTIVATE_REQUEST';
export const ACTIVATE_SUCCESS = 'app/ActivateNewPage/ACTIVATE_SUCCESS';
export const ACTIVATE_ERROR = 'app/ActivateNewPage/ACTIVATE_ERROR';

export const ACTIVATE_CHANGE = 'app/ActivateNewPage/ACTIVATE_CHANGE';

export const ACTIVATE_CLEAR = 'app/ActivateNewPage/ACTIVATE_CLEAR';
