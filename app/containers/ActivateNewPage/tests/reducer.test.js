
import { fromJS } from 'immutable';
import activateNewPageReducer from '../reducer';

describe('activateNewPageReducer', () => {
  it('returns the initial state', () => {
    expect(activateNewPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
