/*
 *
 * ActivateNewPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  ACTIVATE_REQUEST,
  ACTIVATE_SUCCESS,
  ACTIVATE_ERROR,
  ACTIVATE_CHANGE,
  ACTIVATE_CLEAR,
} from './constants';

const initialState = fromJS({
  loading: false,
  success: false,
  error: false,
  email: ''
});

function activateNewPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case ACTIVATE_CHANGE:
      return state
      .set(action.prop, action.value)
    case ACTIVATE_REQUEST:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false)
    case ACTIVATE_SUCCESS:
      return state
      .set('loading', false)
      .set('success', true)
      .set('error', false)
    case ACTIVATE_ERROR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', action.error)
    case ACTIVATE_CLEAR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', false)
      .set('email', '')
    default:
      return state;
  }
}

export default activateNewPageReducer;
