/**
 *
 * ActivateNewPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectActivateNewPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {Container, Row, Col, CardGroup, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon} from "reactstrap";

import { activateRequest, changeProp, clear } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

import { Link } from 'react-router-dom';

export class ActivateNewPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    this.props.onClear()
  }
  submit(evt) {
    let keyCode = evt.keyCode || evt.which
    if (evt.shiftKey === false && keyCode === 13) {
      this.props.onActivateRequest()
    }
  }
  render() {
    const {activatenewpage} = this.props
    return (
      <div className="app flex-row align-items-center site-header-background">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  {activatenewpage.loading
                  ? <h1 className='text-center'>Enviado correo ...</h1>
                  : activatenewpage.success
                  ? <CardBlock className="card-body">
                      <div className={'text-center'}>
                        <h1 className="titulo">Revisa tu correo</h1>
                        <i className="icon-envelope-letter icons font-5xl d-block mt-4"></i>
                        <br/>
                        <p className="texto1">Te hemos enviado un correo a <b>{`${passwordnewpage.email}`}</b></p>
                        <p className="texto1">
                          Por favor sigue las instrucciones para activar tu cuenta.
                        </p>
                        <p className="texto2">
                          <i>*Si no llega tu correo en la siguiente hora, revisa la carpeta de correo no deseado (SPAM)</i>
                        </p>
                        <Button outline color="secondary" size="lg" block onClick={this.props.onClear}>Reiniciar</Button>
                      </div>
                    </CardBlock>
                  : <CardBlock className="card-body">
                      <h1>Instrucciones de confirmación</h1>
                      <p className="text-muted">Puedes solicitar un nuevo correo para activar su cuenta</p>
                      {activatenewpage.error
                      ? <Card className="text-white bg-danger">
                          <CardBlock className="card-body">
                            {activatenewpage.error}
                          </CardBlock>
                        </Card>                  
                      : null}
                      <InputGroup className="mb-3">
                        <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                        <Input
                          ref="email"
                          type="text"
                          placeholder="Ingresa tu correo"
                          defaultValue={activatenewpage.email}
                          onChange={(evt) => {
                            this.props.onChangeProp('email', evt.target.value)
                          }}
                          onKeyDown={this.submit.bind(this)}
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="12">
                          <Button
                            color="success"
                            block
                            onClick={this.props.onActivateRequest}
                            disabled={activatenewpage.loading}
                          >
                            Envíame instrucciones para restablecer mi contraseña
                          </Button>
                        </Col>
                        <div className="col-sm-12 form-group">
                          <br/>
                          <Link to={`/iniciar-sesion`}>Iniciar sesión</Link>
                          <br/>
                          <Link to={`/registro`}>Registrarse</Link>
                          <br/>
                          <Link to={`/new-password`}>¿Olvidaste tu contraseña?</Link>
                          <br/>
                        </div>
                        {/* <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col> */}
                      </Row>
                    </CardBlock>
                  }
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

ActivateNewPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  activatenewpage: makeSelectActivateNewPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onChangeProp: (prop, value) => dispatch(changeProp(prop, value)),
    onActivateRequest: () => dispatch(activateRequest()),
    onClear: () => dispatch(clear())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'activateNewPage', reducer });
const withSaga = injectSaga({ key: 'activateNewPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ActivateNewPage);
