import { createSelector } from 'reselect';

/**
 * Direct selector to the activateNewPage state domain
 */
const selectActivateNewPageDomain = (state) => state.get('activateNewPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ActivateNewPage
 */

const makeSelectActivateNewPage = () => createSelector(
  selectActivateNewPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectActivateNewPage;
export {
  selectActivateNewPageDomain,
};
