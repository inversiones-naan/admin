/*
 * ActivateNewPage Messages
 *
 * This contains all the text for the ActivateNewPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ActivateNewPage.header',
    defaultMessage: 'This is ActivateNewPage container !',
  },
});
