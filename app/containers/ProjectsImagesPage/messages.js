/*
 * ProjectsImagesPage Messages
 *
 * This contains all the text for the ProjectsImagesPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ProjectsImagesPage.header',
    defaultMessage: 'This is ProjectsImagesPage container !',
  },
});
