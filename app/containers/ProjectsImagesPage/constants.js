/*
 *
 * ProjectsImagesPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ProjectsImagesPage/DEFAULT_ACTION';
export const PROJECTS_IMAGES_REQUEST = 'app/ProjectsImagesPage/PROJECTS_IMAGES_REQUEST';
export const PROJECTS_IMAGES_SUCCESS = 'app/ProjectsImagesPage/PROJECTS_IMAGES_SUCCESS';
export const PROJECTS_IMAGES_ERROR = 'app/ProjectsImagesPage/PROJECTS_IMAGES_ERROR';

export const IMAGES_UPLOAD_REQUEST = 'app/ProjectsImagesPage/IMAGES_UPLOAD_REQUEST';
export const IMAGES_UPLOAD_SUCCESS = 'app/ProjectsImagesPage/IMAGES_UPLOAD_SUCCESS';
export const IMAGES_UPLOAD_ERROR = 'app/ProjectsImagesPage/IMAGES_UPLOAD_ERROR';

export const CHANGE_PROP = 'app/ProjectsImagesPage/CHANGE_PROP';

export const IMAGE_ENABLE_REQUEST = 'app/ProjectsImagesPage/IMAGE_ENABLE_REQUEST';
export const IMAGE_ENABLE_SUCCESS = 'app/ProjectsImagesPage/IMAGE_ENABLE_SUCCESS';
export const IMAGE_ENABLE_ERROR = 'app/ProjectsImagesPage/IMAGE_ENABLE_ERROR';

export const IMAGE_DELETE_REQUEST = 'app/ProjectsImagesPage/IMAGE_DELETE_REQUEST';
export const IMAGE_DELETE_SUCCESS = 'app/ProjectsImagesPage/IMAGE_DELETE_SUCCESS';
export const IMAGE_DELETE_ERROR = 'app/ProjectsImagesPage/IMAGE_DELETE_ERROR';

