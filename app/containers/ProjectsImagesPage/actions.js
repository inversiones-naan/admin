/*
 *
 * ProjectsImagesPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PROJECTS_IMAGES_REQUEST,
  PROJECTS_IMAGES_SUCCESS,
  PROJECTS_IMAGES_ERROR,
  IMAGES_UPLOAD_REQUEST,
  IMAGES_UPLOAD_SUCCESS,
  IMAGES_UPLOAD_ERROR,
  CHANGE_PROP,
  IMAGES_ENABLE_REQUEST,
  IMAGE_ENABLE_REQUEST,
  IMAGE_ENABLE_SUCCESS,
  IMAGE_ENABLE_ERROR,
  IMAGE_DELETE_REQUEST,
  IMAGE_DELETE_SUCCESS,
  IMAGE_DELETE_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}



/**
 * Solicita los usuarios al API
 *
 * @return {object}    An action object with a type of PROJECTS_REQUEST
 */
export function imagesRequest(projectId) {
  return {
    type: PROJECTS_IMAGES_REQUEST,
    projectId
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of PROJECTS_IMAGES_SUCCESS
 */
export function imagesSuccess(payload) {
  return {
    type: PROJECTS_IMAGES_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of PROJECTS_IMAGES_ERROR
 */
export function imagesError(error) {
  return {
    type: PROJECTS_IMAGES_ERROR,
    error,
  };
}


/**
 * Subir imagenes al API
 *
 * @return {object}    An action object with a type of IMAGES_UPLOAD_REQUEST
 */
export function imagesUploadRequest(projectId) {
  return {
    type: IMAGES_UPLOAD_REQUEST,
    projectId
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of IMAGES_UPLOAD_SUCCESS
 */
export function imagesUploadSuccess(payload) {
  return {
    type: IMAGES_UPLOAD_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of IMAGES_UPLOAD_ERROR
 */
export function imagesUploadError(error) {
  return {
    type: IMAGES_UPLOAD_ERROR,
    error,
  };
}

/**
 * Change prop
 *
 * @return {object}    An action object with a type of CHANGE_PROP
 */
export function changeProp(prop, value) {
  return {
    type: CHANGE_PROP,
    prop,
    value
  };
}


/**
 * Habilitar imagen API
 *
 * @return {object}    An action object with a type of IMAGE_ENABLE_REQUEST
 */
export function imageEnableRequest(image) {
  return {
    type: IMAGE_ENABLE_REQUEST,
    image
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of IMAGE_ENABLE_SUCCESS
 */
export function imageEnableSuccess(payload) {
  return {
    type: IMAGE_ENABLE_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of IMAGE_ENABLE_ERROR
 */
export function imageEnableError(error) {
  return {
    type: IMAGE_ENABLE_ERROR,
    error,
  };
}



/**
 * Eliminar imagen API
 *
 * @return {object}    An action object with a type of IMAGE_DELETE_REQUEST
 */
export function imageDeleteRequest(image) {
  return {
    type: IMAGE_DELETE_REQUEST,
    image
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of IMAGE_DELETE_SUCCESS
 */
export function imageDeleteSuccess(payload) {
  return {
    type: IMAGE_DELETE_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of IMAGE_DELETE_ERROR
 */
export function imageDeleteError(error) {
  return {
    type: IMAGE_DELETE_ERROR,
    error,
  };
}
