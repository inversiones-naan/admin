import { createSelector } from 'reselect';

/**
 * Direct selector to the projectsImagesPage state domain
 */
const selectProjectsImagesPageDomain = (state) => state.get('projectsImagesPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ProjectsImagesPage
 */

const makeSelectProjectsImagesPage = () => createSelector(
  selectProjectsImagesPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectProjectsImagesPage;
export {
  selectProjectsImagesPageDomain,
};
