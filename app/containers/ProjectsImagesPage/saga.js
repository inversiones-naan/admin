import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PROJECTS_IMAGES_REQUEST, IMAGES_UPLOAD_REQUEST, IMAGE_ENABLE_REQUEST, IMAGE_DELETE_REQUEST } from './constants';
import { imagesRequest, imagesSuccess, imagesError, imagesUploadSuccess, imagesUploadError, imageEnableSuccess, imageEnableError, imageDeleteSuccess, imageDeleteError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectProjectsImagesPage from './selectors';
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PROJECTS_IMAGES_REQUEST, getImages)
  yield takeLatest(IMAGES_UPLOAD_REQUEST, uploadImages)
  yield takeLatest(IMAGE_ENABLE_REQUEST, updateImage)
  yield takeLatest(IMAGE_DELETE_REQUEST, deleteImage)
}
/**
 * Obtener las imagenes de la API
 */
export function* getImages() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(usersError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsImagesPage());
  const {projectId} = reducer
  
  const requestURL = `${params.apiUrl}/projects-images/?where={"projectId":{"e":"${projectId}"}}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(imagesError(result.error.message));
    } else {
      yield put(imagesSuccess(result.ProjectsImages));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(imagesError(err));
  }
}


/**
 * Obtener las imagenes de la API
 */
export function* uploadImages() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(usersError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsImagesPage());
  let {fileList, projectId} = reducer
  let formData = new FormData();
  formData.append('projectId', projectId);
  let x = 0
  fileList.map((f) => {
    formData.append('file'+x, f.file);
    x+=1
  });
  const requestURL = `${params.apiUrl}projects-images`;
  const options = {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${authToken}`
    },
    body: formData
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(imagesUploadError(result.error.message));
    } else {
      yield put(imagesUploadSuccess(result.payload));

      // Recargar la tabla
      yield put(imagesRequest(projectId));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(imagesUploadError(err));
  }
}


/**
 * Actualizar la imagen
 */
export function* updateImage() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(usersError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsImagesPage());
  let {image} = reducer
  let payload = {
    status: image.status === 1 ? 0 : 1
  }
  const requestURL = `${params.apiUrl}projects-images/${image.id}`;
  const options = {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${authToken}`
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(imageEnableError(result.error.message));
    } else {
      yield put(imageEnableSuccess(result.projectImage));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(imageEnableError(err));
  }
}


/**
 * Actualizar la imagen
 */
export function* deleteImage() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(usersError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsImagesPage());
  let {image} = reducer
  const requestURL = `${params.apiUrl}projects-images/${image.id}`;
  const options = {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(imageDeleteError(result.error.message));
    } else {
      yield put(imageDeleteSuccess(result.projectImage));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(imageDeleteError(err));
  }
}
