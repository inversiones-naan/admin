/*
 *
 * ProjectsImagesPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  PROJECTS_IMAGES_REQUEST,
  PROJECTS_IMAGES_SUCCESS,
  PROJECTS_IMAGES_ERROR,
  IMAGES_UPLOAD_REQUEST,
  IMAGES_UPLOAD_SUCCESS,
  IMAGES_UPLOAD_ERROR,
  CHANGE_PROP,
  IMAGE_ENABLE_REQUEST,
  IMAGE_ENABLE_SUCCESS,
  IMAGE_ENABLE_ERROR,
  IMAGE_DELETE_REQUEST,
  IMAGE_DELETE_SUCCESS,
  IMAGE_DELETE_ERROR,
} from './constants';


const initialState = fromJS({
  images: [],
  autoRefresh: false,
  projectId: null,
  loading: false,
  error: false,
  fileList: [],
  loadingUpload: false,
  successUpload: false,
  errorUpload: false,
  image: {}
});

function projectsImagesPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PROJECTS_IMAGES_REQUEST:
      return state
      .set('projectId', action.projectId)
      .set('images', [])
      .set('loading', true)
      .set('error', false)
      .set('autoRefresh', false)
    case PROJECTS_IMAGES_SUCCESS:
      return state
      .set('images', action.payload)
      .set('loading', false)
      .set('error', false)
    case PROJECTS_IMAGES_ERROR:
      return state
      .set('images', [])
      .set('loading', false)
      .set('error', action.error)
    case IMAGES_UPLOAD_REQUEST:
      return state
      .set('projectId', action.projectId)
      .set('loadingUpload', true)
      .set('successUpload', false)
      .set('errorUpload', false)      
    case IMAGES_UPLOAD_SUCCESS:
      return state
      .set('fileList', [])
      .set('loadingUpload', false)
      .set('successUpload', 'Imagenes subidas')
      .set('errorUpload', false)
      .set('autoRefresh', true)
    case IMAGES_UPLOAD_ERROR:
      return state
      .set('loadingUpload', false)
      .set('successUpload', false)
      .set('errorUpload', action.error)
    case CHANGE_PROP:
      return state
      .set(action.prop, action.value)
    case IMAGE_ENABLE_REQUEST:
      return state
      .set('image', action.image)
    case IMAGE_ENABLE_SUCCESS:
      let images = state.toJS().images
      let image = action.payload
      for (let i = 0; i < images.length; i++) {
        let element = images[i];
        if (parseInt(element.id) === parseInt(image.id)) {
          element.status = image.status
        }
      }
      return state
      .set('image', {})
      .set('images', fromJS(images))
    case IMAGE_ENABLE_ERROR:
      return state
      .set('image', {})
    case IMAGE_DELETE_REQUEST:
      return state
      .set('image', action.image)
    case IMAGE_DELETE_SUCCESS:
      let images2 = state.toJS().images
      let image2 = state.toJS().image
      for (let i = 0; i < images2.length; i++) {
        let element = images2[i];
        if (parseInt(element.id) === parseInt(image2.id)) {
          images2.splice(i,1)
          i = images2.length
        }
      }
      return state
      .set('image', {})
      .set('images', fromJS(images2))
    case IMAGE_DELETE_ERROR:
      return state
      .set('image', {})
    default:
      return state;
  }
}

export default projectsImagesPageReducer;
