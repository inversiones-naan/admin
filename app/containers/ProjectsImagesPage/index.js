/**
 *
 * ProjectsImagesPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectProjectsImagesPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  FormGroup,
  Label,
  Input,
  Button,
  ModalHeader,
  Modal,
  ModalBody,
  ModalFooter,
  Form,
  CardFooter,
} from "reactstrap";

import { imagesRequest, imagesUploadRequest, changeProp, imageEnableRequest, imageDeleteRequest } from './actions';

import Paginator from 'components/Paginator';
import LoadingIndicator from 'components/LoadingIndicator';
import params from 'utils/params';

export class ProjectsImagesPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      images:[]
    };
    // file: '',imagePreviewUrl: ''
  }
  _handleSubmit(e) {
    e.preventDefault();
    // TODO: do something with -> this.state.file
    // console.log('handle uploading-', this.state.file);
  }
  _handleImageChange(e) {
    e.preventDefault();
    this.props.onChangeProp('fileList', [])

    let reader = new FileReader();
    let files = e.target.files
    for (let i = 0; i < files.length; i++) {
      let reader = new FileReader();
      reader.onloadend = this.handleUploaded.bind(this,files[i], reader, files.length)
      reader.readAsDataURL(files[i])
    }
  }
  handleUploaded (file, reader, total) {
    let fileList = this.props.projectsimagespage.fileList
    fileList.push({
      file: file,
      imagePreviewUrl: reader.result
    })
    if (total === fileList.length) {
      this.props.onChangeProp('fileList', fileList)
      this.forceUpdate()
    }    
  }
  componentDidMount () {
    const { projectId } = this.props.match.params
    // this.props.onProjectsfundingRequest(projectId, 1, '')
    // this.props.onUsersRequest()
    this.props.onImagesRequest(projectId)
  }
  handleSave () {
    const { projectId } = this.props.match.params
    this.props.onImagesUploadRequest(projectId)
  }
  clearImages () {
    this.props.onChangeProp('fileList', [])
  }
  handleEnableImage (image) {
    // console.log(image)
  }
  render() {
    const {projectsimagespage} = this.props
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12">
            <Card>
              <CardHeader>
                <strong>Agregar</strong> Imagenes
              </CardHeader>
              <CardBlock className="card-body">
                {projectsimagespage.successUpload
                ? <Card className="text-white bg-success">
                    <CardBlock className="card-body">
                      {projectsimagespage.successUpload}
                    </CardBlock>
                  </Card>
                : null}
                {projectsimagespage.errorUpload
                ? <Card className="text-white bg-danger">
                    <CardBlock className="card-body">
                      {projectsimagespage.errorUpload}
                    </CardBlock>
                  </Card>
                : null}
                {projectsimagespage.loadingUpload
                ? <LoadingIndicator />
                : <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                    <FormGroup row>
                      <Col md="3">
                        <Label htmlFor="file-multiple-input">Agregar multiples archivos</Label>
                      </Col>
                      <Col xs="12" md="9">
                        <Input type="file" id="file-multiple-input" name="file-multiple-input" multiple onChange={this._handleImageChange.bind(this)} />
                      </Col>
                    </FormGroup>
                    <Row>
                      {projectsimagespage.fileList.map( (image, index)=>{
                        const {imagePreviewUrl} = image
                        let $imagePreview = null;
                        if (imagePreviewUrl) {
                          $imagePreview = (<img className="centerImg" src={imagePreviewUrl} />);
                        } else {
                          $imagePreview = (<div className="previewText">Por favor seleccione una imagen</div>);
                        }
                        return (
                          <Col xs="12" sm="6" md="4" key={`image-display-${index}`}>
                            <Card className="card-accent-primary">
                              <CardBlock className="card-body" style={{textAlign:'center'}}>
                                <div className="imgPreview">
                                  {$imagePreview}
                                </div>
                              </CardBlock>
                            </Card>
                          </Col>
                        )
                      })}
                    </Row>
                  </Form>
                }
              </CardBlock>
              <CardFooter>
                <Button
                  type="submit"
                  size="sm"
                  color="primary"
                  onClick={this.handleSave.bind(this)}
                  disabled={projectsimagespage.loadingUpload}
                >
                  <i className="fa fa-dot-circle-o"></i> Subir Archivos
                </Button>
                <Button
                  type="reset"
                  size="sm"
                  color="danger"
                  onClick={this.clearImages.bind(this)}
                >
                  <i className="fa fa-ban"></i> Limpiar
                </Button>
              </CardFooter>
            </Card>
          </Col>
          {projectsimagespage.images.map( (image, index)=>{
            const {url} = image
            let $imagePreview = null;
            return (
              <Col xs="12" sm="12" md="6" key={`image-display-saved-${index}`}>
                <Card className="card-accent-success">
                  <CardHeader>
                    {`Imagen ${image.id}`}
                    <Label className="switch switch-sm switch-text switch-info float-right mb-0">
                      <Input
                        type="checkbox"
                        className="switch-input"
                        onChange={() => this.props.onImageEnableRequest(image)}
                        checked={image.status === 1}
                      />
                      <span className="switch-label" data-on="On" data-off="Off"></span>
                      <span className="switch-handle"></span>
                    </Label>
                  </CardHeader>
                  <CardBlock className="card-body" style={{textAlign:'center'}}>
                    {projectsimagespage.image.id === image.id
                    ? <LoadingIndicator />
                    : <div className="imgPreviewSaved">
                      <img className="centerImgSaved" src={`${params.apiUrl}${url}`} />
                    </div>}                    
                  </CardBlock>
                  <CardFooter>
                    <Button
                      outline
                      color="danger"
                      size="sm"
                      onClick={() => this.props.onImageDeleteRequest(image)}
                    >
                      Eliminar
                    </Button>
                  </CardFooter>
                </Card>
              </Col>
            )
          })}          
        </Row>
      </div>
    )
  }
}

ProjectsImagesPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  projectsimagespage: makeSelectProjectsImagesPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onImagesRequest: (projectId) => dispatch(imagesRequest(projectId)),
    onImagesUploadRequest: (projectId) => dispatch(imagesUploadRequest(projectId)),
    onChangeProp: (prop, value) => dispatch(changeProp(prop, value)),
    onImageEnableRequest: (image) => dispatch(imageEnableRequest(image)),
    onImageDeleteRequest: (image) => dispatch(imageDeleteRequest(image))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'projectsImagesPage', reducer });
const withSaga = injectSaga({ key: 'projectsImagesPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ProjectsImagesPage);
