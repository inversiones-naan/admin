
import { fromJS } from 'immutable';
import projectsImagesPageReducer from '../reducer';

describe('projectsImagesPageReducer', () => {
  it('returns the initial state', () => {
    expect(projectsImagesPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
