/**
 *
 * Asynchronously loads the component for AdminNewsPage
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
