/*
 *
 * AdminNewsPage actions
 *
 */

import {
  DEFAULT_ACTION,
  ADMIN_NEWS_ATTEMP,
  ADMIN_NEWS_SUCCESS,
  ADMIN_NEWS_FAILURE,
  ADMIN_NEWS_CHANGE_PROPS,
  ADMIN_NEWS_CREATING,
  ADMIN_NEWS_CREATED,
  ADMIN_NEWS_CREATE_ERROR,
  ADMIN_NEWS_UPDATING,
  ADMIN_NEWS_UPDATED,
  ADMIN_NEWS_UPDATE_ERROR,
  ADMIN_NEWS_DELETING,
  ADMIN_NEWS_DELETED,
  ADMIN_NEWS_DELETED_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/** 
* Obtener el listado de noticias
*/
export function adminNewsGetAttemp() {
  return {
    type: ADMIN_NEWS_ATTEMP
  }
}

/** 
* Listado de noticias por proyecto
*/
export function adminNewsGetSuccess(payload) {
  return {
    type: ADMIN_NEWS_SUCCESS,
    payload
  }
}

/** 
* Error al obtener las noticias por proyecto
*/
export function adminNewsGetFailure(error) {
  return {
    type: ADMIN_NEWS_FAILURE,
    error
  }
}

/** 
* Cambiar las propiedades del componente
*/
export function adminNewsChangeProps(object) {
  return {
    type: ADMIN_NEWS_CHANGE_PROPS,
    object
  }
}


/** 
* Crear Mensaje
*/
export function adminNewsCreating() {
  return {
    type: ADMIN_NEWS_CREATING
  }
}

/** 
* Mensaje creado
*/
export function adminNewsCreated(payload) {
  return {
    type: ADMIN_NEWS_CREATED,
    payload
  }
}

/** 
* Error al crear el mensaje
*/
export function adminNewsCreateError(error) {
  return {
    type: ADMIN_NEWS_CREATE_ERROR,
    error
  }
}

/** 
* Actualizar Mensaje
*/
export function adminNewsUpdating() {
  return {
    type: ADMIN_NEWS_UPDATING
  }
}

/** 
* Mensaje aptualizado
*/
export function adminNewsUpdated(payload) {
  return {
    type: ADMIN_NEWS_UPDATED,
    payload
  }
}

/** 
* Error al actualizar el mensaje
*/
export function adminNewsUpdateError(error) {
  return {
    type: ADMIN_NEWS_UPDATE_ERROR,
    error
  }
}

/** 
* Eliminar Mensaje
*/
export function adminNewsDeleting() {
  return {
    type: ADMIN_NEWS_DELETING
  }
}

/** 
* Mensaje eliminado
*/
export function adminNewsDeleted(payload) {
  return {
    type: ADMIN_NEWS_DELETED,
    payload
  }
}

/** 
* Error al eliminar el mensaje
*/
export function adminNewsDeleteError(error) {
  return {
    type: ADMIN_NEWS_DELETED_ERROR,
    error
  }
}