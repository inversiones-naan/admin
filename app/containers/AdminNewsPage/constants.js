/*
 *
 * AdminNewsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/AdminNewsPage/DEFAULT_ACTION';

export const ADMIN_NEWS_ATTEMP = 'app/AdminNewsPage/ADMIN_NEWS_ATTEMP';
export const ADMIN_NEWS_SUCCESS = 'app/AdminNewsPage/ADMIN_NEWS_SUCCESS';
export const ADMIN_NEWS_FAILURE = 'app/AdminNewsPage/ADMIN_NEWS_FAILURE';

export const ADMIN_NEWS_CHANGE_PROPS = 'app/AdminNewsPage/ADMIN_NEWS_CHANGE_PROPS';

export const ADMIN_NEWS_CREATING = 'app/AdminNewsPage/ADMIN_NEWS_CREATING';
export const ADMIN_NEWS_CREATED = 'app/AdminNewsPage/ADMIN_NEWS_CREATED';
export const ADMIN_NEWS_CREATE_ERROR = 'app/AdminNewsPage/ADMIN_NEWS_CREATE_ERROR';

export const ADMIN_NEWS_UPDATING = 'app/AdminNewsPage/ADMIN_NEWS_UPDATING';
export const ADMIN_NEWS_UPDATED = 'app/AdminNewsPage/ADMIN_NEWS_UPDATED';
export const ADMIN_NEWS_UPDATE_ERROR = 'app/AdminNewsPage/ADMIN_NEWS_UPDATE_ERROR';

export const ADMIN_NEWS_DELETING = 'app/AdminNewsPage/ADMIN_NEWS_DELETING';
export const ADMIN_NEWS_DELETED = 'app/AdminNewsPage/ADMIN_NEWS_DELETED';
export const ADMIN_NEWS_DELETED_ERROR = 'app/AdminNewsPage/ADMIN_NEWS_DELETED_ERROR';