import { createSelector } from 'reselect';

/**
 * Direct selector to the adminNewsPage state domain
 */
const selectAdminNewsPageDomain = (state) => state.get('adminNewsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AdminNewsPage
 */

const makeSelectAdminNewsPage = () => createSelector(
  selectAdminNewsPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectAdminNewsPage;
export {
  selectAdminNewsPageDomain,
};
