/*
 *
 * AdminNewsPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  ADMIN_NEWS_ATTEMP,
  ADMIN_NEWS_SUCCESS,
  ADMIN_NEWS_FAILURE,
  ADMIN_NEWS_CHANGE_PROPS,
  ADMIN_NEWS_CREATING,
  ADMIN_NEWS_CREATED,
  ADMIN_NEWS_CREATE_ERROR,
  ADMIN_NEWS_UPDATING,
  ADMIN_NEWS_UPDATED,
  ADMIN_NEWS_UPDATE_ERROR,
  ADMIN_NEWS_DELETING,
  ADMIN_NEWS_DELETED,
  ADMIN_NEWS_DELETED_ERROR,
} from './constants';

const initialState = fromJS({
  loadingGet: false,
  projectId: null,
  news: [],
  errorGet: null,
  modal: false,
  textNews: '',
  newsSuccess: null,
  newsError: null,
  newsLoading: false,
  newsUpdating: false,
  modalUpdate: null,
  newsId: null
});

function adminNewsPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case ADMIN_NEWS_ATTEMP:
      return state
      .set('loadingGet', true)
      .set('news', [])
      .set('errorGet', null)
    case ADMIN_NEWS_SUCCESS:
      return state
      .set('loadingGet', false)
      .set('news', action.payload)
      .set('errorGet', null)
    case ADMIN_NEWS_FAILURE:
      return state
      .set('loadingGet', false)
      .set('news', [])
      .set('errorGet', action.error)
    case ADMIN_NEWS_CHANGE_PROPS:
      let newState = JSON.parse(JSON.stringify(state))
      const { object } = action
      for (const key in object) {
        if (newState.hasOwnProperty(key)) {
          newState[key] = object[key];
        }
      }
      return fromJS(newState)
    case ADMIN_NEWS_CREATING:
      return state
      .set('newsLoading', true)
      .set('newsSuccess', null)
      .set('newsError', null)
    case ADMIN_NEWS_CREATED:
      return state
      .set('newsLoading', false)
      .set('newsSuccess', 'Se ha registrado el mensaje')
      .set('newsError', null)
    case ADMIN_NEWS_CREATE_ERROR:
      return state
      .set('newsLoading', false)
      .set('newsSuccess', null)
      .set('newsError', action.error)
    case ADMIN_NEWS_UPDATING:
      return state
      .set('newsUpdating', true)
      .set('newsSuccess', null)
      .set('newsError', null)
    case ADMIN_NEWS_UPDATED:
      return state
      .set('newsUpdating', false)
      .set('newsSuccess', 'Se ha actualizado el mensaje')
      .set('newsError', null)
    case ADMIN_NEWS_UPDATE_ERROR:
      return state
      .set('newsUpdating', false)
      .set('newsSuccess', null)
      .set('newsError', action.error)
    case ADMIN_NEWS_DELETING:
      return state
      .set('newsUpdating', true)
      .set('newsSuccess', null)
      .set('newsError', null)
    case ADMIN_NEWS_DELETED:
      return state
      .set('newsUpdating', false)
      .set('newsSuccess', 'Se ha eliminado el mensaje')
      .set('newsError', null)
    case ADMIN_NEWS_DELETED_ERROR:
      return state
      .set('newsUpdating', false)
      .set('newsSuccess', null)
      .set('newsError', action.error)
    default:
      return state;
  }
}

export default adminNewsPageReducer;
