
import { fromJS } from 'immutable';
import adminNewsPageReducer from '../reducer';

describe('adminNewsPageReducer', () => {
  it('returns the initial state', () => {
    expect(adminNewsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
