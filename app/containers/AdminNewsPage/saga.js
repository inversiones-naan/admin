import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { ADMIN_NEWS_ATTEMP, ADMIN_NEWS_CREATING, ADMIN_NEWS_UPDATING, ADMIN_NEWS_DELETING } from './constants';
import { adminNewsGetSuccess, adminNewsGetFailure, adminNewsCreated, adminNewsCreateError , adminNewsGetAttemp, adminNewsUpdated, adminNewsUpdateError, adminNewsDeleted, adminNewsDeleteError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectAdminNewsPage from './selectors';
import { makeSelectToken, makeSelectUser } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(ADMIN_NEWS_ATTEMP, getNews)
  yield takeLatest(ADMIN_NEWS_CREATING, createNews)
  yield takeLatest(ADMIN_NEWS_UPDATING, updateNews)
  yield takeLatest(ADMIN_NEWS_DELETING, deleteNews)
}

export function* getNews () {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(adminNewsGetFailure('Error token'));
    return
  }
  const adminnewspage = yield select(makeSelectAdminNewsPage());
  const { projectId } = adminnewspage
  // console.log(payload)
  const requestURL = `${params.apiUrl}project-news/?having={"projectId":"${projectId}"}&include=projects&sort={"id":-1}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    console.log(result)
    if (result.error) {
      yield put(adminNewsGetFailure(result.error.message));
    } else {
      yield put(adminNewsGetSuccess(result.projectsNews));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(adminNewsGetFailure(err));
  }
}

export function* createNews () {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(adminNewsCreateError('Error token'));
    return
  }

  const user = yield select(makeSelectUser())

  const adminnewspage = yield select(makeSelectAdminNewsPage());
  const { projectId, textNews } = adminnewspage
  
  const payload = {
    projectId,
    text: textNews,
    creatorId: user.id,
  }

  const requestURL = `${params.apiUrl}project-news`;
  const options = {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${authToken}`
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(adminNewsCreateError(result.error.message));
    } else {
      yield put(adminNewsCreated());
      yield put(adminNewsGetAttemp());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(adminNewsCreateError(err));
  }
}

export function* updateNews () {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(adminNewsCreateError('Error token'));
    return
  }

  const adminnewspage = yield select(makeSelectAdminNewsPage());
  const { modalUpdate, textNews } = adminnewspage
  
  const payload = {
    id: modalUpdate,
    text: textNews
  }

  const requestURL = `${params.apiUrl}project-news/${modalUpdate}`;
  const options = {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${authToken}`
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(adminNewsUpdateError(result.error.message));
    } else {
      yield put(adminNewsUpdated());
      yield put(adminNewsGetAttemp());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(adminNewsUpdateError(err));
  }
}


export function* deleteNews () {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(adminNewsCreateError('Error token'));
    return
  }

  const adminnewspage = yield select(makeSelectAdminNewsPage());
  const { modalUpdate } = adminnewspage

  const requestURL = `${params.apiUrl}project-news/${modalUpdate}`;
  const options = {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(adminNewsDeleteError(result.error.message));
    } else {
      yield put(adminNewsDeleted());
      yield put(adminNewsGetAttemp());
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(adminNewsDeleteError(err));
  }
}