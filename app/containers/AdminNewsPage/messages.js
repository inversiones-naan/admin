/*
 * AdminNewsPage Messages
 *
 * This contains all the text for the AdminNewsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AdminNewsPage.header',
    defaultMessage: 'This is AdminNewsPage container !',
  },
});
