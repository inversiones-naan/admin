/**
 *
 * AdminNewsPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

// Selectores
import makeSelectAdminNewsPage from './selectors';

// Acciones
import { adminNewsChangeProps, adminNewsGetAttemp, adminNewsCreating, adminNewsUpdating, adminNewsDeleting } from './actions';

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

// Componentes
import LoadingIndicator from 'components/LoadingIndicator';

// Externos
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  FormGroup,
  Label,
  Input,
  CardFooter,
  Button,
  Form,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";

export class AdminNewsPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    const { projectId } = this.props.match.params
    this.props.onAdminNewsChangeProps({projectId})
    this.props.onAdminNewsGetAttemp()
  }
  render() {
    const { loadingGet, news, modal, textNews, newsSuccess, newsError, newsLoading, modalUpdate, newsUpdating, adminNewsDeleting } = this.props.adminnewspage
    return (
      <div className="animated fadeIn">
        <Row>
            <Col xs="12" sm="12">
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Mensajes en el proyecto
                </CardHeader>
                <CardBlock className="card-body">
                  <Button
                    color="primary float-left"
                    onClick={() => {
                      this.props.onAdminNewsChangeProps({
                        modal: true,
                        newsSuccess: null,
                        newsError: null,
                        textNews: ''
                      })
                    }}
                  >Agregar Mensaje</Button>
                  <br />
                  <br />
                  {loadingGet
                  ? <LoadingIndicator />
                  : <ReactTable
                    data={news}
                    defaultPageSize={5}
                    filterable
                    defaultFilterMethod={(filter, row) =>
                      String(row[filter.id]) === filter.value}
                    columns={[{
                      Header: 'Mensaje',
                      accessor: 'text',
                      style: { textAlign: 'center' },
                      filterMethod: (filter, row) => {
                        return row[filter.id].name.indexOf(filter.value) > -1
                      },
                      Cell: row => (
                        <span
                          onClick={() => {
                            this.props.onAdminNewsChangeProps({
                              modalUpdate: row.original.id,
                              newsUpdating: false,
                              newsSuccess: null,
                              newsError: null,
                              textNews: row.value
                            })
                          }}
                          style={{width: '100%'}}
                        >{row.value}</span>
                      )
                    },{
                      Header: 'Creado',
                      accessor: 'updatedAt'
                    }]}
                    className="-striped -highlight"
                    previousText='Anterior'
                    nextText='Siguiente'
                    loadingText='Cargando...'
                    noDataText='No hay mensajes'
                    pageText='Página'
                    ofText='de'
                    rowsText='filas'
                  />
                  }
                </CardBlock>
              </Card>
            </Col>
        </Row>
        <Modal
          isOpen={modal}
          toggle={()=>{
            this.props.onAdminNewsChangeProps({
              modal: false,
              newsSuccess: null,
              newsError: null,
              textNews: ''
            })
          }}
        >
          <ModalHeader
            toggle={()=>{
              this.props.onAdminNewsChangeProps({
                modal: false,
                newsSuccess: null,
                newsError: null,
                textNews: ''
              })
            }}
          >
            Agregar un Mensaje al Proyecto
          </ModalHeader>
          <ModalBody>
            {newsSuccess
            ? <Card className="text-white bg-success">
                <CardBlock className="card-body">
                  {newsSuccess}
                </CardBlock>
              </Card>                  
            : null}
            {newsError
            ? <Card className="text-white bg-danger">
                <CardBlock className="card-body">
                  {newsError}
                </CardBlock>
              </Card>
            : null}
            {newsLoading
            ? <LoadingIndicator />
            : <FormGroup>
              <Label htmlFor="city">Escriba el mensaje</Label>
              <Input
                type="textarea"
                value={textNews}
                onChange={(evt) => this.props.onAdminNewsChangeProps({textNews: evt.target.value})}
              />
            </FormGroup>
            }
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={this.props.onAdminNewsCreating}
            >Crear</Button>
            {' '}
            <Button
              color="secondary"
              onClick={()=>{
                this.props.onAdminNewsChangeProps({
                  modal: false,
                  newsSuccess: null,
                  newsError: null,
                  textNews: ''
                })
              }}
            >Cancel</Button>
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={!!modalUpdate}
          toggle={()=>{
            this.props.onAdminNewsChangeProps({
              modalUpdate: false,
              newsSuccess: null,
              newsError: null,
              textNews: ''
            })
          }}
        >
          <ModalHeader
            toggle={()=>{
              this.props.onAdminNewsChangeProps({
                modalUpdate: false,
                newsSuccess: null,
                newsError: null,
                textNews: ''
              })
            }}
          >
            Edita el mensaje
          </ModalHeader>
          <ModalBody>
            {newsSuccess
            ? <Card className="text-white bg-success">
                <CardBlock className="card-body">
                  {newsSuccess}
                </CardBlock>
              </Card>                  
            : null}
            {newsError
            ? <Card className="text-white bg-danger">
                <CardBlock className="card-body">
                  {newsError}
                </CardBlock>
              </Card>
            : null}
            {newsUpdating
            ? <LoadingIndicator />
            : <FormGroup>
              <Label htmlFor="city">Escriba el mensaje</Label>
              <Input
                type="textarea"
                value={textNews}
                onChange={(evt) => this.props.onAdminNewsChangeProps({textNews: evt.target.value})}
              />
            </FormGroup>
            }
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={this.props.onAdminNewsUpdating}
            >Actualizar</Button>
            {' '}
            <Button
              color="danger"
              onClick={this.props.onAdminNewsDeleting}
            >Eliminar</Button>
            {' '}
            <Button
              color="secondary"
              onClick={()=>{
                this.props.onAdminNewsChangeProps({
                  modalUpdate: false,
                  newsSuccess: null,
                  newsError: null,
                  textNews: ''
                })
              }}
            >Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

AdminNewsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  adminnewspage: makeSelectAdminNewsPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onAdminNewsChangeProps: (object) => dispatch(adminNewsChangeProps(object)),
    onAdminNewsGetAttemp: () => dispatch(adminNewsGetAttemp()),
    onAdminNewsCreating: () => dispatch(adminNewsCreating()),
    onAdminNewsUpdating: () => dispatch(adminNewsUpdating()),
    onAdminNewsDeleting: () => dispatch(adminNewsDeleting())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'adminNewsPage', reducer });
const withSaga = injectSaga({ key: 'adminNewsPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AdminNewsPage);
