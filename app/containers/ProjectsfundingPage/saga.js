import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PROJECTSFUNDING_REQUEST, PROJECTSFUNDING_USERS_REQUEST, FUNDING_CREATING, STATUS_REQUEST } from './constants';
import { projectsfundingSuccess, projectsfundingError, usersSuccess, usersError, fundingSuccess, fundingError, statusSuccess, statusError, projectsfundingRequest } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectProjectsfundingPage from './selectors';
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PROJECTSFUNDING_REQUEST, getProjectsFunding);
  yield takeLatest(PROJECTSFUNDING_USERS_REQUEST, getUsers);  
  yield takeLatest(FUNDING_CREATING, createFund)
  yield takeLatest(STATUS_REQUEST, changeStatus)
}
/**
 * Obtener las transacciones por proyectos de la API
 */
export function* getProjectsFunding() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(projectsfundingError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsfundingPage());
  const {payload, word, id} = reducer
  
  const requestURL = `${params.apiUrl}funding/?having={"projectId":"${id}"}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    console.log(requestURL)
    console.log(result)
    if (result.error) {
      yield put(projectsfundingError(payload.error.message));
    } else {
      yield put(projectsfundingSuccess(result.funding));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectsfundingError(err));
  }
}

/**
 * Obtener los usuarios de la API
 */
export function* getUsers() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(usersError('Error token'));
    return
  }
  
  const requestURL = `${params.apiUrl}users?sort={"firstName":1}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(usersError(payload.error.message));
    } else {
      yield put(usersSuccess(result.users));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(usersError(err));
  }
}
/**
 * Crear un fondo para el proyecto
 */
export function* createFund() {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(fundingError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsfundingPage());
  const {userId, id, fundingValue, fundingType} = reducer
  if (userId === null) {
    yield put(fundingError('Seleccione un usuario'));
    return
  }
  if (fundingValue <= 0) {
    yield put(fundingError('Agregue una cantidad'));
    return
  }
  console.log(id)
  const payload = {
    projectId: id,
    userId: userId,
    creatorId: 0,
    value: fundingValue,
    type: fundingType,
    status: 0// Al crear por defecto el monto no esta activado
  }
  const requestURL = `${params.apiUrl}funding`;
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`,
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(fundingError(payload.error.message));
    } else {
      yield put(fundingSuccess());
      // Recargar la tabla
      yield put(projectsfundingRequest())
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(fundingError('Ocurrio un error al crear la transacción'));
  }
}

/**
 * Cambiar el estado del fondeo
 */
export function* changeStatus() {
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(fundingError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsfundingPage());
  const {objFund, status} = reducer

  const payload = {
    status: status,
  }
  const requestURL = `${params.apiUrl}funding/${objFund.id}`;
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`,
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(statusError(payload.error.message));
    } else {
      yield put(statusSuccess(payload.fund));
      // Recargar la tabla
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(statusError('Error al cambiar de estado'));
  }
}