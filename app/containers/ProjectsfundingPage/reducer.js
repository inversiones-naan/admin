/*
 *
 * ProjectsfundingPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  PROJECTSFUNDING_REQUEST,
  PROJECTSFUNDING_SUCCESS,
  PROJECTSFUNDING_ERROR,
  PROJECTSFUNDING_USERS_REQUEST,
  PROJECTSFUNDING_USERS_SUCCESS,
  PROJECTSFUNDING_USERS_ERROR,
  PROJECTSFUNDING_CHANGE_PROP,
  FUNDING_CREATING,
  FUNDING_SUCCESS,
  FUNDING_ERROR,
  STATUS_REQUEST,
  STATUS_SUCCESS,
  STATUS_ERROR,
  PROJECTSFUNDING_CHANGE_PROPS,
} from './constants';

const initialState = fromJS({
  funding: [],
  loading: false,
  error: false,
  id: null,
  users: [],
  userId: null,
  loadingCreating: false,
  objFund: {},
  status: null,
  fundLoading: false,
  fundSuccess: false,
  fundError: false,
  fundingModal: false,
  fundingType: '0',
  fundingSuccess: null,
  fundingError: null,
  fundingValue: '',
  fundingUserId: 0
});

function projectsfundingPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PROJECTSFUNDING_REQUEST:
      return state
      .set('funding', fromJS([]))
      .set('loading', true)
      .set('error', false)
    case PROJECTSFUNDING_SUCCESS:
      return state
      .set('funding', fromJS(action.payload))
      .set('loading', false)
      .set('error', false)
    case PROJECTSFUNDING_ERROR:
      return state
      .set('funding', [])
      .set('loading', false)
      .set('error', false)
    case PROJECTSFUNDING_USERS_REQUEST:
      return state
      .set('users', [])
      .set('userId', null)
      .set('fund', 0)    
    case PROJECTSFUNDING_USERS_SUCCESS:
      let userId = null
      if (action.payload.length > 0) {
        userId = action.payload[0].id
      }
      return state
      .set('users', fromJS(action.payload))
      .set('userId', userId)
      .set('fund', 0)
    case PROJECTSFUNDING_USERS_ERROR:
      return state
      .set('users', [])
      .set('userId', null)
      .set('fund', 0)
    case PROJECTSFUNDING_CHANGE_PROP:
      return state
      .set(action.prop, action.value)
    case FUNDING_CREATING:
      return state
      .set('loadingCreating', true)
      .set('successFunding', false)
      .set('errorFunding', false)
    case FUNDING_SUCCESS:
      return state
      .set('loadingCreating', false)
      .set('fundingSuccess', 'Fondo Agregado')
      .set('fundingError', false)
    case FUNDING_ERROR:
      return state
      .set('loadingCreating', false)
      .set('fundingSuccess', false)
      .set('fundingError', action.error)
    case STATUS_REQUEST:
      return state
      .set('status', action.status)
      .set('fundLoading', true)
      .set('fundSuccess', false)
      .set('fundError', false)
    case STATUS_SUCCESS:
      let newState = state.toJS()
      let project = newState.objFund
      let projects = newState.funding
      for (let i = 0; i < projects.length; i++) {
        let element = projects[i];
        if (parseInt(element.id) === parseInt(action.fund.id)) {
          element.status = action.fund.status
          project = JSON.parse(JSON.stringify(element))
        }
      }
      newState.fundLoading = false
      newState.fundSuccess = 'Estado actualizado'
      newState.fundError = false
      return fromJS(newState)
    case STATUS_ERROR:
      return state
      .set('status', null)
      .set('fundLoading', false)
      .set('fundSuccess', false)
      .set('fundError', action.error)
    case PROJECTSFUNDING_CHANGE_PROPS: 
      let copyState = state.toJS()
      let { object } = action
      for (const key in object) {
        if (object.hasOwnProperty(key)) {
          copyState[key] = object[key];
        }
      }
      return fromJS(copyState)
    default:
      return state;
  }
}

export default projectsfundingPageReducer;
