
import { fromJS } from 'immutable';
import projectsfundingPageReducer from '../reducer';

describe('projectsfundingPageReducer', () => {
  it('returns the initial state', () => {
    expect(projectsfundingPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
