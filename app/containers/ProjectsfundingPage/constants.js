/*
 *
 * ProjectsfundingPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ProjectsfundingPage/DEFAULT_ACTION';
export const PROJECTSFUNDING_REQUEST = 'app/ProjectsfundingPage/PROJECTSFUNDING_REQUEST';
export const PROJECTSFUNDING_SUCCESS = 'app/ProjectsfundingPage/PROJECTSFUNDING_SUCCESS';
export const PROJECTSFUNDING_ERROR = 'app/ProjectsfundingPage/PROJECTSFUNDING_ERROR';
export const PROJECTSFUNDING_USERS_REQUEST = 'app/ProjectsfundingPage/PROJECTSFUNDING_USERS_REQUEST';
export const PROJECTSFUNDING_USERS_SUCCESS = 'app/ProjectsfundingPage/PROJECTSFUNDING_USERS_SUCCESS';
export const PROJECTSFUNDING_USERS_ERROR = 'app/ProjectsfundingPage/PROJECTSFUNDING_USERS_ERROR';
export const PROJECTSFUNDING_CHANGE_PROP = 'app/ProjectsfundingPage/PROJECTSFUNDING_CHANGE_PROP';

export const FUNDING_CREATING = 'app/ProjectsfundingPage/FUNDING_CREATING';
export const FUNDING_SUCCESS = 'app/ProjectsfundingPage/FUNDING_SUCCESS';
export const FUNDING_ERROR = 'app/ProjectsfundingPage/FUNDING_ERROR';

export const STATUS_REQUEST = 'app/ProjectsfundingPage/STATUS_REQUEST';
export const STATUS_SUCCESS = 'app/ProjectsfundingPage/STATUS_SUCCESS';
export const STATUS_ERROR = 'app/ProjectsfundingPage/STATUS_ERROR';

export const PROJECTSFUNDING_CHANGE_PROPS = 'app/ProjectsfundingPage/PROJECTSFUNDING_CHANGE_PROPS';