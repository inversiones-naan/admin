import { createSelector } from 'reselect';

/**
 * Direct selector to the projectsfundingPage state domain
 */
const selectProjectsfundingPageDomain = (state) => state.get('projectsfundingPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ProjectsfundingPage
 */

const makeSelectProjectsfundingPage = () => createSelector(
  selectProjectsfundingPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectProjectsfundingPage;
export {
  selectProjectsfundingPageDomain,
};
