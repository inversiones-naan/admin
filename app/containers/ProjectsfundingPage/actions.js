/*
 *
 * ProjectsfundingPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PROJECTSFUNDING_REQUEST,
  PROJECTSFUNDING_SUCCESS,
  PROJECTSFUNDING_ERROR,
  PROJECTSFUNDING_USERS_REQUEST,
  PROJECTSFUNDING_USERS_SUCCESS,
  PROJECTSFUNDING_USERS_ERROR,
  PROJECTSFUNDING_CHANGE_PROP,
  FUNDING_CREATING,
  FUNDING_SUCCESS,
  FUNDING_ERROR,
  STATUS_REQUEST,
  STATUS_SUCCESS,
  STATUS_ERROR,
  PROJECTSFUNDING_CHANGE_PROPS
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}


/**
 * Solicita las transacciones por proyectos al API
 *
 * @return {object}    An action object with a type of PROJECTSFUNDING_REQUEST
 */
export function projectsfundingRequest(id, page, word) {
  return {
    type: PROJECTSFUNDING_REQUEST,
    id,
    page,
    word
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of PROJECTSFUNDING_SUCCESS
 */
export function projectsfundingSuccess(payload) {
  return {
    type: PROJECTSFUNDING_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of PROJECTSFUNDING_ERROR
 */
export function projectsfundingError(error) {
  return {
    type: PROJECTSFUNDING_ERROR,
    error,
  };
}


/**
 * Solicita los usuarios del sistema
 *
 * @return {object} An action object with a type of PROJECTSFUNDING_USERS_REQUEST
 */
export function usersRequest() {
  return {
    type: PROJECTSFUNDING_USERS_REQUEST,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of PROJECTSFUNDING_SUCCESS
 */
export function usersSuccess(payload) {
  return {
    type: PROJECTSFUNDING_USERS_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of PROJECTSFUNDING_ERROR
 */
export function usersError(error) {
  return {
    type: PROJECTSFUNDING_USERS_ERROR,
    error,
  };
}

export function changeProp(prop, value) {
  return {
    type: PROJECTSFUNDING_CHANGE_PROP,
    prop,
    value,
  };
}


/**
 * Solicita las transacciones por proyectos al API
 *
 * @return {object} An action object with a type of PROJECTSFUNDING_REQUEST
 */
export function fundingCreating() {
  return {
    type: FUNDING_CREATING,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object} An action object with a type of FUNDING_SUCCESS
 */
export function fundingSuccess(payload) {
  return {
    type: FUNDING_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object} An action object with a type of FUNDING_ERROR
 */
export function fundingError(error) {
  return {
    type: FUNDING_ERROR,
    error,
  };
}

/**
 * Chambiar el estatus del fondo
 * 
 * @param {Int} status An action with a type of STATUS_REQUEST
 */
export function statusRequest(status) {
  return {
    type: STATUS_REQUEST,
    status,
  };
}

/**
 *  Respuesta exitosa del API
 * 
 * @param {Object} fund An action with a type of STATUS_SUCCESS
 */
export function statusSuccess(fund) {
  return {
    type: STATUS_SUCCESS,
    fund,
  };
}

/**
 *  Respuesta error del API
 * 
 * @param {String} fund An action with a type of STATUS_SUCCESS
 */
export function statusError(error) {
  return {
    type: STATUS_ERROR,
    error,
  };
}

export function changeProps (object) {
  return {
    type: PROJECTSFUNDING_CHANGE_PROPS,
    object
  }
}