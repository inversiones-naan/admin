/**
 *
 * ProjectsfundingPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Link } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectProjectsfundingPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { projectsfundingRequest, usersRequest, changeProp, fundingCreating, statusRequest, changeProps } from './actions';

import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  FormGroup,
  Label,
  Input,
  CardFooter,
  Button,
  Form,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";

import Paginator from 'components/Paginator'

import LoadingIndicator from 'components/LoadingIndicator';

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

export class ProjectsfundingPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    const { projectId } = this.props.match.params
    this.props.onChangeProps({id:projectId})
    this.props.onProjectsfundingRequest()
    this.props.onUsersRequest()
  }
  handleSearch (evt) {
    const { projectsfundingpage } = this.props
    const { projectId } = this.props.match.params
    this.props.onProjectsfundingRequest(projectId, projectsfundingpage.payload.current, evt.target.value)
  }
  handleRequest (page) {
    const { projectsfundingpage } = this.props
    const { projectId } = this.props.match.params
    this.props.onProjectsfundingRequest(projectId, page, projectsfundingpage.word)
  }
  status (objFund) {
    let {status} = objFund
    if (status === "0") {
      return <Badge color="warning" onClick={()=>this.props.onChangeProp('objFund', objFund)}>Pendiente</Badge>
    } else if (status === "1") {
      return <Badge color="success" onClick={()=>this.props.onChangeProp('objFund', objFund)}>Confirmado</Badge>
    } else if (status === "2") {
      return <Badge color="success">Cancelado</Badge>
    }
    return null
  }
  closeModalFunding () {
    this.props.onChangeProps({
      fundingModal: false,
      fundingType: '0',
      fundingSuccess: null,
      fundingError: null,
      fundingValue: '',
      fundingUserId: 0
    })
  }
  render() {
    const { projectsfundingpage } = this.props
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Transacciones del Proyecto
              </CardHeader>
              <CardBlock className="card-body">
                <Button color="primary float-left" onClick={() => {this.props.onChangeProps({fundingModal: true, fundingType: '0'})}}>Fondear</Button>
                <Button color="primary float-right" onClick={() => {this.props.onChangeProps({fundingModal: true, fundingType: '1'})}}>Pagar</Button>
                <br />
                <br />
                {projectsfundingpage.loading
                ? <LoadingIndicator />
                : <ReactTable
                  data={projectsfundingpage.funding}
                  defaultPageSize={5}
                  filterable
                  defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value}
                  columns={[{
                    Header: 'Proyecto',
                    accessor: 'project',
                    style: { textAlign: 'center' },
                    filterMethod: (filter, row) => {
                      return row[filter.id].name.indexOf(filter.value) > -1
                    },
                    Cell: row => (
                      <div>
                        {row.value.name}
                      </div>
                    )
                  },{
                    Header: 'Usuario',
                    accessor: 'user',
                    filterMethod: (filter, row) => {
                      return (row[filter.id].firstName.indexOf(filter.value) > -1) || (row[filter.id].lastName.indexOf(filter.value) > -1)
                    },
                    Cell: row => (
                      <div>
                        {`${row.value.firstName} ${row.value.lastName}`}
                      </div>
                    )
                  },{
                    Header: 'Monto',
                    accessor: 'value',
                    Cell: row => (
                      <div>
                        {row.value}
                      </div>
                    )
                  },{
                    Header: 'Estado',
                    accessor: 'status',
                    filterMethod: (filter, row) => {
                      if (filter.value === "all") {
                        return true;
                      }
                      return row[filter.id] === filter.value
                    },
                    Filter: ({ filter, onChange }) =>
                      <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}
                      >
                        <option value="all">Mostrar Todo</option>
                        <option value="0">No activo</option>
                        <option value="1">Activo</option>
                      </select>
                    ,
                    Cell: row => (
                      <span onClick={()=>this.props.onChangeProp('objFund', row.original)}>
                        <span style={{
                          color: row.value === '0' ? '#ff0000'
                          : row.value === '1' ? '#00ff00'
                          : '#000000',
                          transition: 'all .3s ease'
                        }}>
                          &#x25cf;
                        </span> {
                          row.value === '0' ? 'No activo'
                          : row.value === '1' ? `Activo`
                          : 'Indefinido'
                        }
                      </span>
                    )
                  },{
                    Header: 'Tipo',
                    accessor: 'type',
                    filterMethod: (filter, row) => {
                      if (filter.value === "all") {
                        return true;
                      }
                      return row[filter.id] === filter.value
                    },
                    Filter: ({ filter, onChange }) =>
                      <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}
                      >
                        <option value="all">Mostrar Todo</option>
                        <option value="0">Inversión</option>
                        <option value="1">Pago</option>
                      </select>
                    ,
                    Cell: row => (
                      <span>
                        <span style={{
                          color: row.value === '0' ? '#00ff00'
                            : row.value === '1' ? '#ff0000'
                            : '#000000',
                          transition: 'all .3s ease'
                        }}>
                          &#x25cf;
                        </span> {
                          row.value === '0' ? 'Inversión'
                          : row.value === '1' ? `Pago`
                          : 'Indefinido'}
                      </span>
                    )
                  }]}
                  className="-striped -highlight"
                  previousText='Anterior'
                  nextText='Siguiente'
                  loadingText='Cargando...'
                  noDataText='No hay datos'
                  pageText='Página'
                  ofText='de'
                  rowsText='filas'
                />
                }
              </CardBlock>
            </Card>
          </Col>
        </Row>
        <Modal
          isOpen={projectsfundingpage.objFund.id?true:false}
          toggle={()=>{
            this.props.onChangeProps({objFund: {},fundSuccess: null, fundError: null})
          }}
          className={'modal-sm'}
        >
          <ModalHeader
            toggle={()=>{
              this.props.onChangeProps({objFund: {},fundSuccess: null, fundError: null})
            }}
          >
            Estado transacción
          </ModalHeader>
          <ModalBody>
            {projectsfundingpage.fundSuccess
            ? <Card className="text-white bg-success">
                <CardBlock className="card-body">
                  {projectsfundingpage.fundSuccess}
                </CardBlock>
              </Card>                  
            : null}
            {projectsfundingpage.fundError
            ? <Card className="text-white bg-danger">
                <CardBlock className="card-body">
                  {projectsfundingpage.fundError}
                </CardBlock>
              </Card>
            : null}
            {projectsfundingpage.fundLoading
            ? <LoadingIndicator />
            : projectsfundingpage.objFund.status === '0' && projectsfundingpage.objFund.type === '0'
            ?'Al activar esta transacción acumulará su valor al fondeo del proyecto.'
            : projectsfundingpage.objFund.status === '1' && projectsfundingpage.objFund.type === '0'
            ?'Al desactivar esta transacción restará su valor al fondeo del proyecto'
            : projectsfundingpage.objFund.status === '0' && projectsfundingpage.objFund.type === '1'
            ?'Al activar esta transacción autoriza el pago al inversionista'
            :'Al desactivar esta transacción inhabilita el pago al inversionista'}
          </ModalBody>
          <ModalFooter>
            {projectsfundingpage.objFund.status === '0'
            ? <Button color="primary" onClick={this.props.onStatusRequest.bind(this, 1)}>Activar</Button>
            : <Button color="primary" onClick={this.props.onStatusRequest.bind(this, 0)}>Desactivar</Button> }
            {' '}
            <Button color="secondary" onClick={()=>{
              this.props.onChangeProps({objFund: {},fundSuccess: null, fundError: null})
            }}>Cancel</Button>
          </ModalFooter>
        </Modal>
        {/*Formulario fondear o pagar a usuario*/}
        <Modal
          isOpen={projectsfundingpage.fundingModal}
          toggle={this.closeModalFunding.bind(this)}
        >
          <ModalHeader
            toggle={this.closeModalFunding.bind(this)}
          >
            {projectsfundingpage.fundingType === '0'
            ? 'Invertir'
            : 'Pagar'}            
          </ModalHeader>
          <ModalBody>
            {projectsfundingpage.fundingSuccess
            ? <Card className="text-white bg-success">
                <CardBlock className="card-body">
                  {projectsfundingpage.fundingSuccess}
                </CardBlock>
              </Card>                  
            : null}
            {projectsfundingpage.fundingError
            ? <Card className="text-white bg-danger">
                <CardBlock className="card-body">
                  {projectsfundingpage.fundingError}
                </CardBlock>
              </Card>                  
            : null}
            {projectsfundingpage.loadingCreating
            ? <LoadingIndicator />
            : projectsfundingpage.fundingType === '0'
            ?'Ingrese el valor de la inversión y el usuario a quien se registrará.'
            :'Ingrese el valor a pagar y el usuario a quien se registrará el pago'}
            <br />
            <br />
            <FormGroup row>                    
              <Col md="3">
                <Label htmlFor="select">Usuario</Label>
              </Col>
              <Col xs="12" md="9">
                <Input
                  ref="user"
                  type="select"
                  name="select"
                  id="selectUser"
                  value={this.props.userId}
                  onChange={(evt) => {
                    this.props.onChangeProp('userId', evt.target.value)
                  }}
                >
                {projectsfundingpage.users.map( (user, index)=>{ 
                  return (
                    <option key={`select-builder-${user.id}`} value={user.id}>{`${user.firstName} ${user.lastName}`}</option>
                  )
                })}
                </Input>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col md="3">
                <Label htmlFor="text-input">Cantidad</Label>
              </Col>
              <Col xs="12" md="9">
                <Input
                  ref='name'
                  type="text"
                  id="fund"
                  name="number-input"
                  placeholder="Escriba el monto"
                  value={projectsfundingpage.fundingValue}
                  onChange={(evt) => {
                    this.props.onChangeProps({'fundingValue': evt.target.value})
                  }}
                />
              </Col>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.closeModalFunding.bind(this)}>Cancel</Button>
            {' '}
            <Button color="primary" onClick={this.props.onFundingCreating}>
              {projectsfundingpage.fundingType === '0'
              ? 'Fondear'
              : 'Pagar'}              
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

ProjectsfundingPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  projectsfundingpage: makeSelectProjectsfundingPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onProjectsfundingRequest: (id, page, word) => dispatch(projectsfundingRequest(id, page, word)),
    onUsersRequest: () => dispatch(usersRequest()),
    onChangeProp: (prop, value) => dispatch(changeProp(prop, value)),
    onFundingCreating: () => dispatch(fundingCreating()),
    onStatusRequest: (status) => dispatch(statusRequest(status)),
    onChangeProps: (object) => dispatch(changeProps(object))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'projectsfundingPage', reducer });
const withSaga = injectSaga({ key: 'projectsfundingPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ProjectsfundingPage);
