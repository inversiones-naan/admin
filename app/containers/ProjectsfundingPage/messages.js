/*
 * ProjectsfundingPage Messages
 *
 * This contains all the text for the ProjectsfundingPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ProjectsfundingPage.header',
    defaultMessage: 'This is ProjectsfundingPage container !',
  },
});
