/*
 * ProjectsusersPage Messages
 *
 * This contains all the text for the ProjectsusersPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ProjectsusersPage.header',
    defaultMessage: 'This is ProjectsusersPage container !',
  },
});
