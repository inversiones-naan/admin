
import { fromJS } from 'immutable';
import projectsusersPageReducer from '../reducer';

describe('projectsusersPageReducer', () => {
  it('returns the initial state', () => {
    expect(projectsusersPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
