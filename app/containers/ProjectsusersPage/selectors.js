import { createSelector } from 'reselect';

/**
 * Direct selector to the projectsusersPage state domain
 */
const selectProjectsusersPageDomain = (state) => state.get('projectsusersPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ProjectsusersPage
 */

const makeSelectProjectsusersPage = () => createSelector(
  selectProjectsusersPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectProjectsusersPage;
export {
  selectProjectsusersPageDomain,
};
