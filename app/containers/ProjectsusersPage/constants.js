/*
 *
 * ProjectsusersPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ProjectsusersPage/DEFAULT_ACTION';
export const PROJECTSUSERS_REQUEST = 'app/ProjectsusersPage/PROJECTSUSERS_REQUEST';
export const PROJECTSUSERS_SUCCESS = 'app/ProjectsusersPage/PROJECTSUSERS_SUCCESS';
export const PROJECTSUSERS_ERROR = 'app/ProjectsusersPage/PROJECTSUSERS_ERROR';
