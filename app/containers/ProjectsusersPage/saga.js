import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PROJECTSUSERS_REQUEST } from './constants';
import { projectsusersSuccess, projectsusersError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectProjectsusersPage from './selectors';
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PROJECTSUSERS_REQUEST, getProjectsUsers);
}
/**
 * Obtener los usuarios por proyectos de la API
 */
export function* getProjectsUsers() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(usersError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsusersPage());
  const {payload, word, id} = reducer
  
  const requestURL = `${params.apiUrl}users-projects/pages/?projectId=${id}&page=${payload.current}&word=${word}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(projectsusersError(payload.error.message));
    } else {
      yield put(projectsusersSuccess(result.payload));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectsusersError(err));
  }
}
