/*
 *
 * ProjectsusersPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  PROJECTSUSERS_REQUEST,
  PROJECTSUSERS_SUCCESS,
  PROJECTSUSERS_ERROR
} from './constants';

const paginator = {
  items: [],
  before: 1,
  next: 1,
  last: 1,
  current: 1,
  total_pages: 1
}
const initialState = fromJS({
  payload: JSON.parse(JSON.stringify(paginator)),
  loading: false,
  error: false,
  id: null,
  word: ''
});

function projectsusersPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PROJECTSUSERS_REQUEST:
      return state
      .set('payload', fromJS(paginator))
      .set('loading', true)
      .set('error', false)
      .set('word', action.word)
      .set('id', action.id)
    case PROJECTSUSERS_SUCCESS:
      return state
      .set('payload', fromJS(action.payload))
      .set('loading', false)
      .set('error', false)
    case PROJECTSUSERS_ERROR:
      return state
      .set('payload', fromJS(paginator))
      .set('loading', false)
      .set('error', false)
    default:
      return state;
  }
}

export default projectsusersPageReducer;
