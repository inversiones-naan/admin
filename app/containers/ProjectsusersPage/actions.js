/*
 *
 * ProjectsprojectsusersPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PROJECTSUSERS_REQUEST,
  PROJECTSUSERS_SUCCESS,
  PROJECTSUSERS_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}


/**
 * Solicita los usuarios al API
 *
 * @return {object}    An action object with a type of PROJECTSUSERS_REQUEST
 */
export function projectsusersRequest(id, page, word) {
  return {
    type: PROJECTSUSERS_REQUEST,
    id,
    page,
    word
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of PROJECTSUSERS_SUCCESS
 */
export function projectsusersSuccess(payload) {
  return {
    type: PROJECTSUSERS_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of PROJECTSUSERS_ERROR
 */
export function projectsusersError(error) {
  return {
    type: PROJECTSUSERS_ERROR,
    error,
  };
}
