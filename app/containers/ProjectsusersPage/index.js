/**
 *
 * ProjectsusersPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Link } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectProjectsusersPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { projectsusersRequest } from './actions';

import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  FormGroup,
  Label,
  Input
} from "reactstrap";

import Paginator from 'components/Paginator'

import LoadingIndicator from 'components/LoadingIndicator';

import TD from 'components/Td'

export class ProjectsusersPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    const { projectId } = this.props.match.params
    this.props.onUsersProjectsRequest(projectId, 1, '')
  }
  status (status) {
    if (status === "0") {
      return <Badge color="warning">Pending</Badge>
    } else if (status === "1") {
      return <Badge color="success">Active</Badge>
    } else if (status === "2") {
      return <Badge color="danger">Banned</Badge>
    } else if (status === "3") {
      return <Badge color="secondary">Inactive</Badge>
    }
  }
  handleSearch (evt) {
    const { projectsuserspage } = this.props
    const { projectId } = this.props.match.params
    this.props.onUsersProjectsRequest(projectId, projectsuserspage.payload.current, evt.target.value)
  }
  projectsusersRequest (page) {
    const { projectsuserspage } = this.props
    const { projectId } = this.props.match.params
    this.props.onUsersProjectsRequest(projectId, page, projectsuserspage.word)
  }
  render() {
    const { projectsuserspage } = this.props
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Proyectos
              </CardHeader>
              <CardBlock className="card-body">
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="name">Nombre de usuario, Nombres, Apellidos</Label>
                    <Input
                      type="text"
                      id="name"
                      placeholder="Escriba aqui la palabra"
                      value={projectsuserspage.word}
                      onChange={this.handleSearch.bind(this)}
                    />
                  </FormGroup>
                </Col>
                {projectsuserspage.loading
                ? <LoadingIndicator />
                : <Table hover bordered striped responsive size="sm">
                    <thead>
                    <tr>
                      <th>Username</th>
                      <th>Estado</th>
                      <th>Nombre</th>
                      <th>Apellidos</th>
                      <th>Inversión</th>
                    </tr>
                    </thead>
                    <tbody>
                    {projectsuserspage.payload.items.map( (user, index)=>{
                      return (
                        <tr key={`user-${user.id}`}>
                          <td>
                            <Link to={`/users/edit/${user.id}`}>
                              {' '}{user.username}
                            </Link>
                          </td>
                          <TD>{this.status(user.status)}</TD>
                          <TD>{user.firstName}</TD>
                          <TD>{user.lastName}</TD>
                          <TD>{user.funding}</TD>
                        </tr>
                      )
                    })}
                    </tbody>
                  </Table>
                }
                <nav>
                  {projectsuserspage.payload
                  ?<Paginator
                    before={parseInt(projectsuserspage.payload.before)}
                    next={parseInt(projectsuserspage.payload.next)}
                    last={parseInt(projectsuserspage.payload.last)}
                    current={parseInt(projectsuserspage.payload.current)}
                    total_pages={parseInt(projectsuserspage.payload.total_pages)}
                    range={5}
                    request={this.projectsusersRequest.bind(this)}
                  />
                  :null}                  
                </nav>
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

ProjectsusersPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  projectsuserspage: makeSelectProjectsusersPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onUsersProjectsRequest: (id, page, word) => dispatch(projectsusersRequest(id, page, word))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'projectsusersPage', reducer });
const withSaga = injectSaga({ key: 'projectsusersPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ProjectsusersPage);
