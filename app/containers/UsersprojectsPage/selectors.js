import { createSelector } from 'reselect';

/**
 * Direct selector to the usersprojectsPage state domain
 */
const selectUsersprojectsPageDomain = (state) => state.get('usersprojectsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by UsersprojectsPage
 */

const makeSelectUsersprojectsPage = () => createSelector(
  selectUsersprojectsPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectUsersprojectsPage;
export {
  selectUsersprojectsPageDomain,
};
