/*
 * UsersprojectsPage Messages
 *
 * This contains all the text for the UsersprojectsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.UsersprojectsPage.header',
    defaultMessage: 'This is UsersprojectsPage container !',
  },
});
