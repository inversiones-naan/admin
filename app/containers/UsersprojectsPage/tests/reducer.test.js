
import { fromJS } from 'immutable';
import usersprojectsPageReducer from '../reducer';

describe('usersprojectsPageReducer', () => {
  it('returns the initial state', () => {
    expect(usersprojectsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
