import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { LOAD_USER } from 'containers/App/constants';
import { loginSuccess, loginError } from 'containers/App/actions';
import base64 from 'base-64'

import request from 'utils/request';
import params from 'utils/params';

import { makeSelectUsernameEmail, makeSelectPassword } from './selectors';

/**
 * Root saga manages watcher lifecycle
 */
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js// Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_USER, loginUser);
}
/**
 * Autenticar el usuario
 */
export function* loginUser() {
  // Select username from store
  // const username = yield select(makeSelectUsername());
  // const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;
  // Select usernameEmail from store
  const usernameEmail = yield select(makeSelectUsernameEmail());
  const password = yield select(makeSelectPassword());
  const requestURL = `${params.apiUrl}users/authenticate`;
  const options = {
    method: 'POST',
    headers: {
      'Authorization': `Basic ${base64.encode(usernameEmail + ":" + password)}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result) 
    if (result.error) {
      yield put(loginError(result.error.message));
    } else {
      yield put(loginSuccess(result));
    }
    // const payload = {
    //   data: {
    //     token: "sdasdasdasdasdas",
    //     expires: 13156464,
    //     user: {
    //       id: 1,
    //       role: "User",
    //       email: "aaaa@aaa.com",
    //       username: "naaaaa",
    //       firstName: "Demo",
    //       lastName: "Demo",
    //       location: null,
    //       createAt: '2015',
    //       updateAt: null
    //     }
    //   }
    // }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición login'
    yield put(loginError(err));
  }
}