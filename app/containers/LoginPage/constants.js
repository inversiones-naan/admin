/*
 *
 * LoginPage constants
 *
 */

export const DEFAULT_ACTION = 'app/LoginPage/DEFAULT_ACTION';
export const CHANGE_USERNAMEEMAIL = 'app/LoginPage/CHANGE_USERNAMEEMAIL';
export const CHANGE_PASSWORD = 'app/LoginPage/CHANGE_PASSWORD';
export const CLEAR_DATA = 'app/LoginPage/CLEAR_DATA';
