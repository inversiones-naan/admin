/*
 *
 * LoginPage actions
 *
 */

import {
  DEFAULT_ACTION,
  CHANGE_USERNAMEEMAIL,
  CHANGE_PASSWORD,
  CLEAR_DATA
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {name} name The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAMEEMAIL
 */
export function changeUsernameEmail(name) {
  return {
    type: CHANGE_USERNAMEEMAIL,
    name,
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {name} name The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAMEEMAIL
 */
export function changePassword(password) {
  return {
    type: CHANGE_PASSWORD,
    password,
  };
}

/**
 * Limpia los datos del formulario
 *
 * @return {object}    An action object with a type of CLEAR_DATA
 */
export function clearData() {
  return {
    type: CLEAR_DATA
  };
}
