/*
 *
 * LoginPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  CHANGE_USERNAMEEMAIL,
  CHANGE_PASSWORD,
  CLEAR_DATA
} from './constants';

const initialState = fromJS({
  usernameEmail: '',
  password: ''
});

function loginPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case CHANGE_USERNAMEEMAIL:
      return state
        .set('usernameEmail', action.name);
    case CHANGE_PASSWORD: 
      return state
        .set('password', action.password);
    case CLEAR_DATA: 
      return state
        .set('password', '')
        .set('usernameEmail', '');
    default:
      return state;
  }
}

export default loginPageReducer;
