import { createSelector } from 'reselect';

/**
 * Direct selector to the loginPage state domain
 */
const selectLoginPageDomain = (state) => state.get('loginPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by LoginPage
 */

const makeSelectLoginPage = () => createSelector(
  selectLoginPageDomain,
  (substate) => substate.toJS()
);

const makeSelectUsernameEmail = () => createSelector(
  selectLoginPageDomain,
  (loginPageState) => loginPageState.get('usernameEmail')
);

const makeSelectPassword = () => createSelector(
  selectLoginPageDomain,
  (loginPageState) => loginPageState.get('password')
);

// export default makeSelectLoginPage;
export {
  makeSelectLoginPage,
  selectLoginPageDomain,
  makeSelectUsernameEmail,
  makeSelectPassword,
};
