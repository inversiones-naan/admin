/**
 *
 * LoginPage
 *
 */

import React from 'react';
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { login } from '../App/actions';

import injectSaga from 'utils/injectSaga';
import { makeSelectUser, makeSelectLoading, makeSelectError } from 'containers/App/selectors';
import injectReducer from 'utils/injectReducer';
import { makeSelectLoginPage, makeSelectUsernameEmail, makeSelectPassword } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { changeUsernameEmail, changePassword, clearData } from './actions';

import {Container, Row, Col, CardGroup, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon} from "reactstrap";

import LoadingIndicator from 'components/LoadingIndicator';

import { Link } from 'react-router-dom';


// <FormattedMessage {...messages.header} />

export class LoginPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentWillReceiveProps (nextProps) {
    let { history } = this.props
    this.goDashboard(nextProps.user, history)
  }
  componentDidMount () {
    let { history, user } = this.props
    this.goDashboard (user, history)
  }
  goDashboard (user, history) {
    if (user) {
      history.push('/')
      this.props.onClear()
    }
  }
  submit(evt) {
    let keyCode = evt.keyCode || evt.which
    if (evt.shiftKey === false && keyCode === 13) {
      this.props.onLogin()
    }
  }
  render() {
    return (
      <div className="app flex-row align-items-center site-header-background">
        <Container style={{marginTop: 100, maxWidth: 1800, paddingLeft: 40, paddingRight: 40}}>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  {this.props.loading
                  ?
                    <LoadingIndicator />
                  : <CardBlock className="card-body">
                      <h1>Iniciar Sesión</h1>
                      <p className="text-muted">Iniciar sesión con su cuenta</p>
                      {this.props.error
                      ? <Card className="text-white bg-danger">
                          <CardBlock className="card-body">
                            {this.props.error}
                          </CardBlock>
                        </Card>                  
                      : null}
                      <InputGroup className="mb-3">
                        <InputGroupAddon><i className="icon-envelope"></i></InputGroupAddon>
                        <Input
                          ref="username"
                          type="text"
                          placeholder="Ingresa tu correo"
                          value={this.props.usernameEmail}
                          onChange={this.props.onChangeUsernameEmail}
                          onKeyDown={(evt) => {
                            let keyCode = evt.keyCode || evt.which
                            if (evt.shiftKey === false && keyCode === 13) {
                              evt.preventDefault()
                              let el = ReactDOM.findDOMNode(this.refs.password)
                              el.focus()
                            }
                          }}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                        <Input
                          ref="password"
                          type="password"
                          placeholder="Contraseña"
                          value={this.props.password}
                          onChange={this.props.onChangePassword}
                          onKeyDown={this.submit.bind(this)}
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="12">
                          <Button
                            color="success"
                            block
                            onClick={this.props.onLogin}
                            disabled={this.props.loading}
                          >
                            Iniciar
                          </Button>
                        </Col>
                        <div className="col-sm-12 form-group">
                          <br/>
                          <Link to={`/registro`}>Registrarse</Link>
                          <br/>
                          <Link to={`/new-password`}>¿Olvidaste tu contraseña?</Link>
                          <br/>
                          <Link to={`/new-activate`}>¿No recibiste instrucciones para confirmar tu cuenta?</Link>
                          <br/>
                        </div>
                        {/* <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col> */}
                      </Row>
                    </CardBlock>
                  }
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

LoginPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loginpage: makeSelectLoginPage(),
  user: makeSelectUser(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  usernameEmail: makeSelectUsernameEmail(),
  password: makeSelectPassword()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onLogin: (usernameEmail, password) => dispatch(login(usernameEmail, password)),
    onChangeUsernameEmail: (evt) => dispatch(changeUsernameEmail(evt.target.value)),
    onChangePassword: (evt) => dispatch(changePassword(evt.target.value)),
    onClear: () => dispatch(clearData()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'loginPage', reducer });
const withSaga = injectSaga({ key: 'loginPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LoginPage);
