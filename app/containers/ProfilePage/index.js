/**
 *
 * ProfilePage
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";

import { userChangeProp, userClear, userRequest, userUpdating } from './actions';

import {
  makeSelectProfilePage,
} from './selectors';

import { makeSelectUserId } from '../App/selectors';

import LoadingIndicator from 'components/LoadingIndicator';

import 'react-widgets/dist/css/react-widgets.css'
import { Calendar } from 'react-widgets'
import moment from 'moment'

export class ProfilePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }
  componentDidMount () {
    const { userId } = this.props.match.params
    if (userId) {
      this.props.onUserRequest(userId)
    } else {
      this.props.onUserRequest(this.props.myId)
    }
  }
  render() {
    const {profilepage} = this.props
    if (!profilepage) {return <div />}
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <strong>Perfíl</strong>
              </CardHeader>
              <CardBlock className="card-body">
                {profilepage.loading
                ?<LoadingIndicator />
                :<Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  {profilepage.success
                  ? <Card className="text-white bg-success">
                      <CardBlock className="card-body">
                        Se ha actualizado el usuario
                      </CardBlock>
                    </Card>                  
                  : null}
                  {profilepage.error
                  ? <Card className="text-white bg-danger">
                      <CardBlock className="card-body">
                        {profilepage.error}
                      </CardBlock>
                    </Card>                  
                  : null}
                  <FormGroup row>
                    <Col md="3">
                      <Label>Nombres</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <p className="form-control-static">{profilepage.firstName}{' '}{profilepage.lastName}</p>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Correo (requerido)</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="email"
                        type="email"
                        id="email-input"
                        name="email-input"
                        placeholder="Escriba su correo"
                        value={profilepage.username||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('username', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.role)
                            el.focus()
                          }
                        }}
                      />
                      <FormText className="help-block">Por favor escriba su correo electrónico</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="select">Rol</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="role"
                        type="select"
                        name="select"
                        id="select"
                        value={profilepage.role}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('role', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.password)
                            el.focus()
                          }
                        }}
                      >
                        <option value="Unauthorized">No Autorizado</option>
                        <option value="Authorized">Autorizado</option>
                        <option value="User">Usuario</option>
                        <option value="Manager">Gestor</option>
                        <option value="Administrator">Administrador</option>
                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="password-input">Password</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="password"
                        type="password"
                        id="password-input"
                        name="password-input"
                        placeholder="Escriba la contraseña"
                        value={profilepage.password||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('password', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.rePassword)
                            el.focus()
                          }
                        }}
                      />
                      <FormText className="help-block">Por favor ingrese una contraseña compleja</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="password-input">Repetir contraseña</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="rePassword"
                        type="password"
                        id="rePassword-input"
                        name="rePassword-input"
                        placeholder="Repetir la contraseña"
                        value={profilepage.rePassword||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('rePassword', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.firstName)
                            el.focus()
                          }
                        }}
                      />
                      <FormText className="help-block">Por favor ingrese una contraseña compleja</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Nombre</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="firstName"
                        type="text"
                        id="email-input"
                        name="email-input"
                        placeholder="Escriba el Nombre"
                        value={profilepage.firstName||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('firstName', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.lastName)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Apellidos</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="lastName"
                        type="text"
                        id="email-input"
                        name="email-input"
                        placeholder="Escriba los Apellidos"
                        value={profilepage.lastName||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('lastName', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.rfc)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">RFC</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="rfc"
                        type="text"
                        id="rfc-input"
                        name="rfc-input"
                        placeholder="Escriba el RFC"
                        value={profilepage.rfc||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('rfc', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.street)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Calle</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="street"
                        type="text"
                        id="street-input"
                        name="street-input"
                        placeholder="Escriba la calle"
                        value={profilepage.street||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('street', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.ext)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Exterior</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="ext"
                        type="text"
                        id="ext-input"
                        name="ext-input"
                        placeholder="Escriba el Exterior"
                        value={profilepage.ext||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('ext', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.int)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Interior</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="int"
                        type="text"
                        id="int-input"
                        name="int-input"
                        placeholder="Escriba el Interior"
                        value={profilepage.int||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('int', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.colony)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Colonia</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="colony"
                        type="text"
                        id="colony-input"
                        name="colony-input"
                        placeholder="Escriba la Conlonia"
                        value={profilepage.colony||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('colony', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.city)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Ciudad</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="city"
                        type="text"
                        id="city-input"
                        name="city-input"
                        placeholder="Escriba la Ciudad"
                        value={profilepage.city||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('city', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.cp)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Codigo Postal</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="cp"
                        type="text"
                        id="cp-input"
                        name="cp-input"
                        placeholder="Escriba el Código Postal"
                        value={profilepage.cp||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('cp', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.state)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Estado</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="state"
                        type="text"
                        id="state-input"
                        name="state-input"
                        placeholder="Escriba el Estado"
                        value={profilepage.state||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('state', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.country)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">País</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="country"
                        type="text"
                        id="country-input"
                        name="country-input"
                        placeholder="Escriba el Pais"
                        value={profilepage.country||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('country', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.interbankClabe)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Clabe Interbancaria</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="interbankClabe"
                        type="text"
                        id="interbankClabe-input"
                        name="interbankClabe-input"
                        placeholder="Escriba la Clabe Interbancaria"
                        value={profilepage.interbankClabe||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('interbankClabe', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.birthdate)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Fecha de nacimiento</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input
                        ref="birthdate"
                        type="text"
                        id="birthdate-input"
                        name="birthdate-input"
                        placeholder="Escriba la Fecha de Nacimiento"
                        value={profilepage.birthdate||''}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('birthdate', evt.target.value)
                        }}
                        onFocus={() => this.setState({show:true})}
                      />
                    </Col>
                  </FormGroup>
                </Form>}
              </CardBlock>
              <CardFooter>
                <Button
                  type="submit"
                  size="sm"
                  color="primary"
                  onClick={this.props.onUserUpdating}
                  disabled={profilepage.loading}
                >
                  <i className="fa fa-dot-circle-o"></i> Editar
                </Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
        <Modal
          isOpen={this.state.show}
          toggle={()=> this.setState({show:false})}
          className={'modal-sm'}
        >
          <ModalHeader
            toggle={()=> this.setState({show:false})}
          >
            Seleccione la fecha
          </ModalHeader>
          <ModalBody>
            <Calendar
              onChange={value => {
                this.setState({show:false})
                this.props.onUserChangeProp('birthdate', moment(value).format('YYYY-MM-DD'))
              }}
            />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

ProfilePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  profilepage: makeSelectProfilePage(),
  myId: makeSelectUserId()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onUserChangeProp: (prop, value) => dispatch(userChangeProp(prop, value)),
    onUserRequest: (userId) => dispatch(userRequest(userId)),
    onUserUpdating: () => dispatch(userUpdating())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'profilePage', reducer });
const withSaga = injectSaga({ key: 'profilePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ProfilePage);
