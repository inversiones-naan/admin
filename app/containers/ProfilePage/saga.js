import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { USER_REQUEST, USER_UPDATING } from './constants';
import { userSuccess, userError, userSuccessUpdated } from './actions';

import request from 'utils/request';
import params from 'utils/params';

import { makeSelectProfilePage } from './selectors';
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(USER_REQUEST, getUser);
  yield takeLatest(USER_UPDATING, updateUser)
}

export function* getUser() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(userError('Error token'));
    return
  }
  const profilepage = yield select(makeSelectProfilePage());
  const requestURL = `${params.apiUrl}users/${profilepage.userId}`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(userError(payload.error.message));
    } else {
      yield put(userSuccess(payload.user));
    }    
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error al obtener la información del usuario'
    yield put(userError(err));
  }
}

export function* updateUser() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(userError('Error token'));
    return
  }
  const profilepage = yield select(makeSelectProfilePage());
  if (profilepage.password.length > 0 && profilepage.password.length < 8) {
    yield put(userError('La contraseña mínimo debe tener 8 caracteres'));
    return
  } else if (profilepage.password.length > 0) {
    payload.passwordRenew = profilepage.password // EN el caso que la contraseña tenga algun valor, se añade al request
  }
  if (profilepage.password !== profilepage.password) {
    yield put(userError('Las contraseñas no coinciden'));
    return
  }
  const payload = {
    username: profilepage.username,
    role: profilepage.role,
    firstName: profilepage.firstName,
    lastName: profilepage.lastName,
    role: profilepage.role,
    password: profilepage.password,
    firstName: profilepage.firstName,
    lastName: profilepage.lastName,
    rfc: profilepage.rfc,
    street: profilepage.street,
    ext: profilepage.ext,
    int: profilepage.int,
    colony: profilepage.colony,
    city: profilepage.city,
    rfc: profilepage.rfc,
    cp: profilepage.cp,
    state: profilepage.state,
    country: profilepage.country,
    interbankClabe: profilepage.interbankClabe,
    birthdate: profilepage.birthdate
  }
  // console.log(payload)
  
  const requestURL = `${params.apiUrl}users/${profilepage.userId}`;
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`
    },
    body: JSON.stringify(payload)
  }
  // console.log(options)
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(userError(result.error.message));
    } else {
      yield put(userSuccessUpdated());
    }    
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error al actualizar el usuario'
    yield put(userError(err));
  }
}