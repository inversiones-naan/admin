/*
 *
 * ProfilePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  USER_REQUEST,
  USER_SUCCESS,
  USER_ERROR,
  USER_CHANGE_PROP,
  USER_UPDATING,
  USER_SUCCESS_UPDATED
} from './constants';

const initialState = fromJS({
  loading: true, 
  success: false,
  error: false,
  userId: null,
  username: '',
  role: 'User',
  firstName: '',
  lastName: '',
  role: '',
  password: '',
  rePassword: '',
  firstName: '',
  lastName: '',
  rfc: '',
  street: '',
  ext: '',
  int: '',
  colony: '',
  city: '',
  rfc: '',
  cp: '',
  state: '',
  country: '',
  interbankClabe: '',
  birthdate: '',
});

function profilePageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case USER_REQUEST:
      return state
        .set('loading', true)
        .set('success', false)
        .set('error', false)
        .set('userId', action.userId)
    case USER_SUCCESS:
      const {payload} = action
      return state
        .set('loading', false)
        .set('error', false)
        .set('username', payload.username)
        .set('role', payload.role)
        .set('password', '')
        .set('rePassword', '')
        .set('firstName', payload.firstName)
        .set('lastName', payload.lastName)
        .set('rfc', payload.rfc)
        .set('street', payload.street)
        .set('ext', payload.ext)
        .set('int', payload.int)
        .set('colony', payload.colony)
        .set('city', payload.city)
        .set('rfc', payload.rfc)
        .set('cp', payload.cp)
        .set('state', payload.state)
        .set('country', payload.country)
        .set('interbankClabe', payload.interbankClabe)
        .set('birthdate', payload.birthdate)
    case USER_ERROR:
      return state
        .set('loading', false)
        .set('success', false)
        .set('error', action.error)
    case USER_CHANGE_PROP:
      return state
        .set(action.prop, action.value)
    case USER_UPDATING:
      return state
        .set('loading', true)
        .set('success', false)
        .set('error', false)
    case USER_SUCCESS_UPDATED:
      return state
        .set('loading', false)
        .set('success', true)
        .set('error', false)
    default:
      return state;
  }
}

export default profilePageReducer;
