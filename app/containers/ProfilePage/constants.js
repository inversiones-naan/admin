/*
 *
 * ProfilePage constants
 *
 */

export const DEFAULT_ACTION = 'app/ProfilePage/DEFAULT_ACTION';
export const USER_REQUEST = 'app/ProfilePage/USER_REQUEST';
export const USER_SUCCESS = 'app/ProfilePage/USER_SUCCESS';
export const USER_ERROR = 'app/ProfilePage/USER_ERROR';
export const USER_UPDATING = 'app/ProfilePage/USER_UPDATING';
export const USER_SUCCESS_UPDATED = 'app/ProfilePage/USER_SUCCESS_UPDATED';
export const USER_CLEAR = 'app/ProfilePage/USER_CLEAR';
export const USER_CHANGE_PROP = 'app/ProfilePage/USER_CHANGE_PROP';
