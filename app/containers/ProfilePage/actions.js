/*
 *
 * ProfilePage actions
 *
 */

import {
  DEFAULT_ACTION,
  USER_REQUEST,
  USER_SUCCESS,
  USER_ERROR,
  USER_UPDATING,
  USER_SUCCESS_UPDATED,
  USER_CLEAR,
  USER_CHANGE_PROP
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/**
 * Solicita el usuario al API
 *
 * @return {object}    An action object with a type of USER_REQUEST
 */
export function userRequest(userId) {
  return {
    type: USER_REQUEST,
    userId
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of USER_SUCCESS
 */
export function userSuccess(payload) {
  return {
    type: USER_SUCCESS,
    payload
  };
}

/**
 * Respuesta con error del API
 *
 * @return {object}    An action object with a type of USER_ERROR
 */
export function userError(error) {
  return {
    type: USER_ERROR,
    error
  };
}

/**
 * Se esta actualisando el usuario en el API
 *
 * @return {object}    An action object with a type of USER_UPDATING
 */
export function userUpdating() {
  return {
    type: USER_UPDATING,
  };
}

/**
 * Se esta actualisando el usuario en el API
 *
 * @return {object}    An action object with a type of USER_SUCCESS_UPDATED
 */
export function userSuccessUpdated() {
  return {
    type: USER_SUCCESS_UPDATED,
  };
}

/**
 * Limpiar el formulario
 *
 * @return {object}    An action object with a type of USER_SUCCESS_UPDATED
 */
export function userClear() {
  return {
    type: USER_CLEAR,
  };
}

/**
 * Cambiar los parametros del usuario
 *
 * @return {object}    An action object with a type of USER_CHANGE_PROP
 */
export function userChangeProp(prop, value) {
  return {
    type: USER_CHANGE_PROP,
    prop,
    value,
  };
}