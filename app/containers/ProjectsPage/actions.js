/*
 *
 * ProjectsPage actions
 *
 */

import {
  DEFAULT_ACTION,
  PROJECTS_REQUEST,
  PROJECTS_SUCCESS,
  PROJECTS_ERROR,
  STATUS_REQUEST,
  STATUS_SUCCESS,
  STATUS_ERROR,
  SEE_PROJECT,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/**
 * Solicita los usuarios al API
 *
 * @return {object}    An action object with a type of PROJECTS_REQUEST
 */
export function projectsRequest() {
  return {
    type: PROJECTS_REQUEST
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of PROJECTS_SUCCESS
 */
export function projectsSuccess(payload) {
  return {
    type: PROJECTS_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of PROJECTS_ERROR
 */
export function projectsError(error) {
  return {
    type: PROJECTS_ERROR,
    error,
  };
}

/**
 * Solicita activar o desactivar el proyecto API
 *
 * @return {object} An action object with a type of PROJECTS_REQUEST
 */
export function statusRequest(activate) {
  return {
    type: STATUS_REQUEST,
    activate,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of PROJECTS_SUCCESS
 */
export function statusSuccess(project) {
  return {
    type: STATUS_SUCCESS,
    project,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of PROJECTS_ERROR
 */
export function statusError(error) {
  return {
    type: STATUS_ERROR,
    error,
  };
}

/**
 * Agrega un proyecto
 *
 * @return {object} An action object with a type of SEE_PROJECT
 */
export function seeProject(project) {
  return {
    type: SEE_PROJECT,
    project
  };
}
