/*
 *
 * ProjectsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ProjectsPage/DEFAULT_ACTION';
export const PROJECTS_REQUEST = 'app/ProjectsPage/PROJECTS_REQUEST';
export const PROJECTS_SUCCESS = 'app/ProjectsPage/PROJECTS_SUCCESS';
export const PROJECTS_ERROR = 'app/ProjectsPage/PROJECTS_ERROR';

export const SEE_PROJECT = 'app/ProjectsPage/SEE_PROJECT';
export const STATUS_REQUEST = 'app/ProjectsPage/STATUS_REQUEST';
export const STATUS_SUCCESS = 'app/ProjectsPage/STATUS_SUCCESS';
export const STATUS_ERROR = 'app/ProjectsPage/STATUS_ERROR';
