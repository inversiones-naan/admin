/*
 * ProjectsPage Messages
 *
 * This contains all the text for the ProjectsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ProjectsPage.header',
    defaultMessage: 'This is ProjectsPage container !',
  },
});
