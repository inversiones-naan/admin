/*
 *
 * ProjectsPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  PROJECTS_REQUEST,
  PROJECTS_SUCCESS,
  PROJECTS_ERROR,
  STATUS_REQUEST,
  STATUS_SUCCESS,
  STATUS_ERROR,
  SEE_PROJECT,
} from './constants';
import { PROJECT_ERROR } from '../ProjectNewPage/constants';

const paginator = {
  items: [],
  before: 1,
  next: 1,
  last: 1,
  current: 1,
  total_pages: 1
}

const initialState = fromJS({
  payload: JSON.parse(JSON.stringify(paginator)),
  loading: false,
  error: false,
  word: '',
  activate: null,
  project: {},
  activateLoading: null,
  activateSuccess: false,
  activateError: false,
  projects: []
});

function projectsPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case PROJECTS_REQUEST:
      return state
      .set('projects', fromJS([]))
      .set('loading', true)
      .set('error', false)
      .set('word', action.word)
    case PROJECTS_SUCCESS:
      return state
      .set('projects', fromJS(action.payload))
      .set('loading', false)
      .set('error', false)
    case PROJECTS_ERROR:
      return state
      .set('projects', fromJS([]))
      .set('loading', false)
      .set('error', false)
    case STATUS_REQUEST:
      return state
      .set('activate', action.activate)
      .set('activateLoading', true)
      .set('activateSuccess', false)
      .set('activateError', false)
    case STATUS_SUCCESS:
      // Buscar proyecto
      let projects = state.toJS().projects
      let project = action.payload
      for (let i = 0; i < projects.length; i++) {
        let element = projects[i];
        if (parseInt(element.id) === parseInt(action.project.id)) {
          element.status = action.project.status
          project = JSON.parse(JSON.stringify(element))
        }
      }
      return state
      .set('projects', fromJS(projects))
      .set('project', fromJS(project))
      .set('activate', null)
      .set('activateLoading', false)
      .set('activateSuccess', 'Cambio realizado')
      .set('activateError', false)
    case STATUS_ERROR:
      return state
      .set('activate', null)
      .set('activateLoading', false)
      .set('activateSuccess', false)
      .set('activateError', action.error)
    case SEE_PROJECT:
      return state
      .set('project', fromJS(action.project))
      .set('activate', null)
      .set('activateLoading', false)
      .set('activateSuccess', false)
      .set('activateError', false)
    default:
      return state;
  }
}

export default projectsPageReducer;
