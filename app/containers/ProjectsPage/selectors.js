import { createSelector } from 'reselect';

/**
 * Direct selector to the projectsPage state domain
 */
const selectProjectsPageDomain = (state) => state.get('projectsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ProjectsPage
 */

const makeSelectProjectsPage = () => createSelector(
  selectProjectsPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectProjectsPage;
export {
  selectProjectsPageDomain,
};
