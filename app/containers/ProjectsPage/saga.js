import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { PROJECTS_REQUEST, STATUS_REQUEST } from './constants';
import { projectsSuccess, projectsError, statusSuccess, statusError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectProjectsPage from './selectors';
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(PROJECTS_REQUEST, getProjects);
  yield takeLatest(STATUS_REQUEST, statusProject);
}
/**
 * Obtener los proyectos de la API
 */
export function* getProjects() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(usersError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsPage());
  const {payload, word} = reducer
  
  const requestURL = `${params.apiUrl}projects`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(projectsError(result.error.message));
    } else {
      yield put(projectsSuccess(result.projects));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(projectsError(err));
  }
}
/**
 * Activar o desactivar el proyecto
 */
export function* statusProject() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(usersError('Error token'));
    return
  }
  const reducer = yield select(makeSelectProjectsPage());
  const {activate, project} = reducer

  const payload = {
    status: activate,
  }
  const requestURL = `${params.apiUrl}projects/${project.id}`;
  const options = {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${authToken}`
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    // console.log(result)
    if (result.error) {
      yield put(statusError(result.error.message));
    } else {
      yield put(statusSuccess(result.project));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(statusError(err));
  }
}