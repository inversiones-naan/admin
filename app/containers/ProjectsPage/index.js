/**
 *
 * ProjectsPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Link } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectProjectsPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { projectsRequest, statusRequest, seeProject } from './actions';

import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  FormGroup,
  Label,
  Input,
  Button,
  ModalHeader,
  Modal,
  ModalBody,
  ModalFooter,
} from "reactstrap";

import Paginator from 'components/Paginator'

import LoadingIndicator from 'components/LoadingIndicator';

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

export class ProjectsPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    this.props.onProjectsRequest()
  }
  projectsRequest () {
    this.props.onProjectsRequest()
  }
  handleSearch (evt) {
    const { projectspage } = this.props
    this.props.onProjectsRequest(projectspage.payload.current, evt.target.value)
  }
  toggleModal (project) {
    this.setState({
      project: !project?{}:project
    });
  }
  render() {
    const { projectspage } = this.props
    const self = this
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Proyectos
              </CardHeader>
              <CardBlock className="card-body">
                {projectspage.loading
                ? <LoadingIndicator />
                : <ReactTable
                  data={projectspage.projects}
                  defaultPageSize={5}
                  filterable
                  defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value}
                  columns={[{
                    Header: 'Nombre',
                    accessor: 'name',
                    style: { textAlign: 'center' },
                    filterMethod: (filter, row) => {
                      return row[filter.id].indexOf(filter.value) > -1
                    },
                    Cell: row => (
                      <div>
                        <Link to={`/admin/projects/edit/${row.original.id}`}>
                          {row.value}
                        </Link>
                      </div>
                    )
                  },{
                    Header: 'Monto',
                    accessor: 'funding',
                    style: { textAlign: 'center' },
                    filterMethod: (filter, row) => {
                      return row[filter.id].indexOf(filter.value) > -1
                    },
                    Cell: row => (
                      <div>
                        {row.value}
                      </div>
                    )
                  },{
                    Header: 'Progreso',
                    accessor: 'progressFunding',
                    filterMethod: (filter, row) => {
                      if (filter.value === "all") {
                        return true;
                      }
                      if( row[filter.id] < filter.value) {
                        return true
                      }
                      return false
                    },
                    Filter: ({ filter, onChange }) =>
                      <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}
                      >
                        <option value="25.00">Hasta 25%</option>
                        <option value="50.00">Hasta 50%</option>
                        <option value="75.00">Hasta 75%</option>
                        <option value="all">Todo</option>
                      </select>
                    ,
                    Cell: row => (
                      <div
                        style={{
                          width: '100%',
                          height: '100%',
                          backgroundColor: '#dadada',
                          borderRadius: '2px'
                        }}
                      >
                        <div
                          style={{
                            width: `${row.value}%`,
                            height: '100%',
                            backgroundColor: row.value > 66 ? '#85cc00'
                              : row.value > 33 ? '#ffbf00'
                              : '#ff2e00',
                            borderRadius: '2px',
                            transition: 'all .2s ease-out'
                          }}
                        >{' '}{`${row.value}%`}</div>
                      </div>
                    )
                  },{
                    Header: 'Estado',
                    accessor: 'status',
                    filterMethod: (filter, row) => {
                      if (filter.value === "all") {
                        return true;
                      }
                      return row[filter.id] === filter.value
                    },
                    Filter: ({ filter, onChange }) =>
                      <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}
                      >
                        <option value="all">Todo</option>
                        <option value="0">No activo</option>
                        <option value="1">En fondeo</option>
                        <option value="2">Fondeo completo</option>
                        <option value="3">Generando</option>
                        <option value="4">Finalizado</option>
                      </select>
                    ,
                    Cell: row => (
                      <span onClick={(self.props.onSeeProject.bind(self, row.original))}>
                        <span style={{
                          color: row.value === '0' ? '#ff2e00'
                            : row.value === '1' ? '#66A4D6'
                            : row.value === '2' ? '#98C46F'
                            : row.value === '3' ? '#FB9230'
                            : row.value === '4' ? '#FFD200'
                            : '#000000',
                          transition: 'all .3s ease'
                        }}>
                          &#x25cf;
                        </span> {
                          row.value === '0' ? 'No activo'
                          : row.value === '1' ? `En fondeo`
                          : row.value === '2' ? `Fondeo completo`
                          : row.value === '3' ? `Generando`
                          : row.value === '4' ? `Finalizado`
                          : 'Indefinido'
                        }
                      </span>
                    )
                  },{
                    Header: 'Transacciones',
                    accessor: 'countTransaction',
                    Cell: row => (
                      <div style={{textAlign: 'center'}}>
                        <Link to={`/admin/projects/funding/${row.original.id}`}>
                          {row.value}
                        </Link>
                      </div>
                    )
                  },{
                    Header: 'Imagenes',
                    accessor: 'countImages',
                    Cell: row => (
                      <div style={{textAlign: 'center'}}>
                        <Link to={`/admin/projects/images/${row.original.id}`}>
                          {row.value}
                        </Link>
                      </div>
                    )
                  },{
                    Header: 'Mensajes',
                    accessor: 'countNews',
                    Cell: row => (
                      <div style={{textAlign: 'center'}}>
                        <Link to={`/admin/projects/news/${row.original.id}`}>
                          {row.value}
                        </Link>
                      </div>
                    )
                  }]}
                  className="-striped -highlight"
                  previousText='Anterior'
                  nextText='Siguiente'
                  loadingText='Cargando...'
                  noDataText='No hay datos'
                  pageText='Página'
                  ofText='de'
                  rowsText='filas'
                />
                }
              </CardBlock>
            </Card>
          </Col>
        </Row>
        <Modal isOpen={projectspage.project.id?true:false} toggle={this.props.onSeeProject.bind(this, {})}
                className={'modal-sm'}>
          <ModalHeader toggle={this.props.onSeeProject.bind(this, {})}>Cambiar Estado del Proyecto</ModalHeader>
          <ModalBody>
            {projectspage.activateSuccess
            ? <Card className="text-white bg-success">
                <CardBlock className="card-body">
                  {projectspage.activateSuccess}
                </CardBlock>
              </Card>                  
            : null}
            {projectspage.activateError
            ? <Card className="text-white bg-danger">
                <CardBlock className="card-body">
                  {projectspage.activateError}
                </CardBlock>
              </Card>                  
            : null}
            {projectspage.activateLoading
            ? <LoadingIndicator />
            : projectspage.project.status === '0'
            ?'Es necesario activar el proyecto para que sea visible al publico y tambien pueda ser fondeado.'
            :'Al desactivar el proyecto ya no será visible al publico y no se podrá fondear'}
          </ModalBody>
          <ModalFooter>
            {projectspage.project.status === '0'
            ? <Button color="primary" onClick={this.props.onStatusRequest.bind(this, 1)}>Activar</Button>
            : <Button color="primary" onClick={this.props.onStatusRequest.bind(this, 0)}>Desactivar</Button> }
            {' '}
            <Button color="secondary" onClick={this.props.onSeeProject.bind(this, {})}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

ProjectsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  projectspage: makeSelectProjectsPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onProjectsRequest: () => dispatch(projectsRequest()),
    onStatusRequest: (projectId, activate) => dispatch(statusRequest(projectId, activate)),
    onSeeProject: (project) => dispatch(seeProject(project)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'projectsPage', reducer });
const withSaga = injectSaga({ key: 'projectsPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ProjectsPage);
