/**
 *
 * ActivatePasswordPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectActivatePasswordPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Container,
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  CardGroup,
  InputGroup,
  InputGroupAddon,
  Input,
} from "reactstrap";

import { validateRequest, validateError, clear, changeProp, passwordRequest } from './actions';

import LoadingIndicator from 'components/LoadingIndicator';

import { makeSelectUser } from 'containers/App/selectors';

export class ActivatePasswordPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount () {
    let { location, user } = this.props
    if (user) {
      this.props.onClear()
      return
    }
    const params = new URLSearchParams(location.search); 
    const code = params.get('code');
    if (code === null) {
      this.props.onValidateError('No existe el código para cambiar contraseña')
    } else {
      this.props.onValidateRequest(code)
    }
  }
  render() {
    const {activatepasswordpage, user} = this.props
    return (
      <div className="app flex-row site-header-background">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              {user
              ? <CardGroup className="mb-0">
                <Card className="p-4">
                  <CardBlock className="card-body">
                    <h1>Cambia tu contraseña</h1>
                    <p className="text-muted">Puedes cambiar la contraseña</p>
                    {activatepasswordpage.loading
                    ? <h2>Actualizando...</h2>
                    : activatepasswordpage.error
                    ? <Card className="text-white bg-danger mx-4">
                      <CardBlock className="card-body p-4">
                        <h1 className='text-center'>{activatepasswordpage.error}</h1>
                      </CardBlock>
                    </Card>
                    :activatepasswordpage.success
                    ? <Card className="mx-4">
                      <CardBlock className="card-body p-4">
                        <h1 className='text-center'>{activatepasswordpage.success}</h1>
                      </CardBlock>
                    </Card>
                    :null}
                    <InputGroup className="mb-3">
                      <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                      <Input
                        ref="password"
                        type="password"
                        placeholder="Ingresa nueva contraseña"
                        value={activatepasswordpage.password}
                        onChange={(evt) => {
                          this.props.onChangeProp('password', evt.target.value)
                        }}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                      <Input
                        ref="rePassword"
                        type="password"
                        placeholder="Escirbe de nuevo la nueva contraseña"
                        value={activatepasswordpage.rePassword}
                        onChange={(evt) => {
                          this.props.onChangeProp('rePassword', evt.target.value)
                        }}
                      />
                    </InputGroup>
                    <Row>
                        <Col xs="6">
                          <Button
                            color="primary"
                            className="px-4"
                            disabled={activatepasswordpage.loading}
                            onClick={this.props.onPasswordRequest}
                          >
                            Cambiar contraseña
                          </Button>
                        </Col>
                      </Row>
                  </CardBlock>
                </Card>
              </CardGroup>
              : activatepasswordpage.loading
              ? <Card className="mx-4">
                <CardBlock className="card-body p-4">
                  <h1 className='text-center'>{'Validando cuenta'}</h1>
                  <LoadingIndicator />
                </CardBlock>
              </Card>
              :activatepasswordpage.error
              ? <Card className="text-white bg-danger mx-4">
                <CardBlock className="card-body p-4">
                  <h1 className='text-center'>{activatepasswordpage.error}</h1>
                </CardBlock>
              </Card>
              :activatepasswordpage.success
              ? <Card className="mx-4">
                <CardBlock className="card-body p-4">
                  <h1 className='text-center'>{activatepasswordpage.success}</h1>
                </CardBlock>
              </Card>
              :<Card className="mx-4">
                <CardBlock className="card-body p-4">
                  <h1 className='text-center'>Validar codigo de cambio de contraseña</h1>
                </CardBlock>
              </Card>
              }
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

ActivatePasswordPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  activatepasswordpage: makeSelectActivatePasswordPage(),
  user: makeSelectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onValidateRequest: (code) => dispatch(validateRequest(code)),
    onValidateError: (error) => dispatch(validateError(error)),
    onClear: () => dispatch(clear()),
    onChangeProp: (prop, value) => dispatch(changeProp(prop, value)),
    onPasswordRequest: () => dispatch(passwordRequest()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'activatePasswordPage', reducer });
const withSaga = injectSaga({ key: 'activatePasswordPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ActivatePasswordPage);
