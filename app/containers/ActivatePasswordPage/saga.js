import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { VALIDATE_REQUEST, PASSWORD_REQUEST } from './constants';
import { validateSuccess, validateError, passwordSuccess, passwordError } from './actions';
import { loginSuccess } from 'containers/App/actions';

import request from 'utils/request';
import params from 'utils/params';
import makeSelectActivatePage from './selectors'
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(VALIDATE_REQUEST, activatePassword);
  yield takeLatest(PASSWORD_REQUEST, resetPassword);
}

export function* activatePassword () {
  const activatepage = yield select(makeSelectActivatePage());
  const { code } = activatepage
  const payload = {
    code: code
  }
  const requestURL = `${params.apiUrl}users/activate-password`;
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    if (result.error) {
      yield put(validateError(result.error.message));
    } else {
      yield put(validateSuccess());
      yield put(loginSuccess(result));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la solicitud de verificación de codigo para cambio de contraseña'
    yield put(validateError(err));
  }
}

export function* resetPassword() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(userError('Error token'));
    return
  }
  const activatepage = yield select(makeSelectActivatePage());
  const { password, rePassword } = activatepage
  const payload = {
    password: password,
    rePassword: rePassword
  }
  const requestURL = `${params.apiUrl}users/update`;
  const options = {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${authToken}`
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(passwordError(payload.error.message));
    } else {
      yield put(passwordSuccess());
    }    
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error al cambiar la contraseña'
    yield put(passwordError(err));
  }
}