import { createSelector } from 'reselect';

/**
 * Direct selector to the activatePasswordPage state domain
 */
const selectActivatePasswordPageDomain = (state) => state.get('activatePasswordPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ActivatePasswordPage
 */

const makeSelectActivatePasswordPage = () => createSelector(
  selectActivatePasswordPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectActivatePasswordPage;
export {
  selectActivatePasswordPageDomain,
};
