
import { fromJS } from 'immutable';
import activatePasswordPageReducer from '../reducer';

describe('activatePasswordPageReducer', () => {
  it('returns the initial state', () => {
    expect(activatePasswordPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
