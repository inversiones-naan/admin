/*
 *
 * ActivatePasswordPage actions
 *
 */

import {
  DEFAULT_ACTION,
  VALIDATE_REQUEST,
  VALIDATE_SUCCESS,
  VALIDATE_ERROR,
  CHANGE_PROP,
  VALIDATE_CLEAR,
  PASSWORD_REQUEST,
  PASSWORD_SUCCESS,
  PASSWORD_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}


/**
 * Validar codigo de activación
 *
 * @return {object} An action object with a type of VALIDATE_REQUEST
 */
export function validateRequest(code) {
  return {
    type: VALIDATE_REQUEST,
    code,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object} An action object with a type of VALIDATE_SUCCESS
 */
export function validateSuccess() {
  return {
    type: VALIDATE_SUCCESS,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object} An action object with a type of VALIDATE_ERROR
 */
export function validateError(error) {
  return {
    type: VALIDATE_ERROR,
    error,
  };
}

/**
 * Limpiar data
 *
 */
export function clear() {
  return {
    type: VALIDATE_CLEAR,
  };
}


/**
 * Cambiar contraseña
 */
export function passwordRequest() {
  return {
    type: PASSWORD_REQUEST,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object} An action object with a type of PASSWORD_SUCCESS
 */
export function passwordSuccess() {
  return {
    type: PASSWORD_SUCCESS,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object} An action object with a type of PASSWORD_ERROR
 */
export function passwordError(error) {
  return {
    type: PASSWORD_ERROR,
    error,
  };
}

/**
 * Cambiar el valor de una propiedad
 *
 */
export function changeProp(prop, value) {
  return {
    type: CHANGE_PROP,
    prop,
    value,
  };
}