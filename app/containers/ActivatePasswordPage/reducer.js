/*
 *
 * ActivatePasswordPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  VALIDATE_REQUEST,
  VALIDATE_SUCCESS,
  VALIDATE_ERROR,
  CHANGE_PROP,
  VALIDATE_CLEAR,
  PASSWORD_REQUEST,
  PASSWORD_SUCCESS,
  PASSWORD_ERROR,
} from './constants';

const initialState = fromJS({
  loading: false,
  success: false,
  error: false,
  code: null,
  password: '',
  rePassword: ''
});

function activatePasswordPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case VALIDATE_REQUEST:
      return state
      .set('code', action.code)
      .set('loading', true)
      .set('success', false)
      .set('error', false)
    case VALIDATE_SUCCESS:
      return state
      .set('code', null)
      .set('loading', false)
      .set('success', 'Muy bien, ya tienes acceso a tu cuenta, ahora cambia tu contraseña')
      .set('error', false)
    case VALIDATE_ERROR:
      return state
      .set('code', null)
      .set('loading', false)
      .set('success', false)
      .set('error', action.error)
    case VALIDATE_CLEAR:
      return state
      .set('code', null)
      .set('loading', false)
      .set('success', false)
      .set('error', false)
      .set('password', '')
      .set('rePassword', '')
    case PASSWORD_REQUEST:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false)
    case PASSWORD_SUCCESS:
      return state
      .set('code', null)
      .set('loading', false)
      .set('success', '¡Excelente! se ha modificado la contraseña')
      .set('error', false)
      .set('password', '')
      .set('rePassword', '')
    case PASSWORD_ERROR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', action.error)
    case CHANGE_PROP:
      return state
      .set(action.prop, action.value)
    default:
      return state;
  }
}

export default activatePasswordPageReducer;
