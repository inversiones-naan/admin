/*
 * ActivatePasswordPage Messages
 *
 * This contains all the text for the ActivatePasswordPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ActivatePasswordPage.header',
    defaultMessage: 'This is ActivatePasswordPage container !',
  },
});
