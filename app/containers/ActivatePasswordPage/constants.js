/*
 *
 * ActivatePasswordPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ActivatePasswordPage/DEFAULT_ACTION';
export const VALIDATE_REQUEST = 'app/ActivatePasswordPage/VALIDATE_REQUEST';
export const VALIDATE_SUCCESS = 'app/ActivatePasswordPage/VALIDATE_SUCCESS';
export const VALIDATE_ERROR = 'app/ActivatePasswordPage/VALIDATE_ERROR';

export const CHANGE_PROP = 'app/ActivatePasswordPage/CHANGE_PROP';

export const VALIDATE_CLEAR = 'app/ActivatePasswordPage/VALIDATE_CLEAR';

export const PASSWORD_REQUEST = 'app/ActivatePasswordPage/PASSWORD_REQUEST';
export const PASSWORD_SUCCESS = 'app/ActivatePasswordPage/PASSWORD_SUCCESS';
export const PASSWORD_ERROR = 'app/ActivatePasswordPage/PASSWORD_ERROR';

