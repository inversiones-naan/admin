
import { fromJS } from 'immutable';
import userNewPageReducer from '../reducer';

describe('userNewPageReducer', () => {
  it('returns the initial state', () => {
    expect(userNewPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
