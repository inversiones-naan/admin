/*
 *
 * UserNewPage constants
 *
 */

export const DEFAULT_ACTION = 'app/UserNewPage/DEFAULT_ACTION';
export const USER_CREATING = 'app/UsersPage/USER_CREATING';
export const USER_CREATED = 'app/UsersPage/USER_CREATED';
export const USER_ERROR = 'app/UsersPage/USER_ERROR';
export const USER_CHANGE_PROP = 'app/UsersPage/USER_CHANGE_PROP';
export const USER_CLEAR_DATA = 'app/UsersPage/USER_CLEAR_DATA';

