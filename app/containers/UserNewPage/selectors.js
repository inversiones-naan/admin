import { createSelector } from 'reselect';

/**
 * Direct selector to the userNewPage state domain
 */
const selectUserNewPageDomain = (state) => state.get('userNewPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by UserNewPage
 */

const makeSelectUserNewPage = () => createSelector(
  selectUserNewPageDomain,
  (substate) => substate.toJS()
);

const makeSelectUserLoading = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('loading')
);

const makeSelectUserSuccess = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('success')
);

const makeSelectUserError = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('error')
);

const makeSelectUserUsername = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('username')
);

const makeSelectUserEmail = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('email')
);

const makeSelectUserFirstName = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('firstName')
);

const makeSelectUserLastName = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('lastName')
);

const makeSelectUserPassword = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('password')
);

const makeSelectUserRePassword = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('rePassword')
);

const makeSelectUserRole = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('role')
);

const makeSelectUserSendEmail = () => createSelector(
  selectUserNewPageDomain,
  (userNewPageState) => userNewPageState.get('sendEmail')
);

// export default makeSelectUserNewPage;
export {
  makeSelectUserNewPage,
  selectUserNewPageDomain,
  makeSelectUserLoading,
  makeSelectUserSuccess,
  makeSelectUserError,
  makeSelectUserUsername,
  makeSelectUserEmail,
  makeSelectUserFirstName,
  makeSelectUserLastName,
  makeSelectUserPassword,
  makeSelectUserRePassword,
  makeSelectUserRole,
  makeSelectUserSendEmail
};
