import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { USER_CREATING, USER_CREATED, USER_ERROR } from './constants';
import { userCreated, userError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import { makeSelectToken } from '../App/selectors';
import { makeSelectUserUsername, makeSelectUserEmail, makeSelectUserFirstName, makeSelectUserLastName, makeSelectUserPassword, makeSelectUserRePassword, makeSelectUserRole, makeSelectUserSendEmail } from './selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(USER_CREATING, createUser);
}
/**
 * Obtener los usuarios de la API
 */
export function* createUser() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(userError('Error token'))
    return
  }
  const email = yield select(makeSelectUserEmail())
  // Es un correo?
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!re.test(email.toLowerCase())) {
    yield put(userError('No es un correo válido'));
    return
  }
  const password = yield select(makeSelectUserPassword())
  const rePassword = yield select(makeSelectUserRePassword())
  if (password.length < 8) {
    yield put(userError('La contraseña mínimo debe tener 8 caracteres'));
    return
  }
  if (password !== rePassword) {
    yield put(userError('Las contraseas son diferentes'));
    return
  }
  const firstName = yield select(makeSelectUserFirstName())
  const lastName = yield select(makeSelectUserLastName())
  const role = yield select(makeSelectUserRole())
  const sendEmail = yield select(makeSelectUserSendEmail())
  const payload = {
    email: email,
    firstName: firstName,
    lastName: lastName,
    password: password,
    role: role,
    sendEmail: sendEmail,
  }

  const requestURL = `${params.apiUrl}users/`;
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`,
    },
    body: JSON.stringify(payload)
  }
  try {
    // Call our request helper (see 'utils/request')
    const payload = yield call(request, requestURL, options);
    // console.log(payload)
    if (payload.error) {
      yield put(userError(payload.error.message));
    } else {
      yield put(userCreated(payload.user));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(userError('Error creating user'));
  }
}
