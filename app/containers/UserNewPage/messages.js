/*
 * UserNewPage Messages
 *
 * This contains all the text for the UserNewPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.UserNewPage.header',
    defaultMessage: 'This is UserNewPage container !',
  },
});
