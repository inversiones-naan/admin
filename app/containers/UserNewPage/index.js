/**
 *
 * UserNewPage
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
// import makeSelectUserNewPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

import { userCreate, userChangeProp, userClear } from './actions';
import {
  makeSelectUserNewPage,
  makeSelectUserLoading,
  makeSelectUserSuccess,
  makeSelectUserError,
  makeSelectUserUsername,
  makeSelectUserEmail,
  makeSelectUserFirstName,
  makeSelectUserLastName,
  makeSelectUserPassword,
  makeSelectUserRePassword,
  makeSelectUserRole,
  makeSelectUserSendEmail
} from './selectors';

import LoadingIndicator from 'components/LoadingIndicator';

import {NavLink} from 'react-router-dom';

export class UserNewPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {};
  }
  submit(evt) {
    let keyCode = evt.keyCode || evt.which
    if (evt.shiftKey === false && keyCode === 13) {
      this.props.onUserCreate()
    }
  }
  render() {
    const {usernewpage} = this.props
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <strong>Crear un nuevo usuario</strong> Crea un nuevo usuario y agrégalo a este sitio.
              </CardHeader>
              <CardBlock className="card-body">
                {this.props.loading
                ?<LoadingIndicator />
                :<Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  {usernewpage.success
                  ? <Card className="text-white bg-success">
                      <CardBlock className="card-body">
                        {'Se ha creado exitosamente el usuario, si deseas agregar mas información puedes '}<NavLink to={`/admin/users/profile/${usernewpage.success}`}><Button color="success" size="sm">editar</Button></NavLink>{' su perfil. Tambien puedes '}<Button color="success" size="sm" onClick={this.props.onUserClear}>limpiar</Button>{' este formulario.'}
                      </CardBlock>
                    </Card>                  
                  : null}
                  {usernewpage.error
                  ? <Card className="text-white bg-danger">
                      <CardBlock className="card-body">
                        {usernewpage.error}
                      </CardBlock>
                    </Card>                  
                  : null}
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Correo (requerido)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="email"
                        type="email"
                        id="email-input"
                        name="email-input"
                        placeholder="Escriba su correo"
                        value={this.props.email}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('email', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.password)
                            el.focus()
                          }
                        }}
                      />
                      <FormText className="help-block">Por favor introduzca su correo electrónico</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="password-input">Contraseña (requerido)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="password"
                        type="password"
                        id="password-input"
                        name="password-input"
                        placeholder="Contraseña"
                        value={this.props.password}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('password', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.rePassword)
                            el.focus()
                          }
                        }}
                      />
                      <FormText className="help-block">Por favor ingrese una contraseña compleja</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="password-input">Repetir Contraseña (requerido)</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="rePassword"
                        type="password"
                        id="password-input"
                        name="password-input"
                        placeholder="Contraseña"
                        value={this.props.rePassword}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('rePassword', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.role)
                            el.focus()
                          }
                        }}
                      />
                      <FormText className="help-block">Por favor ingrese una contraseña compleja</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="select">Rol</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="role"
                        type="select"
                        name="select"
                        id="select"
                        value={this.props.role}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('role', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.firstName)
                            el.focus()
                          }
                        }}
                      >
                        <option value="Unauthorized">No autorizado</option>
                        <option value="Authorized">Autorizado</option>
                        <option value="User">Usuario</option>
                        <option value="Manager">Gestor</option>
                        <option value="Administrator">Administrador</option>
                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Nombres</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="firstName"
                        type="text"
                        id="email-input"
                        name="email-input"
                        placeholder="Escriba los nombres"
                        value={this.props.firstName}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('firstName', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.lastName)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Apellidos</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input
                        ref="lastName"
                        type="text"
                        id="email-input"
                        name="email-input"
                        placeholder="Escriba los apellidos"
                        value={this.props.lastName}
                        onChange={(evt) => {
                          this.props.onUserChangeProp('lastName', evt.target.value)
                        }}
                        onKeyDown={(evt) => {
                          let keyCode = evt.keyCode || evt.which
                          if (evt.shiftKey === false && keyCode === 13) {
                            evt.preventDefault()
                            let el = ReactDOM.findDOMNode(this.refs.sendEmail)
                            el.focus()
                          }
                        }}
                      />
                    </Col>
                  </FormGroup>                  
                  <FormGroup row>
                    <Col md="3"><Label>Enviar notificacion al usuario</Label></Col>
                    <Col md="9">
                      <FormGroup check>
                        <div className="checkbox">
                          <Label
                            check
                            htmlFor="checkboxSendEmail"
                            onChange={() => {
                              this.props.onUserChangeProp('sendEmail', !this.props.sendEmail)
                            }}
                          >
                            <Input
                              ref="sendEmail"
                              type="checkbox"
                              id="checkboxSendEmail"
                              name="checkboxSendEmail"
                              value="sendEmail"
                              checked={this.props.sendEmail}
                              onChange={() => {}}
                              onKeyDown={this.submit.bind(this)}
                            /> Envíale al nuevo usuario un correo electrónico sobre su cuenta.
                          </Label>
                        </div>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                </Form>
                }
              </CardBlock>
              <CardFooter>
                <Button
                  type="submit"
                  size="sm"
                  color="primary"
                  onClick={this.props.onUserCreate}
                  disabled={this.props.loading}
                >Crear usuario</Button>{'  '}
                <Button
                  color="secondary"
                  size="sm"
                  onClick={this.props.onUserClear}
                >Limpiar datos</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

UserNewPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  usernewpage: makeSelectUserNewPage(),
  loading: makeSelectUserLoading(),
  success: makeSelectUserSuccess(),
  error: makeSelectUserError(),
  username: makeSelectUserUsername(),
  email: makeSelectUserEmail(),
  firstName: makeSelectUserFirstName(),
  lastName: makeSelectUserLastName(),
  password: makeSelectUserPassword(),
  rePassword: makeSelectUserRePassword(),
  role: makeSelectUserRole(),
  sendEmail: makeSelectUserSendEmail()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onUserChangeProp: (prop, value) => dispatch(userChangeProp(prop, value)),
    onUserCreate: () => dispatch(userCreate()),
    onUserClear: () => dispatch(userClear())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'userNewPage', reducer });
const withSaga = injectSaga({ key: 'userNewPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(UserNewPage);
