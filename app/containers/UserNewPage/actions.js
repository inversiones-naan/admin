/*
 *
 * UserNewPage actions
 *
 */

import {
  DEFAULT_ACTION,
  USER_CREATING,
  USER_CREATED,
  USER_ERROR,
  USER_CHANGE_PROP,
  USER_CLEAR_DATA
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/**
 * Solicita los usuarios al API
 *
 * @return {object}    An action object with a type of USER_CREATING
 */
export function userCreate() {
  return {
    type: USER_CREATING,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of USER_SUCCESS
 */
export function userCreated(user) {
  return {
    type: USER_CREATED,
    user,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of USER_ERROR
 */
export function userError(error) {
  return {
    type: USER_ERROR,
    error,
  };
}

/**
 * Cambiar los parametros del usuario
 *
 * @return {object}    An action object with a type of USER_CHANGE_PROP
 */
export function userChangeProp(prop, value) {
  return {
    type: USER_CHANGE_PROP,
    prop,
    value,
  };
}

/**
 * Limpiarla información del formulario
 *
 * @return {object}    An action object with a type of USER_CLEAR_DATA
 */
export function userClear() {
  return {
    type: USER_CLEAR_DATA,
  };
}