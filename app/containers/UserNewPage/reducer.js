/*
 *
 * UserNewPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  USER_CREATING,
  USER_CREATED,
  USER_ERROR,
  USER_CHANGE_PROP,
  USER_CLEAR_DATA
} from './constants';

const initialState = fromJS({
  loading: false,
  success: false,
  error: false,
  username: '',
  email: '',
  firstName: '',
  lastName: '',
  password: '',
  rePassword: '',
  role: 'User',
  sendEmail: false,
});

function userNewPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case USER_CREATING:
      return state
        .set('loading', true)
        .set('success', false)
        .set('error', false)
    case USER_CREATED:
      return state
        .set('loading', false)
        .set('error', false)
        .set('success', action.user.id)
    case USER_ERROR:
      return state
        .set('loading', false)
        .set('success', false)
        .set('error', action.error)
    case USER_CHANGE_PROP:
      return state
        .set(action.prop, action.value)
    case USER_CLEAR_DATA:
      return initialState
    default:
      return state;
  }
}

export default userNewPageReducer;
