import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { USERS_REQUEST } from './constants';
import { usersSuccess, usersError } from './actions';

import request from 'utils/request';
import params from 'utils/params';
import { makeSelectUsers } from './selectors';
import { makeSelectToken } from '../App/selectors';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(USERS_REQUEST, getUsers);
}
/**
 * Obtener los usuarios de la API
 */
export function* getUsers() {
  // Select authToken from store
  const authToken = yield select(makeSelectToken());
  if (!authToken) {
    yield put(usersError('Error token'));
    return
  }
  const requestURL = `${params.apiUrl}users`;
  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${authToken}`
    }
  }
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, options);
    console.log(result)
    if (result.error) {
      yield put(usersError(result.error.message));
    } else {
      yield put(usersSuccess(result.users));
    }
  } catch (err) {
    err = (typeof err === 'string') ?err:'Error en la petición'
    yield put(usersError(err));
  }
}