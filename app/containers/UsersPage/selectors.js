import { createSelector } from 'reselect';

/**
 * Direct selector to the usersPage state domain
 */
const selectUsersPageDomain = (state) => state.get('usersPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by UsersPage
 */

const makeSelectUsersPage = () => createSelector(
  selectUsersPageDomain,
  (substate) => substate.toJS()
);

const makeSelectUsers = () => createSelector(
  selectUsersPageDomain,
  (usersPageState) => usersPageState.toJS().users
);

const makeSelectLoading = () => createSelector(
  selectUsersPageDomain,
  (usersPageState) => usersPageState.get('loading')
);

const makeSelectError = () => createSelector(
  selectUsersPageDomain,
  (usersPageState) => usersPageState.get('error')
);

// export default makeSelectUsersPage;
export {
  makeSelectUsersPage,
  selectUsersPageDomain,
  makeSelectUsers,
  makeSelectLoading,
  makeSelectError
};
