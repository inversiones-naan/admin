/*
 *
 * UsersPage actions
 *
 */

import {
  DEFAULT_ACTION,
  USERS_REQUEST,
  USERS_SUCCESS,
  USERS_ERROR
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

/**
 * Solicita los usuarios al API
 *
 * @return {object}    An action object with a type of USERS_REQUEST
 */
export function usersRequest() {
  return {
    type: USERS_REQUEST,
  };
}

/**
 * Respuesta exitosa del API
 *
 * @return {object}    An action object with a type of USERS_SUCCESS
 */
export function usersSuccess(payload) {
  return {
    type: USERS_SUCCESS,
    payload,
  };
}

/**
 * Mensaje de error del API
 *
 * @return {object}    An action object with a type of USERS_ERROR
 */
export function usersError(error) {
  return {
    type: USERS_ERROR,
    error,
  };
}