/**
 *
 * UsersPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Link } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { usersRequest } from './actions';
import { makeSelectUsersPage, makeSelectUsers, makeSelectLoading, makeSelectError } from './selectors';

import LoadingIndicator from 'components/LoadingIndicator';

import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table
} from "reactstrap";

import Paginator from 'components/Paginator'

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

export class UsersPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-
  componentDidMount () {
    this.props.onUsersRequest()
  }
  status (status) {
    if (status === "0") {
      return <Badge color="warning">Pendiente</Badge>
    } else if (status === "1") {
      return <Badge color="success">Activo</Badge>
    } else if (status === "2") {
      return <Badge color="danger">Baneado</Badge>
    } else if (status === "3") {
      return <Badge color="secondary">Inactivo</Badge>
    }
  }
  render() {
    const { users, loading, error } = this.props
    let url = window.location.origin
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Usuarios
              </CardHeader>
              <CardBlock className="card-body">
                {loading
                ? <LoadingIndicator />
                : <ReactTable
                  data={users}
                  defaultPageSize={5}
                  filterable
                  defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value}
                  columns={[{
                    Header: 'Correo',
                    accessor: 'username',
                    style: { textAlign: 'center' },
                    filterMethod: (filter, row) => {
                      return row[filter.id].indexOf(filter.value) > -1
                    },
                    Cell: row => (
                      <div>
                        <Link to={`/admin/users/edit/${row.original.id}`}>
                          {row.value}
                        </Link>
                      </div>
                    )
                  },{
                    Header: 'Nombres',
                    accessor: 'firstName',
                    style: { textAlign: 'center' },
                    filterMethod: (filter, row) => {
                      return row[filter.id].indexOf(filter.value) > -1
                    },
                    Cell: row => (
                      <div>
                        {row.value}
                      </div>
                    )
                  },{
                    Header: 'Apellidos',
                    accessor: 'lastName',
                    style: { textAlign: 'center' },
                    filterMethod: (filter, row) => {
                      return row[filter.id].indexOf(filter.value) > -1
                    },
                    Cell: row => (
                      <div>
                        {row.value}
                      </div>
                    )
                  },{
                    Header: 'Estado',
                    accessor: 'status',
                    filterMethod: (filter, row) => {
                      if (filter.value === "all") {
                        return true;
                      }
                      return row[filter.id] === filter.value
                    },
                    Filter: ({ filter, onChange }) =>
                      <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}
                      >
                        <option value="all">Mostrar Todo</option>
                        <option value="0">Pendiente</option>
                        <option value="1">Activo</option>
                        <option value="2">Baneado</option>
                        <option value="3">Inactivo</option>
                      </select>
                    ,
                    Cell: row => (
                      <span >
                        <span style={{
                          color: row.value === '0' ? '#ff2e00'
                            : row.value === '1' ? '#66A4D6'
                            : row.value === '2' ? '#98C46F'
                            : row.value === '3' ? '#FB9230'
                            : '#000000',
                          transition: 'all .3s ease'
                        }}>
                          &#x25cf;
                        </span> {
                          row.value === '0' ? 'Pendiente'
                          : row.value === '1' ? `Activo`
                          : row.value === '2' ? `Baneado`
                          : row.value === '3' ? `Inactivo`
                          : 'Indefinido'
                        }
                      </span>
                    )
                  }]}
                  className="-striped -highlight"
                  previousText='Anterior'
                  nextText='Siguiente'
                  loadingText='Cargando...'
                  noDataText='No hay datos'
                  pageText='Página'
                  ofText='de'
                  rowsText='filas'
                />
                }
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

UsersPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  userspage: makeSelectUsersPage(),
  users: makeSelectUsers(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onUsersRequest: (page) => dispatch(usersRequest(page))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'usersPage', reducer });
const withSaga = injectSaga({ key: 'usersPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(UsersPage);
