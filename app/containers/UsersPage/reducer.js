/*
 *
 * UsersPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  USERS_REQUEST,
  USERS_SUCCESS,
  USERS_ERROR,
} from './constants';

const initialState = fromJS({
  users: [],
  loading: false,
  error: false
});

function usersPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case USERS_REQUEST:
      return state
      .set('user', [])
      .set('loading', true)
      .set('error', false)
    case USERS_SUCCESS:
      return state
      .set('users', fromJS(action.payload))
      .set('loading', false)
      .set('error', false)
    case USERS_ERROR:
      return state
      .set('user', [])
      .set('loading', false)
      .set('error', false)
    default:
      return state;
  }
}

export default usersPageReducer;
