/*
 *
 * UsersPage constants
 *
 */

export const DEFAULT_ACTION = 'app/UsersPage/DEFAULT_ACTION';
export const USERS_REQUEST = 'app/UsersPage/USERS_REQUEST';
export const USERS_SUCCESS = 'app/UsersPage/USERS_SUCCESS';
export const USERS_ERROR = 'app/UsersPage/USERS_ERROR';