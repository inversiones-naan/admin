/*
 * HeadScroll Messages
 *
 * This contains all the text for the HeadScroll component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.HeadScroll.header',
    defaultMessage: 'This is the HeadScroll component !',
  },
});
