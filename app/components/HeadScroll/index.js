/**
*
* HeadScroll
*
*/

import React from 'react';
import ReactDOM from 'react-dom';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import classie from 'desandro-classie'
import { Link as LinkScroll, DirectLink, Element , Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import { Parallax, Background } from 'react-parallax';

import {NavLink} from 'react-router-dom';

// Services
import Typing from '../../utils/typing'
import Scrambler from '../../utils/scrambler'

const carouselImages = [
  {
    url: './images/backgrounds/background03.jpg',
    title: 'Realiza tus inversiones en el presente',
    content: 'y prepara tu futuro con Rendex'
  },{
    url: './images/backgrounds/background02.jpg',
    title: 'Facil, rapido y desde la comodidad de tu casa',
    content: 'en Rendex las mejores opciones de inversión'
  },{
    url: './images/backgrounds/background01.jpg',
    title: 'Disfruta tu vida, invierte en tu futuro',
    content: 'con Rendex será mucho mas facil'
  }
]

class HeadScroll extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      count:0
    }
  }
  componentDidMount() {
    this.carouselImageBackgrounds()
    if (typeof window.addEventListener === 'function') {
      window.addEventListener("scroll", this.runOnScrollMozilla, true);
    } else if (typeof document.addEventListener === 'function') {
      document.addEventListener("scroll", this.runOnScroll, true);
    }

    if(window.attachEvent) {
      window.attachEvent('onresize', this.resizeWindows.bind(this));
    } else if(window.addEventListener) {
      window.addEventListener('resize', this.resizeWindows.bind(this), true);
    } else {
        //The browser does not support Javascript event binding
    }
    this.resizeWindows()
  }
  resizeWindows () {    
    let container = document.getElementById('head-intro')
    if (container) {
      container.style.height = document.body.getBoundingClientRect().height+'px'
    }
  }
  componentWillUnmount () {
    document.removeEventListener("scroll", this.runOnScroll, true);
    window.removeEventListener("scroll", this.runOnScrollMozilla, true);
    clearInterval(this.timesImages)
    if(window.detachEvent) {
      window.detachEvent('onresize', this.resizeWindows);
    } else if(window.removeEventListener) {
      window.removeEventListener('resize', this.resizeWindows, true);
    } else {
        //The browser does not support Javascript event binding
    }
  }
  carouselImageBackgrounds() {
    // let counter = 0;

    // const options = {
    //   // Initial position
    //   offset: 0,
    //   // Timeout between each random character
    //   timeout: 5,
    //   // Number of random characters to show
    //   iterations: 10,
    //   // Random characters to pick from
    //   characters: ['&','#','*','+','%','?','£','@','§','$'],
      
    //   // String to resolve
    //   resolveString: carouselImages[counter].title,
    //   // The element
    //   element: document.querySelector('[data-target-typing-target]')
    // }

    // // Callback function when resolve completes
    // function callback() {
    //   setTimeout(() => {
    //     counter ++;
        
    //     if (counter >= carouselImages.length) {
    //       counter = 0;
    //     }
        
    //     let nextOptions = Object.assign({}, options, {resolveString: carouselImages[counter].title});
    //     Typing.resolve(nextOptions, callback);
    //   }, 2000);
    // }
    // Typing.resolve(options, callback);

    let counter = -1;
    const optionsTitle = {
      codeletters: '0123456789',//"&#*+%?£@§$",
      message: 0,
      current_length: 0,
      fadeBuffer: false,
      // String to resolve
      resolveString: carouselImages[0].title,
      // The element
      element: document.querySelector('[data-target-typing-target]')
    }
    const optionsContent = {
      codeletters: '0123456789',//"&#*+%?£@§$",
      message: 0,
      current_length: 0,
      fadeBuffer: false,
      // String to resolve
      resolveString: carouselImages[0].title,
      // The element
      element: document.querySelector('[data-target-typing-content]')
    }
    function callback1() {
      counter ++;
      if (counter >= carouselImages.length) {
        counter = 0;
      }
      optionsContent.element.textContent = ''
      document.getElementById('background-image-top').src = carouselImages[counter].url
      document.getElementById('background-image-bottom').src = carouselImages[counter].url
      let nextOptions = Object.assign({}, optionsTitle, {resolveString: carouselImages[counter].title});
      Scrambler.resolve(nextOptions, callback2);
    }
    function callback2() {
      console.log('callback2')
      let nextOptions = Object.assign({}, optionsContent, {resolveString: carouselImages[counter].content});
      Scrambler.resolve(nextOptions, callback3);
    }
    function callback3() {
      console.log('callback3')
      setTimeout(()=>{
        callback1()
      }, 3500)
    }
    callback1()
  }
  runOnScroll () {
    let container = document.getElementById('container')
    if (document.body.scrollTop >= 0) {
      classie.add(container,'modify');
    } else {
      classie.remove(container,'modify');
    }
  }
  runOnScrollMozilla () {
    var last_known_scroll_position = window.scrollY;
    window.requestAnimationFrame(function() {
      let container = document.getElementById('container')
      if (last_known_scroll_position >= 2) {
        classie.add(container,'modify');
      } else {
        classie.remove(container,'modify');
      }
    });
  }

  render() {
    const {url, title, content} = carouselImages[this.state.count]
    return (
      <div className="header" id="head-intro">
        <div className="bg-img" ref="scrollImgTop"><img alt="Background Image" id='background-image-top'/></div>
        <div className="title">
          <h1 ref="scrollImgTittle" data-target-typing-target className='scrambler'></h1>
          <p ref="scrollImgContent" className="subline" data-target-typing-content></p>
          <div className="text-center">
            <NavLink to="/proyectos">
              <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper button--size-l" >Invierte</button>
            </NavLink>
            <LinkScroll to="howToInvest" spy={true} smooth={true} offset={-100} duration={500} delay={100}>
              <button className="button-rendex button--pipaluk button--inverted button--round-l button--text-thick button--text-upper button--size-l" >Cómo funciona</button>
            </LinkScroll>
          </div>
          <div style={{height:100}} />
        </div>
        <div className="bg-img" ref="scrollImgBottom"><img alt="Background Image" id='background-image-bottom'/></div>
      </div>
    );
  }
}

HeadScroll.propTypes = {

};

export default HeadScroll;
