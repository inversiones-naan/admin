export default {
  items: [
    {
      name: 'Inicio',
      url: '/inicio',
      icon: 'icon-speedometer',
    },
    {
      name: 'Proyectos',
      url: '/proyectos',
      icon: 'icon-layers',
    }
  ]
};
