/**
 *
 * Asynchronously loads the component for Aaa
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./Sidebar'),
  loading: () => null,
});
