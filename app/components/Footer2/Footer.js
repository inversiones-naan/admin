import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <a href="#">Rendex</a> &copy; 2017 Nestor Andres A. A.
        <span className="float-right">Powered by <a href="#">React Js</a></span>
      </footer>
    )
  }
}

export default Footer;
