import React, {Component} from 'react';
import {Row, Col} from "reactstrap";
import {
  Badge,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink as RsNavLink,
  NavbarToggler,
  NavbarBrand,
  DropdownToggle
} from 'reactstrap';

import {NavLink} from 'react-router-dom';

class Header extends Component {

  constructor(props) {
    super(props);

    // this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
    let AppHeader = document.getElementsByClassName('app-header')[0]
    if (document.body.classList.contains('sidebar-mobile-show')) {
      AppHeader.classList.add('navbar-mobile-rendex');
    } else {
      AppHeader.classList.remove('navbar-mobile-rendex');
    }    
  }
  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  render() {
    let url = window.location.origin
    let { user } = this.props
    let name = ''
    if (user) {
      // debugger
      if (user.firstName !== null || user.lastName !== null) {
        name = `${user.firstName} ${user.lastName}`.trim()
      }
      if (name === '') {
        name = user.username.split('@')[0]
      }
    }
    

    return (
      <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>&#9776;</NavbarToggler>
        <NavLink to="/" className='navbar-brand'></NavLink>        
        {/* <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}>&#9776;</NavbarToggler> */}
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/proyectos">Proyectos</NavLink>
          </NavItem>
          {user
          ? <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle.bind(this)}>
            <DropdownToggle className="nav-link dropdown-toggle">
              {`${name}`}
            </DropdownToggle>
            <DropdownMenu right className={this.state.dropdownOpen ? 'show' : ''}>
              <NavLink to="/portafolio">
                <DropdownItem>
                  Portafolio
                </DropdownItem>
              </NavLink>
              <NavLink to="/mi-perfil">
                <DropdownItem>
                  Mi Perfil
                </DropdownItem>
              </NavLink>
              <DropdownItem onClick={this.props.onLogout}>
                Salir
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
          :null}
          {user
          ? user.role === 'Administrator'
          ? <NavItem className="px-3">
            <NavLink to="/admin">Administrador</NavLink>
          </NavItem>
          :null: null}
          {!user
          ? <NavItem className="px-3">
            <NavLink to="/iniciar-sesion">Ingresar</NavLink>
          </NavItem>
          : null}
          {!user
          ? <NavItem className="px-3">
            <NavLink to="/registro">Registro</NavLink>
          </NavItem>
          :null}
        </Nav>
      </header>
    )
  }
}

export default Header;
