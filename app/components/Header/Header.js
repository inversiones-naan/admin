import React, {Component} from 'react';
import {Row, Col} from "reactstrap";
import {
  Badge,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink as RsNavLink,
  NavbarToggler,
  NavbarBrand,
  DropdownToggle
} from 'reactstrap';

import {NavLink} from 'react-router-dom';

class Header extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }

  render() {
    let url = window.location.origin
    let { user } = this.props
    return (
      <header className="app-header navbar navbar-admin">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}><span style={{color: '#000000'}}>&#9776;</span></NavbarToggler>
        <NavbarBrand href="#"></NavbarBrand>
        <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}><span style={{color: '#000000'}}>&#9776;</span></NavbarToggler>
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/">Ir Pagina de inicio</NavLink>
          </NavItem>
          {/* <NavItem className="px-3">
            <NavLink to="/widgets">Widgets</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <RsNavLink href="#">Settings</RsNavLink>
          </NavItem> */}
        </Nav>
        <Nav className="ml-auto" navbar>
          {/* <NavItem className="d-md-down-none">
            <RsNavLink href="#"><i className="icon-bell"></i><Badge pill color="danger">5</Badge></RsNavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <RsNavLink href="#"><i className="icon-list"></i></RsNavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <RsNavLink href="#"><i className="icon-location-pin"></i></RsNavLink>
          </NavItem> */}
          <NavItem>
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <DropdownToggle className="nav-link dropdown-toggle" style={{marginRight: 40}}>
                {/* <img src={url+'/images/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/> */}
                {/* <div className='img-avatar'>
                  <i className="fa fa-user-circle-o fa-3x"></i>
                </div>           */}
                <i className="fa fa-user-circle-o fa-3x"></i>{' '}     
                {/* <span className="d-md-down-none">{`${user.firstName} ${user.lastName}`}</span> */}
              </DropdownToggle>
              <DropdownMenu right className={this.state.dropdownOpen ? 'show' : ''}>
                <DropdownItem header tag="div" className="text-center"><strong>Cuenta</strong></DropdownItem>
                <DropdownItem onClick={this.props.onLogout}><i className="fa fa-lock"></i> Logout</DropdownItem>
                {/* <DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge color="info">42</Badge></DropdownItem>
                <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge color="success">42</Badge></DropdownItem>
                <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge></DropdownItem>
                <DropdownItem><i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge></DropdownItem>
                <DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>
                <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
                <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
                <DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
                <DropdownItem><i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge></DropdownItem>
                <DropdownItem divider/>
                <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem> */}
              </DropdownMenu>
            </Dropdown>
          </NavItem>
        </Nav>
        {/* <NavbarToggler className="d-md-down-none" type="button" onClick={this.asideToggle}>&#9776;</NavbarToggler> */}
      </header>
    )
  }
}

export default Header;
