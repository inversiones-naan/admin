/**
*
* LoadingIndicatorCenter
*
*/

import React from 'react';
// import styled from 'styled-components';
import LoadingIndicator from 'components/LoadingIndicator';

class LoadingIndicatorCenter extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <div style={{height: screen.height/2}}/>
        <LoadingIndicator />
        <div style={{height: screen.height/2}}/>
      </div>
    );
  }
}

LoadingIndicatorCenter.propTypes = {

};

export default LoadingIndicatorCenter;
