/*
 * Paginator Messages
 *
 * This contains all the text for the Paginator component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Paginator.header',
    defaultMessage: 'This is the Paginator component !',
  },
});
