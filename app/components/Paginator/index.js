/**
*
* Paginator
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import {
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";

import numerator from './numerator'

class Paginator extends React.Component { // eslint-disable-line react/prefer-stateless-function
  renderFirst (current) {
    return (
      <PaginationItem disabled={parseInt(current) === 1}>
        <PaginationLink previous href="#">Anterior</PaginationLink>
      </PaginationItem>
    )
  }
  renderLast (current,total_pages) {
    return (
      <PaginationItem disabled={current === total_pages}>
        <PaginationLink previous href="#">Siguiente</PaginationLink>
      </PaginationItem>
    )
  }
  /**
   * Renderiza los numeros del componente.
   */
  renderNumbers (current, total_pages, range, name, request) {
    let p = this.props.pagination
    let items = []
    numerator.getList(parseInt(total_pages), parseInt(current), range).forEach(function (e, i) {
      if (e[0] === '...') {
        items.push(<PaginationItem key={'navigator-' + name + '-' + items.length}><PaginationLink href='javascript:void(0);'>{e[0]}</PaginationLink></PaginationItem>)
      } else {
        items.push(<PaginationItem key={'navigator-' + name + '-' + items.length} active={e[1]}><PaginationLink href='javascript:void(0);' onClick={()=>request(e[0])}>{e[0]}</PaginationLink></PaginationItem>)
      }
    })
    return items
  }
  render() {
    const {before, next, last, current, total_pages, range, request} = this.props
    if (!this.name) {
      this.name = (Math.random() * (100 - 1) + 1) + (+new Date())
    }
    return (
      <Pagination>
        {this.renderFirst(current)}
        {this.renderNumbers(current, total_pages, range, this.name, request)}
        {this.renderLast(current, total_pages)}
      </Pagination>
    );
  }
}

Paginator.propTypes = {
  before: PropTypes.number,
  next:  PropTypes.number,
  last:  PropTypes.number,
  current:  PropTypes.number,
  total_pages:  PropTypes.number,
  range: PropTypes.number,
  request: PropTypes.func.isRequired
};

export default Paginator;
