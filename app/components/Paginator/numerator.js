/**
* @function getList Obtiene los elementos que componen al paginador.
* @param  {Number} total   - total de paginas en la lista
* @param  {Number} actual  - pagina actual
* @param  {Number} visible - numero de numeros a ser visibles
* @return {Array} - lista de objetos que contiene la numeración de la paginación
*/
let getList = function (total, actual, visible = -1) {
  let elementos = []

  if (typeof total === 'undefined') {
    throw Error('Without total')
  }

  if (typeof actual === 'undefined') {
    throw new Error('Without actual')
  }

  if (total.type !== Number.type || actual.type !== Number.type || visible.type !== Number.type) {
    throw Error('Los parametros no cumplen con el tipo de dato necesario. Los paremetros deben de ser Numeros enteros')
  }

  if (actual > total) {
    actual = total
  }

  if (actual < 1) {
    actual = 1
  }
  if (visible === -1 || visible > total) {
    total >= 5 ? visible = 5 : visible = total
  }

  let y = Math.floor(visible / 2)

  let derecha = Math.min(total, actual + y)

  if ((actual + y) > total) {
    y += (actual + y) - total
  }

  if ((visible % 2) === 0) {
    y -= 1
  }

  let izquierda = Math.max(1, actual - y)

  while ((derecha - izquierda + 1) < visible) {
    derecha += 1
  }

  for (let i = izquierda; i <= derecha; i += 1) {
    if (i === actual) {
      elementos.push([i.toString(), true])
    } else {
      elementos.push([i.toString(), false])
    }
  }

  if (izquierda > 1) {
    elementos.unshift(['...', false])
  }

  if (derecha < total) {
    elementos.push(['...', false])
  }

  return elementos
}

module.exports = {
  getList: getList
}
