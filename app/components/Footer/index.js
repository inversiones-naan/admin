import React from 'react';
import { FormattedMessage } from 'react-intl';

import A from 'components/A';
import LocaleToggle from 'containers/LocaleToggle';
import Wrapper from './Wrapper';
import messages from './messages';

function Footer() {
  return (
    <div className="footer">
      <div className="container" style={{maxWidth: 1800, margin: 'auto', paddingLeft: 40, paddingRight: 40}}>
        <div className="row">
          <div className="col-sm-11">
            <div className="row">
              <div className="col-sm-3">
                <ul>
                  <li>
                    <strong>¿Quieres invertir?</strong>
                  </li>
                  <li>
                    <a href="/como-invertir">¿Cómo funciona?</a>
                  </li>
                  <li>
                    <a href="/porque-invertir-en-Briq-mx">Seguridad y confianza</a>
                  </li>
                  <li>
                    <a href="/proyectos">Ver oportunidades</a>
                  </li>
                  <li>
                    <a href="/desarrolladores">Conoce a los desarrolladores</a>
                  </li>
                </ul>
              </div>
              <div className="col-sm-3">
                <ul>
                  <li>
                    <strong>¿Eres desarrollador?</strong>
                  </li>
                  <li>
                    <a href="/levanta-capital">Levanta capital</a>
                  </li>
                  <li>
                    <a href="/proceso-seleccion">Selección de proyectos</a>
                  </li>
                </ul>
              </div>
              <div className="col-sm-3">
                <ul>
                  <li>
                    <strong>Acerca de Rendex.com.mx</strong>
                  </li>
                  <li>
                    <a href="/nosotros">Sobre nosotros</a>
                  </li>
                  <li>
                    <a href="/medios">Medios y prensa</a>
                  </li>
                  <li>
                    <a href="/avisoprivacidad">Aviso de privacidad</a>
                  </li>
                  <li>
                    <a href="/tyc">Términos y condiciones</a>
                  </li>
                </ul>
              </div>
              <div className="col-sm-3">
                <ul>
                  <li>
                    <strong>Centro de educación</strong>
                  </li>
                  <li>
                    <a href="/articulos">Artículos</a>
                  </li>
                  <li>
                    <a href="/preguntas">Preguntas frecuentes</a>
                  </li>
                  <li>
                    <a href="/glosario">Glosario</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer__social-bar">
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-md-4 col-sm-6">
              <ul className="row">
                <li className="col-sm-3">
                  <a target="_blank" href="https://www.facebook.com/BriqMx">
                    <img alt="Rendex.com.mx" src="https://d3o8kmn1hwslvb.cloudfront.net/assets/footer-social-01-a9d312bea894d851b6462ab7f7435bca32e04955d2ae11c9ea4e87101500e7b6.svg"/>
                  </a>
                </li>
                <li className="col-sm-3">
                  <a target="_blank" href="https://twitter.com/BriqMx">
                    <img alt="Rendex.com.mx" src="https://d3o8kmn1hwslvb.cloudfront.net/assets/footer-social-02-8c9ac0195a19d0d010687cce31e8c1d3690ea826d3429804cbc47de07dbb886f.svg"/>
                  </a>
                </li>
                <li className="col-sm-3">
                  <a target="_blank" href="https://www.youtube.com/channel/UC5gSLYFILbOPSXDd9Oax4AA">
                    <img alt="Rendex.com.mx" src="https://d3o8kmn1hwslvb.cloudfront.net/assets/footer-social-youtube-4d99a200b4afd8f8853c66df3545e73707a7403d8e47c2ce7ead10fb53e3cdbe.svg"/>
                  </a>
                </li>
                <li className="col-sm-3">
                  <a target="_blank" href="https://www.linkedin.com/company/briq-mx">
                    <img alt="Rendex.com.mx" src="https://d3o8kmn1hwslvb.cloudfront.net/assets/footer-social-04-370f831dfdf6ac9d6b85270ca5fe28d72ed555104ab86550e475367514f57591.svg"/>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
