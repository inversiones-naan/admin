import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-weight: 300;
  }

  body.fontLoaded {
    font-family: 'Roboto Condensed', 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: 'Roboto Condensed', Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
  button {
    font-size: 0.65em;
  }
`;
